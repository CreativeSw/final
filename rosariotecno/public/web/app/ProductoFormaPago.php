<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoFormaPago extends Model
{
    //
    protected $fillable = [
      'producto',
      'formapago',
      'precio'
    ];
}
