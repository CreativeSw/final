<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Sucursal
{

    public function handle($request, Closure $next)
    {
      if(!isset(Auth::user()->rol))
        return redirect()->to('salir');

      if(Auth::user()->rol=="sucursal"){
        return $next($request);
      }
      return redirect()->to('salir');
    }
}
