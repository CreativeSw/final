<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Deposito
{

    public function handle($request, Closure $next)
    {
        if(!isset(Auth::user()->rol))
          return redirect()->to('salir');

        if(Auth::user()->rol == "deposito"){
          return $next($request);
        }
        return redirect()->to('salir');

    }
}
