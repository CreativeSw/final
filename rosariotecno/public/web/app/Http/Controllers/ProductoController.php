<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Importador;
use App\Marca;
use App\Categoria;
use App\Producto;
use App\ProductoSucursal;
use App\FormadePago;
use App\ProductoFormaPago;
use Session;
use Excel;
use Picqer\Barcode\BarcodeGeneratorJPG;

class ProductoController extends Controller
{
    public function index(){
      $productos = Producto::all();
      foreach ($productos as $p) {
        $marca = Marca::find($p->idmarca);
        $p['marca'] = $marca->nombre;
        $categoria = Categoria::find($marca->idcategoria);
        $p['categoria'] = $categoria->nombre;
      }
      return view('admin.productos.index',compact('productos'));
    }

    public function indexcodigos(){
      $marcas = Marca::all();
      foreach ($marcas as $m) {
        $cat = Categoria::find($m->idcategoria);
        $m['descripcion'] = strtoupper($m->nombre.'-'.$cat->nombre);
      }
      return view('admin.productos.generador',compact('marcas'));
    }

    public function getproducto($id){
      $productos = Producto::where('idmarca',$id)->get();
      return $productos;
    }

    public function generarcodigo(){
      $datos = request()->all();
      $generador = new BarcodeGeneratorJPG();
      $producto = Producto::find($datos['producto']);
      $texto = $producto->modelo;
      $tipo = $generador::TYPE_CODE_128;
      $imagen = $generador->getBarcode($texto, $tipo);
      $nombreArchivo = "codigo_de_barras.png";
      header('Content-Type: application/octet-stream');
      header("Content-Transfer-Encoding: Binary");
      header("Content-disposition: attachment; filename=$nombreArchivo");
      echo $imagen;
    }

    public function indexprecios(){
      return view('admin.precios.index');
    }

    public function indexsucursalupdate(){
      return view('sucursal.precios.index');
    }
    public function updateprecios(){
      $datos = request()->all();
      $productos = Excel::load($datos['archivo'])->get();
      foreach ($productos as $c) {
        if(isset($c['marca']) && $c['marca'] != ""){
          $macaexiste = Marca::where('nombre',$c['marca'])->get();
          if(count($macaexiste) > 0){
            $listmarca = [];
            $idmarca = Marca::where('nombre',$c['marca'])->get(['id']);
            foreach ($idmarca as $m) {
              $listmarca[] = $m['id'];
            }
            $pos = 0;
            foreach ($c as $key => $value) {
              if($pos > 4 && $pos <10){
                $formapago = FormadePago::where('nombre',$key)->get();
                if(count($formapago)== 0){
                  Session::flash('mensaje','NO EXISTE ESTA FORMA DE PAGO '.$key.' EN EL SISTEMA');
                  return redirect()->to('preciosupdate');
                }else{
                  $producto = Producto::where('modelo',$c['modelo'])->get();
                  if(count($producto)== 0){
                    Session::flash('mensaje','NO EXISTE EL ARTICULO '.$c['modelo'].' EN EL SISTEMA');
                    return redirect()->to('preciosupdate');
                  }else{
                    $productomarca = Producto::where('modelo',$c['modelo'])->whereIn('idmarca',$listmarca)->get();
                    if(count($productomarca) == 0){
                      Session::flash('mensaje','NO EXISTE EL ARTICULO '.$c['modelo'].' DE LA MARCA '.$c['marca'].'EN EL SISTEMA');
                      return redirect()->to('preciosupdate');
                    }else{
                      $formadepagoproducto = ProductoFormaPago::where('formapago',$formapago[0]->id)->where('producto',$productomarca[0]->id)->get();
                      if(count($formadepagoproducto) == 0){
                        ProductoFormaPago::create(array(
                          'producto'=>$productomarca[0]->id,
                          'formapago'=>$formapago[0]->id,
                          'precio'=>(float) $value
                        ));
                      }else{
                        $formadepagoproducto = ProductoFormaPago::find($formadepagoproducto[0]->id);
                        $formadepagoproducto->fill(array(
                          'precio'=>(float) $value
                        ));
                        $formadepagoproducto->save();
                      }
                    }
                  }
                }
              }
              $pos++;
            }
          }else{
            Session::flash('mensaje','NO EXISTE LA MARCA '.$c['marca'].' EN EL SISTEMA');
            return redirect()->to('preciosupdate');
          }
        }
      }
      Session::flash('mensaje','LOS PRECIOS FUERON ACTUALIZADOS');
      return redirect()->to('preciosupdate');
    }


    public function sucursalupdate(){
      $datos = request()->all();
      $productos = Excel::load($datos['archivo'])->get();
      foreach ($productos as $c) {
        if(isset($c['marca']) && $c['marca'] != ""){
          $macaexiste = Marca::where('nombre',$c['marca'])->get();
          if(count($macaexiste) > 0){
            $listmarca = [];
            $idmarca = Marca::where('nombre',$c['marca'])->get(['id']);
            foreach ($idmarca as $m) {
              $listmarca[] = $m['id'];
            }
            $pos = 0;
            foreach ($c as $key => $value) {
              if($pos > 4 && $pos <10){
                $formapago = FormadePago::where('nombre',$key)->get();
                if(count($formapago)== 0){
                  Session::flash('mensaje','NO EXISTE ESTA FORMA DE PAGO '.$key.' EN EL SISTEMA');
                  return redirect()->to('sucursal/preciosupdate');
                }else{
                  $producto = Producto::where('modelo',$c['modelo'])->get();
                  if(count($producto)== 0){
                    Session::flash('mensaje','NO EXISTE EL ARTICULO '.$c['modelo'].' EN EL SISTEMA');
                    return redirect()->to('preciosupdate');
                  }else{
                    $productomarca = Producto::where('modelo',$c['modelo'])->whereIn('idmarca',$listmarca)->get();
                    if(count($productomarca) == 0){
                      Session::flash('mensaje','NO EXISTE EL ARTICULO '.$c['modelo'].' DE LA MARCA '.$c['marca'].'EN EL SISTEMA');
                      return redirect()->to('sucursal/preciosupdate');
                    }else{
                      $formadepagoproducto = ProductoFormaPago::where('formapago',$formapago[0]->id)->where('producto',$productomarca[0]->id)->get();
                      if(count($formadepagoproducto) == 0){
                        ProductoFormaPago::create(array(
                          'producto'=>$productomarca[0]->id,
                          'formapago'=>$formapago[0]->id,
                          'precio'=>(float) $value
                        ));
                      }else{
                        $formadepagoproducto = ProductoFormaPago::find($formadepagoproducto[0]->id);
                        $formadepagoproducto->fill(array(
                          'precio'=>(float) $value
                        ));
                        $formadepagoproducto->save();
                      }
                    }
                  }
                }
              }
              $pos++;
            }
          }else{
            Session::flash('mensaje','NO EXISTE LA MARCA '.$c['marca'].' EN EL SISTEMA');
            return redirect()->to('sucursal/preciosupdate');
          }
        }
      }
      Session::flash('mensaje','LOS PRECIOS FUERON ACTUALIZADOS');
      return redirect()->to('sucursal/preciosupdate');
    }

    public function indexcreate(){
      $importadores = Importador::all();
      $marcas = Marca::all();
      foreach ($marcas as $m) {
        $cat = Categoria::find($m->idcategoria);
        $m['descripcion'] = strtoupper($m->nombre.'-'.$cat->nombre);
      }
      return view('admin.productos.create',compact('importadores','marcas'));
    }

    public function create(Request $request){
      $datos = request()->all();
      $mensaje = 'El producto, fue creado con exito';
      $existe = Producto::where('idmarca',$datos['idmarca'])->where('modelo',$datos['modelo'])->count();
      if($existe > 0){
        $mensaje = 'El producto, ya existe en el sistema';
      }else{
        $nombre = $this->upload($datos['imagen']);
        $datos['imagen'] = $nombre;
        Producto::create(array(
          "idmarca" => $datos['idmarca'],
          "modelo" => $datos['modelo'],
          "descripcion" => $datos['descripcion'],
          "stockminimo" => $datos['stockminimo'],
          "imagen" => $datos['imagen'],
          "garantia"=>$datos['grantia'],
          'stock' => -1
        ));
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('productos');
    }

    public function edit($id){
      $producto = Producto::find($id);
      if($producto == null){
        Session::flash('mensaje','No se encontro el producto');
        return redirect()->to('productos');
      }
      $marca = Marca::find($producto->idmarca);
      $categoria = Categoria::find($marca->idcategoria);
      $producto['marcaproducto'] = strtoupper($marca->nombre.'-'.$categoria->nombre);

      $importadores = Importador::all();
      $marcas = Marca::all();
      foreach ($marcas as $m) {
        $cat = Categoria::find($m->idcategoria);
        $m['descripcion'] = strtoupper($m->nombre.'-'.$cat->nombre);
      }
      return view('admin.productos.create',compact('importadores','marcas','producto'));
    }

    public function save($id,Request $request){
      $datos = request()->all();
      $mensaje = 'El producto, fue actualizado con exito';
      $existe = Producto::where('idmarca',$datos['idmarca'])->where('modelo',$datos['modelo'])->where('id','<>',$id)->count();
      if($existe > 0){
        $mensaje = 'El producto ya existe en nuestra base de datos';
      }else{
        $producto = Producto::find($id);
        if($producto == null){
          $mensaje = 'El producto no existe';
        }else{
          if(isset($datos['imagen'])){
            $nombre = $this->upload($datos['imagen']);
            $datos['imagen'] = $nombre;
          }else{
            $datos['imagen'] = $producto->imagen;
          }
          $datos['stock'] = $producto->stock;
          $producto->fill(array(
            "idmarca" => $datos['idmarca'],
            "modelo" => $datos['modelo'],
            "descripcion" => $datos['descripcion'],
            "stockminimo" => $datos['stockminimo'],
            "imagen" => $datos['imagen'],
            'stock' => $datos['stock'],
            'garantia'=>$datos['grantia']
          ));
          $producto->save();
        }
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('productos');
    }

    public function delete($id){
      $mensaje = 'El producto fue eliminado con exito';
      $producto = Producto::find($id);
      if($producto == null){
        $mensaje = 'El producto no puede ser borrado';
      }else{
        unlink('uploads/'.$producto->imagen);
        $producto->delete();
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('productos');
    }

    public function importar(){
      $datos = request()->all();
      $mensaje = 'La importacion se realizo con exito';
      $productos = Excel::load($datos['archivo'])->get();
      foreach ($productos as $c) {
        if(isset($c['marca']) && isset($c['modelo']) && isset($c['descripcion'])
        && isset($c['stock']) && isset($c['stockminimo']) && isset($c['transporte'])
        && isset($c['importador']) && isset($c['categoria'])){
          $marcas = Marca::where('nombre',$c['marca'])->get();
          if(count($marcas) == 0){
            $mensaje = 'La marca '.$c['marca'].', no existe';
            Session::flash('mensaje', $mensaje);
            return redirect()->to('productos');
          }
          $categorias = Categoria::where('nombre',$c['categoria'])->get();
          if(count($marcas) == 0){
            $mensaje = 'La categoria '.$c['categoria'].', no existe';
            Session::flash('mensaje', $mensaje);
            return redirect()->to('productos');
          }

          foreach ($marcas as $m) {
            $categorias = Categoria::where('nombre',$c['categoria'])->where('id',$m->idcategoria)->get();
            if(count($categorias)==0){
              $categoria = Categoria::find($m->idcategoria);
              $mensaje = 'La marca '.$c['marca'].', no pertenece a la categoria '.$categoria->nombre.' '.$c['categoria'];
              Session::flash('mensaje', $mensaje);
              return redirect()->to('productos');
            }
          }
          $importadores = Importador::where('nombre',$c['importador'])->get();
          if(count($importadores) == 0){
            $mensaje = 'El importador '.$c['importar'].', no existe';
            Session::flash('mensaje', $mensaje);
            return redirect()->to('productos');
          }

          $existe = Producto::where('modelo',$c['modelo'])->count();
          if($existe > 0){
            $producto = Producto::where('modelo',$c['modelo'])->forst();
            $producto->fill(array(
              "descripcion" => $c['descripcion'],
              "stock" => $c['stock'],
              "stockminimo" => $c['stockminimo'],
              "transporte" => $c['transporte']
            ));
            $producto->save();
          }else{
            $marcas = Marca::where('nombre',$c['marca'])->get();
            foreach ($marcas as $m) {
              $categoria = Categoria::find($marca->idcategoria);
              if($categoria->nombre == $c['categoria']){
                $idmarca = $m->id;
                break;
              }
              // code...
            }

            $importadores = Importador::where('nombre',$c['importador'])->first();
            $producto = Producto::create(array(
              "idmarca"=>$idmarca,
              "modelo" => $c['modelo'],
              "descripcion" => $c['descripcion'],
              "stockminimo" => $c['stockminimo'],
              "transporte" => $c['transporte'],
            ));
          }


        }else{
          dd('no tiene formato');
        }
      }

      Session::flash('mensaje', $mensaje);
      return redirect()->to('productos');
    }

    private function upload($file){
      $nombre = str_random(40).'.'.$file->getClientOriginalExtension();
      $destinationPath = 'uploads';
      $file->move('uploads',$nombre);
      return $nombre;
    }

}
