<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\FormadePago;
use App\Categoria;
use Session;
use Excel;

class FormadePagoController extends Controller
{
    public function index(){
      $forma = FormadePago::all();

      foreach ($forma as $f) {

        $categoria = Categoria::find($f->idcategoria);
        if($categoria != null){
          $f['categoria'] = $categoria->nombre;
        }else{
          $f['categoria'] = '';
        }

        // code...
      }
      return view('admin.formasdepagos.index', compact('forma'));
    }
    public function indexcreate(){
      $categorias = Categoria::all();
      return view('admin.formasdepagos.create',compact('categorias'));
    }

    public function create(){
      $datos = request()->all();
    //  dd($datos);
      $mensaje = 'La forma de pago fue creada con exito';
      $existe = FormadePago::where('nombre',$datos['nombre'])->count();

      if($existe > 0){
        $mensaje = 'La forma de pago ya existe';
      }else{
        $formapago = FormadePago::create($datos);
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('formadepagos');
    }

    public function edit($id){
      $forma = FormadePago::find($id);
      $categorias = Categoria::all();
    //  dd($categorias);
      if($forma == null){
        Session::flash('mensaje','La forma de pago no existe');
        return redirect()->to('formadepagos');
      }
      return view('admin.formasdepagos.create',compact('forma','categorias'));
    }

    public function save($id){
      $forma = FormadePago::find($id);
      $datos = request()->all();
      $mensaje = 'La forma de pago se actualizo correctamente';
      if($forma == null){
        Session::flash('mensaje','La forma de pago no existe');
        return redirect()->to('formadepagos');
      }
      $existe = FormadePago::where('nombre',$datos['nombre'])->where('id','<>',$id)->count();

      if($existe > 0){
        FormadePago::where('nombre',$datos['nombre'])->where('id','<>',$id)->delete();
      }else{
        $forma->fill($datos);
        $forma->save();
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('formadepagos');
    }

    public function delete($id){
      $forma = FormadePago::find($id);
      $mensaje = 'La forma de pago fue borrado con exito';
      if($forma == null){
        Session::flash('mensaje','La forma de pago no existe');
        return redirect()->to('formadepagos');
      }
      $forma->delete();
      Session::flash('mensaje',$mensaje);
      return redirect()->to('formadepagos');
    }

    public function importador(){
      $datos = request()->all();
      $mensaje = 'La importacion se realizo con exito';
      $formasdepago = Excel::load($datos['archivo'])->get();
      foreach ($formasdepago as $i) {
        if(isset($i['nombre']) && isset($i['recargo'])){
          $imp = FormadePago::where('nombre',$i['nombre'])->count();
          if($imp == 0){
            FormadePago::create(array(
              'nombre' => $i['nombre'],
              'recargo' => $i['recargo']
            ));
          }else{
            $imp = FormadePago::where('nombre',$i['nombre'])->first();
            $imp->fill(array(
              'nombre' => $i['nombre'],
              'recargo' => $i['recargo']
            ));
            $imp->save();
          }
        }else{
          dd('no tiene formato');
        }

      }
      Session::flash('mensaje', $mensaje);
      return redirect()->to('formadepagos');
    }

}
