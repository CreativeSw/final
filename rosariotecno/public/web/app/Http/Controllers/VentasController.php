<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cliente;
use App\FormadePago;
use App\Producto;
use App\Stock;
use App\Moneda;
use App\Venta;
use App\User;
use App\ArticuloVenta;
use App\HistorialCaja;
use App\ProductoSucursal;
use Auth;
use Session;
use Carbon\Carbon;

class VentasController extends Controller
{
    public function indexsucursal(){
      $ventas = Venta::where('sucursal',Auth::user()->id)->orderBy('id','desc')->get();
      foreach ($ventas as $v) {
        $v['articulos'] = ArticuloVenta::where('venta',$v->id)->get();
      }
      return view('sucursal.ventas.index',compact('ventas'));
    }

    public function indexadmin(){
      $sucursales = User::where('rol','sucursal')->get(['id','direccion','telefono','rol']);
      return view('admin.ventas.panel',compact('sucursales'));
    }

    public function sucursal($id){
      $ventas = Venta::where('sucursal',$id)->orderBy('id','desc')->get();
      foreach ($ventas as $v) {
        $v['articulos'] = ArticuloVenta::where('venta',$v->id)->get();
      }
      return view('admin.ventas.index',compact('ventas'));
    }

    public function garantia(){
      $clientes = Cliente::where('tipo','cliente')->get();
      return view('sucursal.garantia.index',compact('clientes'));
    }

    public function garantiaadmin(){
      $clientes = Cliente::where('tipo','cliente')->get();
      return view('admin.garantia.index',compact('clientes'));
    }

    public function getgarantia($imei,$cliente){
      $datos = request()->all();
      $ventas = Venta::where('cliente',$cliente)->join('articulo_ventas','ventas.id','=','articulo_ventas.venta')
      ->where('imei', 'like', '%' . $imei . '%')
      ->get();
      if(count($ventas) == 0){
        $mensaje = 'El producto no fue comprado por el cliente';
      }else{
        foreach ($ventas as $v) {
          $producto = Producto::find($v->articulo);
          $inicio = Carbon::parse($v->created_at);
          $mensaje = 'El producto '.$producto->modelo.'<br> vence: '.$inicio->addMonths($producto->garantia)->format('d/m/Y');
        }
      }
      return $mensaje;
    }

    public function deletesucursal($id){
      ArticuloVenta::where('venta',$id)->delete();
      $venta = Venta::find($id);
      $venta->delete();
      Session::flash('mensaje','La venta fue borrado con exito');
      return redirect()->to('sucursal/ventas');
    }

    public function deleteproducto($id){
      $art = ArticuloVenta::find($id);
      $art->delete();
      return array('oki'=>'oki');
    }
    public function indexcreatesucursal(){
      $clientes = Cliente::where('tipo','clienteminorista')->get();
      $formasdepago = FormadePago::all();
      $productos = Producto::all();
      return view('sucursal.ventas.create',compact('clientes','productos','formasdepago'));
    }

    public function editarventasucursal($id){
      $clientes = Cliente::all();
      $formasdepago = FormadePago::all();
      $productos = Producto::all();
      $venta = Venta::find($id);
      $venta['articulos'] = ArticuloVenta::where('venta',$id)->get();
      return view('sucursal.ventas.create',compact('venta','clientes','productos','formasdepago'));
    }

    public function saveventasucursal(Request $request,$id){
      $this->validate($request,[
         '_token'=>'required',
         'formadepagos'=>'required',
         'cliente'=>'required',
         'total'=>'required',
      ]);

      $datos = request()->all();
      $venta = Venta::find($id);
      $venta->fill($datos);
      $venta->save();
      if(isset($datos['producto'])){
        foreach ($datos['producto'] as $i => $p) {
          $producto = Producto::find($p);
          $stock = Stock::where('idproducto',$p)->first();
          $stock->cantidad = $stock->cantidad - $datos['cantidad'][$i];
          $stock->save();
          ArticuloVenta::create(array(
            'articulo'=>$p,
            'producto'=>$producto->modelo,
            'cantidad'=>$datos['cantidad'][$i],
            'precio'=>$datos['precio'][$i],
            'imei'=>trim($datos['imei'][$i]),
            'venta'=>$venta->id
          ));
        }
      }
      return redirect()->to('sucursal/ventas');
    }

    public function getproducto($forma,$id){
      $existen = Stock::where('idproducto',$id)->where('idsucursal',Auth::user()->id)->first();
      $recargo = ProductoSucursal::where('idproducto',$id)->where('idsucursal',Auth::user()->id)->latest('created_at')->first();
      $recargolist = json_decode($recargo->recargo);
    //  dd($recargo);
      $totalrecargo = 0;

      foreach ($recargolist as $r) {
        $totalrecargo += $r;
      }
      $totalrecargo = $totalrecargo / $recargo->cantidad;
      //dd($totalrecargo);
      $formasdepago = FormadePago::find($forma);
      //dd($formasdepago,$existen);
      $existen->precio = $existen->precio +(($existen->precio * $formasdepago->recargo)/100);
      $existen->precio = $existen->precio +(($existen->precio * $totalrecargo)/100);


      if($existen == null){
        $existen = Stock::where('idproducto',$id)->get();
        $mensaje  = '';
        if(count($existen) > 0){
          foreach ($existen as $e) {
            $sucursal = User::find($e->idsucursal);
            $mensaje .= 'Hay existencia en '.$sucursal->nombre.'<br> <span style="font-weight:bold; ">Dirección:</span> '.$sucursal->direccion.'<br>';
          }
        }else{
          $mensaje = 'No hay en stock';
        }
        return array('mensaje'=>$mensaje);
      }
      $moneda = Moneda::find($existen->idmoneda);
      $existen->precio = $existen->precio*$moneda->cotizacion;
      return $existen;
    }

    public function createsucursal(Request $request){
      $this->validate($request,[
         '_token'=>'required',
         'formadepagos'=>'required',
         'cliente'=>'required',
         'total'=>'required',
         'producto'=>'required',
         'cantidad'=>'required',
         'imei'=>'required',
         'precio'=>'required',
      ]);
      $datos = request()->all();

      $datos['sucursal'] =Auth::user()->id;
      $cliente= Cliente::find($datos['cliente']);
      $venta = Venta::create($datos);
      $formasdepago = FormadePago::find($venta->formadepagos);
      $venta['forma']= $formasdepago->nombre;
      foreach ($datos['producto'] as $i => $p) {
        $producto = Producto::find($p);
        $stock = Stock::where('idproducto',$p)->first();
        $stock->cantidad = $stock->cantidad - $datos['cantidad'][$i];
        $stock->save();
        ArticuloVenta::create(array(
          'articulo'=>$p,
          'producto'=>$producto->modelo,
          'cantidad'=>$datos['cantidad'][$i],
          'precio'=>$datos['precio'][$i],
          'imei'=>trim($datos['imei'][$i]),
          'venta'=>$venta->id
        ));
      }
      $venta['cliente'] = $cliente;
      $venta['articulos'] = ArticuloVenta::where('venta',$venta->id)->get();
      //return view('informes.ticket', compact('venta'));
      $pdf = \PDF::loadView('informes.ticket', compact('venta'));
      return $pdf->download('ticket-'.$cliente->nombre.date('d/m/Y').'.pdf');
    }


    public function cerrarcajasucursal(){
      $total = 0;
      $articulos = ' ';

      $ventas = Venta::where('sucursal',Auth::user()->id)->where('estado','abierta')->get();
      foreach ($ventas as $v) {
        $articulos = $articulos.','.strval($v->id);
        $total += $v->total;
        $v->estado = 'cerrada';
        $v->save();
        $v['articulos'] = ArticuloVenta::where('venta',$v->id)->get();
      }

      if(count($ventas)> 0){
        $ventas[0]['totalcaja'] = $total;
        $sucursal = User::find(Auth::user()->id);
        HistorialCaja::create(array(
          'sucursal'=>$sucursal->nombre,
          'articulos'=>$articulos,
          'total'=>$total
        ));
        $pdf = \PDF::loadView('informes.cierredecaja', compact('ventas'));
        return $pdf->download('cierredecaja-'.Auth::user()->nombre.date('d/m/Y').'.pdf');
      }
      return redirect()->to('sucursal/ventas');
    }
}
