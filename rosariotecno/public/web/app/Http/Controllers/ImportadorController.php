<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Importador;
use App\Http\Requests;
use Session;
use Excel;

class ImportadorController extends Controller
{
    public function index(){
      $importadores = Importador::all();
      return view('admin.importadores.index',compact('importadores'));
    }

    public function indexcreate(){
      return view('admin.importadores.create');
    }

    public function create(){
      $datos = request()->all();
      $mensaje = 'El importador creado con exito';
      $existe = Importador::where('razonsocial',$datos['razonsocial'])->count();
      if($existe > 0){
        $mensaje = 'El importador ya existe en el sistema';
      }else{
        Importador::create($datos);
      }
      Session::flash('mensaje', $mensaje);
      return redirect()->to('importadores');
    }

    public function edit($id){
      $importador = Importador::find($id);
      if($importador == null){
        $mensaje = 'El importador que quieres editar no existe';
        Session::flash('mensaje', $mensaje);
        return redirect()->to('importadores');
      }
      return view('admin.importadores.create',compact('importador'));
    }

    public function save($id){
      $datos = request()->all();
      $importador = Importador::find($id);
      $mensaje = 'El importador fue actualizado con exito';
      if($importador == null){
        $mensaje = 'El importador que quieres editar no existe';
        Session::flash('mensaje', $mensaje);
        return redirect()->to('importadores');
      }
      $existe = Importador::where('razonsocial',$datos['razonsocial'])->where('id','<>',$id)->count();
      if($existe > 0){
        $mensaje = 'El importador ya existe en nuestra base de datos';
      }else{
        $importador->fill($datos);
        $importador->save();
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('importadores');
    }

    public function delete($id){
      $importador = Importador::find($id);
      $mensaje = 'El importador se borro con exito';
      if($importador == null){
        $mensaje = 'El Importador no puede ser borrado';
      }
      $importador->delete();
      Session::flash('mensaje',$mensaje);
      return redirect()->to('importadores');
    }

    public function importar(){

      $datos = request()->all();
      $mensaje = 'La importacion se realizo con exito';
      $importadores = Excel::load($datos['archivo'])->get();
      foreach ($importadores as $i) {
        if(isset($i['razonsocial']) && isset($i['responsable']) &&
        isset($i['direccion']) && isset($i['email']) && isset($i['telefono'])){
          $imp = Importador::where('razonsocial',$i['razonsocial'])->count();
          if($imp == 0){
            Importador::create(array(
              'razonsocial' => $i['razonsocial'],
              'responsable' => $i['responsable'],
              'direccion' => $i['direccion'],
              'email' => $i['email'],
              'telefono' => $i['telefono']
            ));
          }
        }else{
          dd('no tiene formato');
        }

      }
      Session::flash('mensaje', $mensaje);
      return redirect()->to('importadores');
    }


}
