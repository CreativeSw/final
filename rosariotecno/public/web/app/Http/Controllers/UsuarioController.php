<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
Use App\User;
use Auth;

class UsuarioController extends Controller
{
    public function index(){
      $usuarios = User::where('rol','<>','admin')->get();
      return view('admin.usuarios.index',compact('usuarios'));
    }

    public function viewcreate(){
      return view('admin.usuarios.create');
    }

    public function create(){
      $datos = request()->all();
      $datos['password'] = bcrypt($datos['password']);
      User::create($datos);
      return redirect()->to('usuarios');
    }

    public function edit($id){
      $mensaje = "El usuario no fue encontrado";
      if($id == 1){
        return redirect()->to('usuarios');
      }
      $usuario = User::find($id);
      return view('admin.usuarios.create',compact('usuario'));
    }

    public function save($id){

      //revisar cuando actualiza que sucede
      $usuario = User::find($id);
      $datos = request()->all();
      if($datos['password']= ""){
        $datos['password'] = $usuario->password;
      }else{
        $datos['password'] = bcrypt($datos['password']);
      }
      $usuario->fill($datos);
      $usuario->save();
      return redirect()->to('usuarios');
    }

    public function delete($id){
      $usuario = User::find($id);
      $mensaje = "El usuario fue borrado con exito.";
      if($usuario->rol == "admin"){
        $mensaje = "No se pudo borrar este usuario";
      }
      $usuario->delete();
      return array('success'=>$mensaje);
    }

    public function usuariodeposito(){
      return view('deposito.index');
    }

    public function configuracion(){
      return view('deposito/configuracion/index');
    }
    public function sucursalconfiguracion(){
      return view('sucursal/configuracion/index');
    }

    public function adminconfiguracion(){
      return view('admin/configuracion/index');
    }

    public function saveConfiguracion(){
      $datos = request()->all();
      $usuario = User::find(Auth::user()->id);
      $usuario->password = bcrypt($datos['password']);
      $usuario->save();
      \Session::flash('mensaje', 'Su clave de acceso de actualizo correctamente');
      return redirect()->to('deposito/configuracion');
    }
    public function sucursalsaveConfiguracion(){
      $datos = request()->all();
      $usuario = User::find(Auth::user()->id);
      $usuario->password = bcrypt($datos['password']);
      $usuario->save();
      \Session::flash('mensaje', 'Su clave de acceso de actualizo correctamente');
      return redirect()->to('sucursal/configuracion');
    }

    public function adminconfiguracionsave(){
      $datos = request()->all();
      $usuario = User::find(Auth::user()->id);
      $usuario->password = bcrypt($datos['password']);
      $usuario->save();
      \Session::flash('mensaje', 'Su clave de acceso de actualizo correctamente');
      return redirect()->to('admin/configuracion');
    }




}
