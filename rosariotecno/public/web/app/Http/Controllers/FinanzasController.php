<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\FormadePago;
use App\HistorialCaja;
use App\Venta;
use App\Categoria;
use App\ArticuloVenta;
use App\Producto;
use App\Marca;
use App\Gasto;
use App\User;
use Carbon\Carbon;

class FinanzasController extends Controller
{

    public function index(){
      $formasdepago =FormadePago::all();
      $todaslascategorias = Categoria::all();
      $ventastorta =  Venta::whereMonth('created_at', '=', date('m'))->sum('total');
      $todaslasventas = [];
      $ventas = Venta::all();
      foreach ($ventas as $v) {
        if(isset($todaslasventas[$v->created_at->format('d/m/Y')])){
          $todaslasventas[$v->created_at->format('d/m/Y')] += $v->total;
        }else{
          $todaslasventas[$v->created_at->format('d/m/Y')] = $v->total;
        }
     }
     $gastotorta = Gasto::whereMonth('created_at', '=', date('m'))->sum('costo');
      $forma = [];
      $c = '';
      $ccategoria = '';
      $categoria = [];
      $cantidadforma = [];
      $cantidadcategoria = [];
      foreach ($formasdepago as $f) {
        $ventas = Venta::where('formadepagos',$f->id)->count();
        $cantidadforma[] = $ventas;
        $forma[] = $f->grupoderecargo;
      }
      foreach ($todaslascategorias as $t) {
        $articulo = ArticuloVenta::all();
        foreach ($articulo as $a) {
          $producto = Producto::where('productos.id',$a->articulo)->join('marcas','marcas.id','=','productos.idmarca')
          ->where('marcas.idcategoria',$t->id)->count();
          $c = $t->nombre;
          $ccategoria = $producto;
        }
        $categoria[] = $c;
        $cantidadcategoria[] = $ccategoria;
      }
      return view('admin.finanzas.index', compact('todaslasventas','todaslascategorias','gastotorta','ventastorta','forma','cantidadforma','categoria','cantidadcategoria'));
    }

    public function categoriasucursal($sucursal,$id){
      $marca = Marca::where('idcategoria',$id)->get();
      $todaslascategorias = Marca::where('idcategoria',$id)->get();
      $categoria = [];
      $cantidadcategoria = [];
      foreach ($marca as $t) {
        $articulo = ArticuloVenta::join('ventas','ventas.id','=','articulo_ventas.venta')->where('ventas.sucursal',$sucursal)->get();
        foreach ($articulo as $a) {
          $producto = Producto::where('productos.id',$a->articulo)->where('idmarca',$t->id)->count();
          $c = $t->nombre;
          $ccategoria = $producto;
        }
        $categoria[] = $c;
        $cantidadcategoria[] = $ccategoria;
      }
      $ventas = Venta::where('sucursal',$sucursal)->get();
      foreach ($ventas as $v) {
        if(isset($todaslasventas[$v->created_at->format('d/m/Y')])){
          $todaslasventas[$v->created_at->format('d/m/Y')] += $v->total;
        }else{
          $todaslasventas[$v->created_at->format('d/m/Y')] = $v->total;
        }
     }
     $sucursal = 'si';
     return view('admin.finanzas.indexcategoria', compact('id','sucursal','todaslasventas','ventas','categoria','cantidadcategoria','todaslascategorias'));
    }

    public function indexcategoria($id){
      $marca = Marca::where('idcategoria',$id)->get();
      $todaslascategorias = Marca::where('idcategoria',$id)->get();
      $categoria = [];
      $cantidadcategoria = [];
      foreach ($marca as $t) {
        $articulo = ArticuloVenta::all();
        foreach ($articulo as $a) {
          $producto = Producto::where('productos.id',$a->articulo)->where('idmarca',$t->id)->count();
          $c = $t->nombre;
          $ccategoria = $producto;
        }
        $categoria[] = $c;
        $cantidadcategoria[] = $ccategoria;
      }
      $ventas = Venta::all();
      foreach ($ventas as $v) {
        if(isset($todaslasventas[$v->created_at->format('d/m/Y')])){
          $todaslasventas[$v->created_at->format('d/m/Y')] += $v->total;
        }else{
          $todaslasventas[$v->created_at->format('d/m/Y')] = $v->total;
        }
     }
     return view('admin.finanzas.indexcategoria', compact('todaslasventas','ventas','categoria','cantidadcategoria','todaslascategorias'));
    }

    public function indexmarcas($id){
      $marca = Marca::find($id);
      $cantidadcategoria = [];
      foreach ($marca as $t) {
        $productos = Producto::where('idmarca',$id)->get();
        foreach ($productos as $p) {
          $producto = ArticuloVenta::where('articulo',$p->id)->count();
          $c = $p->modelo;
          $ccategoria = $producto;
          $cantidadcategoria[$c] = $ccategoria;
        }
      }
      return view('admin.finanzas.marcas',compact('cantidadcategoria','marca'));
    }

    public function indexmarcassucursal($sucursal,$id){
      $marca = Marca::find($id);
      $cantidadcategoria = [];
      foreach ($marca as $t) {
        $productos = Producto::where('idmarca',$id)->get();
        foreach ($productos as $p) {
          $producto = ArticuloVenta::join('ventas','ventas.id','=','articulo_ventas.venta')->
          where('articulo',$p->id)->where('ventas.sucursal',$sucursal)->count();
          $c = $p->modelo;
          $ccategoria = $producto;
          $cantidadcategoria[$c] = $ccategoria;
        }
      }
      return view('admin.finanzas.marcas',compact('cantidadcategoria','marca'));
    }

    public function index_custome(){
      $datos = request()->all();
      $from = \DateTime::createFromFormat('Y-m-d', $datos['fechainicio']);
      $to = \DateTime::createFromFormat('Y-m-d', $datos['fechafin']);
      $formasdepago =FormadePago::all();
      $todaslascategorias = Categoria::all();
      $ventastorta =  Venta::whereBetween('created_at', [$from->format('Y-m-d')." 00:00:00", $to->format('Y-m-d')." 00:00:00"])->sum('total');
      $todaslasventas = [];
      $ventas = Venta::whereBetween('created_at', [$from->format('Y-m-d')." 00:00:00", $to->format('Y-m-d')." 00:00:00"])->get();
      foreach ($ventas as $v) {
        if(isset($todaslasventas[$v->created_at->format('d/m/Y')])){
          $todaslasventas[$v->created_at->format('d/m/Y')] += $v->total;
        }else{
          $todaslasventas[$v->created_at->format('d/m/Y')] = $v->total;
        }
     }
     $gastotorta = Gasto::whereBetween('created_at', [$from->format('Y-m-d')." 00:00:00", $to->format('Y-m-d')." 00:00:00"])->sum('costo');
      $forma = [];
      $categoria = [];
      $cantidadforma = [];
      $cantidadcategoria = [];
      foreach ($formasdepago as $f) {
        $ventas = Venta::where('formadepagos',$f->id)->count();
        $cantidadforma[] = $ventas;
        $forma[] = $f->grupoderecargo;
      }
      foreach ($todaslascategorias as $t) {
        $articulo = ArticuloVenta::all();
        foreach ($articulo as $a) {
          $producto = Producto::where('productos.id',$a->articulo)->join('marcas','marcas.id','=','productos.idmarca')
          ->where('marcas.idcategoria',$t->id)->count();
          $c = $t->nombre;
          $ccategoria = $producto;
        }
        $categoria[] = $c;
        $cantidadcategoria[] = $ccategoria;
      }
      return view('admin.finanzas.index', compact('todaslasventas','todaslascategorias','gastotorta','ventastorta','forma','cantidadforma','categoria','cantidadcategoria'));
    }

    public function indexcajas(){
      $sucursales = User::where('rol','sucursal')->get();
      return view('admin.finanzas.cajas',compact('sucursales'));
    }

    public function flujocaja($id){
      $formasdepago =FormadePago::all();
      $todaslascategorias = Categoria::all();
      $ventastorta =  Venta::where('sucursal',$id)->whereMonth('created_at', '=', date('m'))->sum('total');
      $totalencaja = Venta::where('sucursal',$id)->where('estado', 'abierta')->sum('total');

      $todaslasventas = [];
      $ventas = Venta::where('sucursal',$id)->get();
      foreach ($ventas as $v) {
        if(isset($todaslasventas[$v->created_at->format('d/m/Y')])){
          $todaslasventas[$v->created_at->format('d/m/Y')] += $v->total;
        }else{
          $todaslasventas[$v->created_at->format('d/m/Y')] = $v->total;
        }
     }
     $gastotorta = Gasto::whereMonth('created_at', '=', date('m'))->sum('costo');
      $forma = [];
      $categoria = [];
      $cantidadforma = [];
      $cantidadcategoria = [];
      foreach ($formasdepago as $f) {
        $ventas = Venta::where('sucursal',$id)->where('formadepagos',$f->id)->count();
        $cantidadforma[] = $ventas;
        $forma[] = $f->grupoderecargo;
      }
      foreach ($todaslascategorias as $t) {
        $articulo = ArticuloVenta::join('ventas','ventas.id','=','articulo_ventas.venta')->get();
        foreach ($articulo as $a) {
          $producto = Producto::where('productos.id',$a->articulo)->join('marcas','marcas.id','=','productos.idmarca')
          ->where('marcas.idcategoria',$t->id)->count();
          $c = $t->nombre;
          $ccategoria = $producto;
        }
        $categoria[] = $c;
        $cantidadcategoria[] = $ccategoria;
      }
      return view('admin.finanzas.cajafinanzas', compact('id','totalencaja','todaslasventas','todaslascategorias','gastotorta','ventastorta','forma','cantidadforma','categoria','cantidadcategoria'));
    }


    public function flujocajacoutome($id){
      $datos = request()->all();
      $from = \DateTime::createFromFormat('Y-m-d', $datos['fechainicio']);
      $to = \DateTime::createFromFormat('Y-m-d', $datos['fechafin']);
      $formasdepago =FormadePago::all();
      $todaslascategorias = Categoria::all();
      $ventastorta =  Venta::where('sucursal',$id)->whereBetween('created_at', [$from->format('Y-m-d')." 00:00:00", $to->format('Y-m-d')." 00:00:00"])->sum('total');
      $todaslasventas = [];
      $ventas = Venta::where('sucursal',$id)->whereBetween('created_at', [$from->format('Y-m-d')." 00:00:00", $to->format('Y-m-d')." 00:00:00"])->get();

      foreach ($ventas as $v) {
        if(isset($todaslasventas[$v->created_at->format('d/m/Y')])){
          $todaslasventas[$v->created_at->format('d/m/Y')] += $v->total;
        }else{
          $todaslasventas[$v->created_at->format('d/m/Y')] = $v->total;
        }
     }
     $gastotorta = Gasto::whereBetween('created_at', [$from->format('Y-m-d')." 00:00:00", $to->format('Y-m-d')." 00:00:00"])->sum('costo');
      $forma = [];
      $categoria = [];
      $cantidadforma = [];
      $cantidadcategoria = [];
      foreach ($formasdepago as $f) {
        $ventas = Venta::where('sucursal',$id)->where('formadepagos',$f->id)->count();
        $cantidadforma[] = $ventas;
        $forma[] = $f->grupoderecargo;
      }
      foreach ($todaslascategorias as $t) {
        $articulo = ArticuloVenta::join('ventas','ventas.id','=','articulo_ventas.venta')->get();
        foreach ($articulo as $a) {
          $producto = Producto::where('productos.id',$a->articulo)->join('marcas','marcas.id','=','productos.idmarca')
          ->where('marcas.idcategoria',$t->id)->count();
          $c = $t->nombre;
          $ccategoria = $producto;
        }
        $categoria[] = $c;
        $cantidadcategoria[] = $ccategoria;
      }
      return view('admin.finanzas.cajafinanzas', compact('id','todaslasventas','todaslascategorias','gastotorta','ventastorta','forma','cantidadforma','categoria','cantidadcategoria'));
    }
}
