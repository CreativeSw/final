<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Requests;
use App\Stock;
use App\Producto;
use App\ProductoSucursal;

class StockController extends Controller
{

    public function indexadmin(){
      $depositos = User::where('rol','deposito')->get(['id','direccion','telefono','rol']);
      $sucursales = User::where('rol','sucursal')->get(['id','direccion','telefono','rol']);
      foreach ($sucursales as $s) {
        $stocks = Stock::where('idsucursal',$s->id)->join('productos','productos.id','=','stocks.idproducto')->first();
        if($stocks != null){
          if($stocks->cantidad < $stocks->stockminimo){
            $s['estado'] = 'faltante';
          }
        }
      }
      foreach ($depositos as $s) {
        $stocks = Stock::where('idsucursal',$s->id)->join('productos','productos.id','=','stocks.idproducto')->first();
        if($stocks != null){
          if($stocks->cantidad < $stocks->stockminimo){
            $s['estado'] = 'faltante';
          }
        }
      }
      return view('admin.stocks.index',compact('depositos','sucursales'));
    }

    public function stocklocal($id){
      $stocks = Stock::where('idsucursal',$id)->get();
      foreach ($stocks as $s) {
        $producto = Producto::find($s->idproducto);
        $s['producto'] = $producto->modelo;
        $s['stockminimo'] = $producto->stockminimo;
      }
      $sucursal = $id;
      return view('admin.stocks.detalle',compact('stocks','sucursal'));
    }

    public function depostio(){
      $stocks = Stock::where('idsucursal',Auth::user()->id)->get();
      $imies = [];

      foreach ($stocks as $s) {
        $psucursal = ProductoSucursal::where('idproducto',$s->idproducto)
        ->where('idsucursal',Auth::user()->id)->get();
        //dd($psucursal);
        $imies = [];
        $imeislist = [];
        foreach ($psucursal as $v) {
          $imies = preg_split('/\r\n/',$v->imeis);
          foreach ($imies as $k => $v) {
            if($v == "")
              unset($imies[$k]);
          }
          foreach ($imies as $i) {
            $imeislist[] = $i;
          }
        }

        $stockovido =  ProductoSucursal::where('idproducto',$s->idproducto)
        ->where('idorigensucursal',Auth::user()->id)->where('tipo','movidos')->get();
        $imeismovidos = [];

        foreach ($stockovido as $v) {
          $imeismovidos = preg_split('/\r\n/',$v->imeis);
          foreach ($imeismovidos as $k => $v) {
            if($v == "")
              unset($imeismovidos[$k]);
          }
        }

        foreach ($imeislist as $i => $imei) {
          if(array_search($imei,$imeismovidos)!== false ){
            unset($imeislist[$i]);
          }
        }
        $producto = Producto::find($s->idproducto);
        $s['producto'] = $producto->modelo;
        $s['stockminimo'] = $producto->stockminimo;
        $s['imeis'] = $imeislist;
      }
      return view('deposito.stock.index',compact('stocks'));
    }

    public function sucursal(){
      $stocks = Stock::where('idsucursal',Auth::user()->id)->get();
      foreach ($stocks as $s) {
      //BUSCA INGRESOS EN ESA SUCURSAL
        $psucursal = ProductoSucursal::where('idproducto',$s->idproducto)
        ->where('idsucursal',Auth::user()->id)->get();
        $imies = [];
        $imeislist = [];
        foreach ($psucursal as $v) {
          $imies = preg_split('/\r\n/',$v->imeis);
          foreach ($imies as $k => $v) {
            if($v == "")
              unset($imies[$k]);
          }
          foreach ($imies as $i) {
            $imeislist[] = $i;
          }
        }
        //BUSCA MOVIDOS DE OTRAS SUCURSAL
        $stockovido =  ProductoSucursal::where('idproducto',$s->idproducto)
        ->where('idorigensucursal',Auth::user()->id)->where('tipo','movidos')->get();
        $imeismovidos= [];
        foreach ($stockovido as $v) {
          $ims = preg_split('/\r\n/',$v->imeis);
          foreach ($ims as $k => $v) {
            if($v == ""){
              unset($imeismovidos[$k]);
            }else{
              $imeismovidos[] = $v;
            }
          }
        }

        foreach ($imeislist as $i => $imei) {
          if(array_search($imei,$imeismovidos)!== false ){
            unset($imeislist[$i]);
          }
        }

        $producto = Producto::find($s->idproducto);
        $s['producto'] = $producto->modelo;
        $s['stockminimo'] = $producto->stockminimo;
        $s['imeis'] = $imeislist;
      }
      return view('sucursal.stock.index',compact('stocks'));
    }


    public function informes(){
      $stocks = Stock::where('idsucursal',Auth::user()->id)->get();
      //dd('acaa');
      foreach ($stocks as $s) {
        /*


        $psucursal = ProductoSucursal::where('idproducto',$s->idproducto)->where('idsucursal',Auth::user()->id)->get();
        $imies = [];
        $imeislist = [];
        foreach ($psucursal as $v) {
          $imies = preg_split('/\r\n/',$v->imeis);
          foreach ($imies as $k => $p) {
            if($p == "")
              unset($imies[$k]);
          }
          foreach ($imies as $i) {
            $imeislist[] = $i;
          }
          $stockovido =  ProductoSucursal::where('idproducto',$v->idproducto)
          ->where('idorigensucursal',Auth::user()->id)->where('tipo','movidos')->get();

          foreach ($stockovido as $v) {
            $imeismovidos = preg_split('/\r\n/',$v->imeis);
            foreach ($imeismovidos as $k => $v) {
              if($v == "")
                unset($imeismovidos[$k]);
            }
          }
        }
        foreach ($imeislist as $i => $imei) {
          if(array_search($imei,$imeismovidos)!== false ){
            unset($imeislist[$i]);
          }
        }
        $imeis =  '';
        foreach ($imeislist as $i) {
          $imeis = $imeis.','.$i;
        }
        */
        $psucursal = ProductoSucursal::where('idproducto',$s->idproducto)
        ->where('idsucursal',Auth::user()->id)->get();
        $imies = [];
        $imeislist = [];
        foreach ($psucursal as $v) {
          $imies = preg_split('/\r\n/',$v->imeis);
          foreach ($imies as $k => $v) {
            if($v == "")
              unset($imies[$k]);
          }
          foreach ($imies as $i) {
            $imeislist[] = $i;
          }
        }
        //BUSCA MOVIDOS DE OTRAS SUCURSAL
        $stockovido =  ProductoSucursal::where('idproducto',$s->idproducto)
        ->where('idorigensucursal',Auth::user()->id)->where('tipo','movidos')->get();
        $imeismovidos= [];
        foreach ($stockovido as $v) {
          $ims = preg_split('/\r\n/',$v->imeis);
          foreach ($ims as $k => $v) {
            if($v == ""){
              unset($imeismovidos[$k]);
            }else{
              $imeismovidos[] = $v;
            }
          }
        }

        foreach ($imeislist as $i => $imei) {
          if(array_search($imei,$imeismovidos)!== false ){
            unset($imeislist[$i]);
          }
        }

        $imeis =  '';
        foreach ($imeislist as $i) {
          $imeis = $imeis.','.$i;
        }

        $producto = Producto::find($s->idproducto);
        $s['producto'] = $producto->modelo;
        $s['imeis'] = $imeis;
        $s['stockminimo'] = $producto->stockminimo;
      }
      $pdf = \PDF::loadView('informes.stock', compact('stocks'))->setPaper('a4');
      return $pdf->download('stock-'.date('d/m/Y').'.pdf');
    }

    public function informessucursal($id){

      $user = User::find($id);
      $stocks = Stock::where('idsucursal',$id)->get();
      foreach ($stocks as $s) {
        $detalle = User::find($s->idsucursal);
        $ps = ProductoSucursal::where('idproducto',$s->idproducto)->where('idsucursal',$id)->get(['imeis']);
        $imeis = '';
        foreach ($ps as $i) {
          $imeis = $imeis.','.$i['imeis'];
        }
      //  dd($ps);
        $imeis = substr($imeis,1,-1);
        $producto = Producto::find($s->idproducto);
        $s['producto'] = $producto->modelo;
        $s['sucursal'] = $detalle;
        $s['imeis'] = $imeis;
        $s['stockminimo'] = $producto->stockminimo;
      }

      $pdf = \PDF::loadView('informes.stockadmin', compact('stocks','user'))->setPaper('a4');
      return $pdf->download('stocksucursal-'.date('d/m/Y').'.pdf');
    }

    public function informegeneral(){

      $stocks = Stock::get();
      /*
      foreach ($stocks as $s) {
        $detalle = User::find($s->idsucursal);

        $producto = Producto::find($s->idproducto);
        $s['sucursal'] = $detalle;
        $s['producto'] = $producto->modelo;
        $s['stockminimo'] = $producto->stockminimo;
      }*/

      foreach ($stocks as $s) {
        $detalle = User::find($s->idsucursal);

        $ps = ProductoSucursal::where('idproducto',$s->idproducto)->where('idsucursal',$s->idsucursal)->get(['imeis']);
        $imeis = '';
        foreach ($ps as $i) {
          $imeis = $imeis.','.$i['imeis'];
        }
        $imeis = substr($imeis,1,-1);
        $producto = Producto::find($s->idproducto);
        $s['producto'] = $producto->modelo;
        $s['sucursal'] = $detalle;
        $s['imeis'] = $imeis;
        $s['stockminimo'] = $producto->stockminimo;
      }





      $pdf = \PDF::loadView('informes.stockgeneral', compact('stocks'))->setPaper('a4');
      return $pdf->download('stockAdmin'.date('d/m/Y').'.pdf');
    }


}
