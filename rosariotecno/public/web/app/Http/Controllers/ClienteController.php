<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cliente;
use Session;
use Auth;
use Excel;
use Illuminate\Support\Facades\URL;

class ClienteController extends Controller
{


    public function index(){
      $clientes = Cliente::where('tipo','cliente')->get();
      return view('admin.clientes.index',compact('clientes'));
    }
    public function indexempleado(){
        $clientes = Cliente::where('tipo','empleado')->get();
        return view('admin.empleado.index',compact('clientes'));
    }

    public function indexcreateempleado(){
      return view('admin.empleado.create');
    }

    public function indexcreate(){
      if(Auth::user()->rol== "admin"){
          return view('admin.clientes.create');
      }else{
        return view('sucursal.clientes.create');
      }
    }


    public function create(){
      $datos = request()->all();
      $mensaje = 'EL cliente fue creado con exito';
      $existe = Cliente::where('email',$datos['email'])->where('nombre',$datos['nombre'])
      ->count();
      if($existe >0 ){
        $mensaje= "El Cliente ya existe";
      }else{
        Cliente::create($datos);
      }
      Session::flash('mensaje',$mensaje);
      if(Auth::user()->rol == "admin"){
        return redirect()->to('clientes');
      }else{
        return redirect()->to('sucursal/clientes');
      }
    }

    public function createempleado(){
      $datos = request()->all();
      $mensaje = 'El empleado fue creado con exito';
      $existe = Cliente::where('email',$datos['email'])->where('nombre',$datos['nombre'])
      ->count();
      if($existe >0 ){
        $mensaje= "El emplado ya existe";
      }else{
        Cliente::create($datos);
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('empleados');
    }

    public function indexsucursal(){
      $clientes = Cliente::all();
      return view('sucursal.clientes.index',compact('clientes'));
    }

    public function edit($id){

      $url = URL::current();
      $cliente = Cliente::find($id);
      if($cliente == null){
        Session::flash('mensaje','El CLiente no existe');
        if(strpos($url,'sucursal')!= false){
          return redirect()->to('sucursal/clientes');
        }else{
          return redirect()->to('clientes');
        }
      }

      if(strpos($url,'sucursal')!= false){
        return view('sucursal.clientes.create',compact('cliente'));
      }else{
        return view('admin.clientes.create',compact('cliente'));
      }
    }

    public function editempleado($id){
      $cliente = Cliente::find($id);
      if($cliente == null){
        Session::flash('mensaje','El empleado no existe');
        return redirect()->to('empleados');
      }
      return view('admin.empleado.create',compact('cliente'));
    }


    public function save($id){
      $datos = request()->all();
      //dd($datos);
      $url = URL::current();
      $cliente = Cliente::find($id);
      $mensaje = 'El cliente se actualizo con exito';
      if($cliente == null){
        Session::flash('mensaje','El Cliente no existe');
        if(strpos($url,'sucursal')!= false){
          return redirect()->to('sucursal/clientes');
        }else{
          return redirect()->to('clientes');
        }
      }
      $existe = Cliente::where('email',$datos['email'])->where('nombre',$datos['nombre'])
      ->where('id','<>',$cliente->id)->count();
      if($existe > 0){
        $mensaje='El cliente ya existe en el sistema';
      }else{
        $cliente->fill($datos);
        $cliente->save();
      }

      Session::flash('mensaje',$mensaje);
      if(strpos($url,'sucursal')!= false){
        return redirect()->to('sucursal/clientes');
      }else{
        return redirect()->to('clientes');
      }

    }

    public function saveempleado($id){
      $datos = request()->all();
      $cliente = Cliente::find($id);
      $mensaje = 'El empleado se actualizo con exito';
      if($cliente == null){
        Session::flash('mensaje','El empleado no existe');
        return redirect()->to('empleados');
      }
      $existe = Cliente::where('email',$datos['email'])->where('nombre',$datos['nombre'])
      ->where('id','<>',$cliente->id)->count();
      if($existe > 0){
        $mensaje='El empleado ya existe en el sistema';
      }else{
        $cliente->fill(array(
          'nombre'=>$datos['nombre'],
          'email'=>$datos['email'],
          'direccion'=>$datos['direccion'],
          'telefono' =>$datos['telefono'],
          'tipo'=>'empleado'
        ));
        $cliente->save();
      }

      Session::flash('mensaje',$mensaje);
      return redirect()->to('empleados');
    }


    public function delete($id){
      $cliente = Cliente::find($id);
      $url = URL::current();
      $mensaje = 'El cliente fue eliminado con exito';
      if($cliente == null){
        $mensaje = 'No se puede eliminar el cliente';
      }
      $cliente->delete();
      Session::flash('mensaje',$mensaje);
      if(strpos($url,'sucursal')!=false){
        return redirect()->to('sucursal/clientes');
      }else{
        return redirect()->to('clientes');
      }
    }


    public function deleteempleado($id){
        $cliente = Cliente::find($id);
        $mensaje = 'El empleado fue eliminado con exito';
        if($cliente == null){
          $mensaje = 'No se puede eliminar el empleado';
        }
        $cliente->delete();
        Session::flash('mensaje',$mensaje);
        return redirect()->to('empleados');
    }

    public function importar(){
      $datos = request()->all();
      $url = URL::current();
      $mensaje = 'La importacion se realizo con exito';
      $clientes = Excel::load($datos['archivo'])->get();
      foreach ($clientes as $c) {
        if(isset($c['nombre']) && isset($c['email']) && isset($c['direccion'])
        && isset($c['telefono']) ){
          $cate = Cliente::where('nombre',$c['nombre'])->count();
          if($cate == 0){
            Cliente::create(array(
              'nombre'=>($c['nombre'] == null) ? '' :$c['nombre'],
              'email'=>($c['email']==null)?'':$c['email'],
              'direccion'=>($c['direccion'] == null)?'':$c['direccion'],
              'telefono'=>($c['telefono']==null)?'':$c['telefono'],
              'tipo'=>'cliente'
            ));
          }else{
            $cate = Cliente::where('nombre',$c['nombre'])->first();
            $cate->fill(array(
              'nombre'=>($c['nombre'] == null)? '':$c['nombre'],
              'email'=>($c['email']==null)?'':$c['email'],
              'direccion'=>($c['direccion'] == null)?'':$c['direccion'],
              'telefono'=>($c['telefono']==null)?'':$c['telefono'],
              'sucursal'=>Auth::user()->id,
              'tipo'=>'cliente'
            ));
            $cate->save();
          }
        }else{
          dd('no tiene formato');
        }
      }

      Session::flash('mensaje', $mensaje);
      if(strpos($url,'sucursal')!= false){
        return redirect()->to('sucursal/clientes');
      }else{
        return redirect()->to('clientes');
      }
    }

    public function importarempleado(){
      $datos = request()->all();
      $mensaje = 'La importacion se realizo con exito';
      $clientes = Excel::load($datos['archivo'])->get();
      foreach ($clientes as $c) {
        if(isset($c['nombre']) && isset($c['email']) && isset($c['direccion'])
        && isset($c['telefono']) ){
          $cate = Cliente::where('nombre',$c['nombre'])->count();
          if($cate == 0){
            Cliente::create(array(
              'nombre'=>($c['nombre'] == null) ? '' :$c['nombre'],
              'email'=>($c['email']==null)?'':$c['email'],
              'direccion'=>($c['direccion'] == null)?'':$c['direccion'],
              'telefono'=>($c['telefono']==null)?'':$c['telefono'],
              'tipo'=>'empleado'
            ));
          }else{
            $cate = Cliente::where('nombre',$c['nombre'])->first();
            $cate->fill(array(
              'nombre'=>($c['nombre'] == null)? '':$c['nombre'],
              'email'=>($c['email']==null)?'':$c['email'],
              'direccion'=>($c['direccion'] == null)?'':$c['direccion'],
              'telefono'=>($c['telefono']==null)?'':$c['telefono'],
              'sucursal'=>Auth::user()->id,
              'tipo'=>'empleado'
            ));
            $cate->save();
          }
        }else{
          dd('no tiene formato');
        }
      }

      Session::flash('mensaje', $mensaje);
      return redirect()->to('empleados');
    }

    public function exportar(){
      $cliente = Cliente::get(['nombre','email','direccion','telefono']);
      return  Excel::create('clientes', function($excel) use ($cliente) {
        $excel->sheet('mySheet', function($sheet) use ($cliente)
        {
            $sheet->fromArray($cliente);
        });
      })->download('xls');
    }

    public function exportarempleado(){
      $cliente = Cliente::get(['nombre','email','direccion','telefono']);
      return  Excel::create('empleados', function($excel) use ($cliente) {
        $excel->sheet('mySheet', function($sheet) use ($cliente){
            $sheet->fromArray($cliente);
        });
      })->download('xls');
    }

}
