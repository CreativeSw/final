<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Categoria;
use App\Marca;
use App\User;
use App\Producto;
use App\Moneda;
use App\ProductoSucursal;
use App\Stock;
use App\Importador;
use Session;
use Auth;

class MercaderiaController extends Controller
{
    public function indexdeposito(){
      $ingresos = ProductoSucursal::where('idsucursal',Auth::user()->id)
      ->where("tipo", "ingreso")->get();
      foreach ($ingresos as $i) {
        $producto = Producto::find($i->idproducto);
        $marca = Marca::find($producto->idmarca);
        $categoria= Categoria::find($marca->idcategoria);
        $i['producto']= $producto->modelo;
        $i['marca'] = $marca->nombre;
        $i['categoria'] = $categoria->nombre;
      }
      return view('deposito.mercaderia.index',compact('ingresos'));
    }

    public function indexsucursal(){
      $ingresos = ProductoSucursal::where('idsucursal',Auth::user()->id)
      ->where("tipo", "ingreso")->get();
      foreach ($ingresos as $i) {
        $producto = Producto::find($i->idproducto);
        $marca = Marca::find($producto->idmarca);
        $categoria= Categoria::find($marca->idcategoria);
        $i['producto']= $producto->modelo;
        $i['marca'] = $marca->nombre;
        $i['categoria'] = $categoria->nombre;
      }
      return view('sucursal.mercaderia.index',compact('ingresos'));
    }

    public function ingresodeposito(){
      $categorias = Categoria::all();
      $monedas = Moneda::all();
      $importadores = Importador::all();
      return view('deposito.mercaderia.create',compact('categorias','monedas','importadores'));
    }

    public function ingresosucursal(){
      $categorias = Categoria::all();
      $monedas = Moneda::all();
      $importadores =  Importador::all();
      return view('sucursal.mercaderia.create',compact('categorias','monedas','importadores'));
    }

    public function createdeposito(){
      $datos = request()->all();
      $existencias = ProductoSucursal::where('idproducto',$datos['idproducto'])->get();
      $imeislist = [];
      foreach ($existencias as $e) {
        $imeislist = preg_split('/\r\n/',$e->imeis);
        foreach ($imeislist as $i => $imei) {
          if($imei == "")
          unset($imeislist[$i]);
        }
      }

      $m = preg_split('/\r\n/',$datos['imeis']);
      foreach ($m as $i => $ex) {
        if($ex == "")
        unset($m[$i]);
      }

      foreach ($m as $k) {
        if(array_search($k,$imeislist)!== false ){
            $mensaje = 'El IMEI fue ingresado con anterioridad';
            Session::flash('mensaje',$mensaje);
            return redirect()->to('deposito/mercaderia');
        }
      }

      $datos['idsucursal'] = Auth::user()->id;
      $mensaje = 'En ingreso fue ralizado con exito';
      $producto = Producto::find($datos['idproducto']);
      if($producto == null){
        $mensaje = 'El producto que intentas cargar no existe';
        Session::flash('mensaje',$mensaje);
        return redirect()->to('deposito/mercaderia');
      }
      $datos['idsucursal'] = $datos['idsucursal'];
      if($datos['cantidad']<1){
        $mensaje = 'Debe ingresar al menos una unidad';
        Session::flash('mensaje',$mensaje);
        return redirect()->to('deposito/mercaderia');
      }
      if($datos['precio'] < 0.00){
        $mensaje = 'El precio del producto debe ser mayo a 0';
        Session::flash('mensaje',$mensaje);
        return redirect()->to('deposito/mercaderia');
      }
      if($datos['idcosto'] == "VARIABLE"){
        $datos['recargo'] = json_encode($datos['recargo']);
      }else{
        $datos['recargo'] = json_encode($datos['fijorecargo']);
      }
      ProductoSucursal::create($datos);
      $existe = Stock::where('idproducto',$datos['idproducto'])->where('idsucursal',$datos['idsucursal'])->get();
      if(count($existe)> 0){
        $stock = Stock::where('idproducto',$datos['idproducto'])->where('idsucursal',$datos['idsucursal'])->first();
        $datos['cantidad'] += $stock->cantidad;
        $stock->fill(array('cantidad'=>$datos['cantidad'], 'precio'=>$datos['precio']));;
        $stock->save();
      }else{
        $stock = Stock::create($datos);
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('deposito/mercaderia');
    }

    public function createsucursal(){
      $datos = request()->all();
    //  dd($datos);
      $datos['idsucursal'] = Auth::user()->id;
      $mensaje = 'En ingreso fue ralizado con exito';
      $producto = Producto::find($datos['idproducto']);
      if($producto == null){
        $mensaje = 'El producto que intentas cargar no existe';
      }
      $datos['idsucursal'] = $datos['idsucursal'];
      if($datos['cantidad']<1){
        $mensaje = 'Debe ingresar al menos una unidad';
        Session::flash('mensaje',$mensaje);
        return redirect()->to('sucursal/mercaderia');
      }
      if($datos['precio'] < 0.00){
        $mensaje = 'El precio del producto debe ser mayo a 0';
        Session::flash('mensaje',$mensaje);
        return redirect()->to('sucursal/mercaderia');
      }
      if($datos['idcosto'] == "VARIABLE"){
        $datos['recargo'] = json_encode($datos['recargo']);
      }else{
        $datos['recargo'] = json_encode($datos['fijorecargo']);
      }
      ProductoSucursal::create($datos);
      $existe = Stock::where('idproducto',$datos['idproducto'])->where('idsucursal',$datos['idsucursal'])->get();
      if(count($existe)> 0){
        $stock = Stock::where('idproducto',$datos['idproducto'])->where('idsucursal',$datos['idsucursal'])->first();
        $datos['cantidad'] += $stock->cantidad;
        $stock->fill(array('cantidad'=>$datos['cantidad'], 'precio'=>$datos['precio']));;
        $stock->save();
      }else{
        $stock = Stock::create($datos);
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('sucursal/mercaderia');
   }

    public function editdeposito($id){
      $productosucursal = ProductoSucursal::find($id);
      if($productosucursal == null){
        Session::flash('mensaje','El ingreso no existe');
      }
      $producto = Producto::find($productosucursal->idproducto);
      $productosucursal['producto'] = $producto->modelo;
      $marca = Marca::find($producto->idmarca);
      $productosucursal['idmarca'] = $marca->id;
      $productosucursal['marca'] = $marca->nombre;
      $categoria = Categoria::find($marca->idcategoria);
      $productosucursal['idcategoria'] = $categoria->id;
      $productosucursal['categoria'] = $categoria->nombre;
      $categorias = Categoria::all();
      $destinos = User::where('rol','deposito')->orWhere('rol','sucursal')->get();
      $monedas  = Moneda::all();
      $importadores = Importador::all();
      return view('deposito.mercaderia.create',compact('productosucursal','categorias',
      'destinos','monedas','importadores'));
    }

    public function editsucursal($id){
      $productosucursal = ProductoSucursal::find($id);
      if($productosucursal == null){
        Session::flash('mensaje','El ingreso no existe');
      }
      $producto = Producto::find($productosucursal->idproducto);
      $productosucursal['producto'] = $producto->modelo;
      $marca = Marca::find($producto->idmarca);
      $productosucursal['idmarca'] = $marca->id;
      $productosucursal['marca'] = $marca->nombre;
      $categoria = Categoria::find($marca->idcategoria);
      $productosucursal['idcategoria'] = $categoria->id;
      $productosucursal['categoria'] = $categoria->nombre;
      $categorias = Categoria::all();
      $destinos = User::where('rol','deposito')->orWhere('rol','sucursal')->get();
      $monedas  = Moneda::all();
      $categorias = Categoria::all();
      $importadores = Importador::all();
      return view('sucursal.mercaderia.create',compact('productosucursal','categorias','destinos','monedas','importadores'));
    }

    public function editdepositomover($id){
      $productos = Stock::where('idsucursal',Auth::user()->id)->get();
      $producto = ProductoSucursal::find($id);
      foreach ($productos as $p) {
        $pro = Producto::find($p->idproducto);
        $p['producto'] = $pro->modelo;
      }

      $destinos = User::where('id','<>',Auth::user()->id)->where('id','<>',1)->get();
      $monedas  = Moneda::all();
      return view('deposito.movermercaderia.create',compact('productos','destinos','monedas','producto'));
    }

    public function editsucursalmover($id){
      $productos = Stock::where('idsucursal',Auth::user()->id)->get();
      $producto = ProductoSucursal::find($id);
      foreach ($productos as $p) {
        $pro = Producto::find($p->idproducto);
        $p['producto'] = $pro->modelo;
      }
      $destinos = User::where('id','<>',Auth::user()->id)->where('id','<>',1)->get();
      $monedas  = Moneda::all();
      return view('sucursal.movermercaderia.create',compact('productos','destinos','monedas','producto'));
    }

    public function savedeposito(Request $request, $id){
      $this->validate($request,[
         '_token'=>'required',
         'idcategoria'=>'required',
         'idmarca'=>'required',
         'cantidad'=>'required',
         'idmoneda'=>'required',
         'precio'=>'required',
       ]);
       $datos = request()->all();
       $existe = ProductoSucursal::where('id','<>',$id)->get(['imeis']);
       $imeislist =[];
       foreach ($existe as $e) {
         $im =  preg_split('/\r\n/',$e['imeis']);
         foreach ($im as $i => $m) {
           if($m != "")
              $imeislist[] = $m;
          }
       }
       $imeisposibles =  preg_split('/\r\n/',$datos['imeis']);
       foreach ($imeisposibles as $i => $v) {
         if($v == "")
          unset($imeisposibles[$i]);
       }

       foreach ($imeisposibles as $posible) {
         $existe = array_search($posible,$imeislist);
         if($existe !== false){
           Session::flash('mensaje','El IMEI: '.$posible.' fue ingresado con anterioridad');
           return redirect()->to('deposito/mercaderia');
         }
       }

       $datos['idsucursal'] = Auth::user()->id;
       $mensaje = 'El ingreso de mercaderia fue exitoso';
       if($datos['idproducto']==""){
         Session::flash('mensaje','No seleciono producto a actualizar');
         return redirect()->to('deposito/mercaderia');
       }
       $ingreso = ProductoSucursal::find($id);
       if($ingreso->idcosto == $datos['idcosto'] ){
         if(isset($datos['recargo']) || isset($datos['fijorecargo'])){
           if($datos['idcosto'] == "FIJO"){
             $recargo = json_encode($datos['fijorecargo']);
             $ingreso->recargo = $recargo;
             $ingreso->save();
           }else{
             $recargo = json_encode($datos['recargo']);
             $ingreso->recargo = $recargo;
             $ingreso->save();
           }
         }
       }else{
         if(isset($datos['recargo']) || isset($datos['fijorecargo'])){
         if($datos['idcosto'] == "FIJO"){
           $recargo = json_encode($datos['fijorecargo']);
           $ingreso->recargo = $recargo;
           $ingreso->save();
         }else{
           $recargo = json_encode($datos['recargo']);
           $ingreso->recargo = $recargo;
           $ingreso->save();
         }
        }
       }
       unset($datos['recargo']);
       $ingreso->save();
       $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',$datos['idsucursal'])->first();
       $stock->cantidad  = $stock->cantidad - $ingreso->cantidad;
       $stock->save();
       $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',$datos['idsucursal'])->first();
       $stock->cantidad = $stock->cantidad + $datos['cantidad'];
       $stock->save();
       $ingreso->fill($datos);
       $ingreso->save();
       Session::flash('mensaje',$mensaje);
       return redirect()->to('deposito/mercaderia');
    }

    public function savesucursal(Request $request,$id){
      $this->validate($request,[
         '_token'=>'required',
         'idcategoria'=>'required',
         'idmarca'=>'required',
         'cantidad'=>'required',
         'idmoneda'=>'required',
         'precio'=>'required',
       ]);
       $datos = request()->all();
       $datos['idsucursal'] = Auth::user()->id;
       $mensaje = 'El ingreso de mercaderia fue exitoso';
       if($datos['idproducto']==""){
         Session::flash('mensaje','No seleciono producto a actualizar');
         return redirect()->to('mercaderia/ingreso');
       }
       $ingreso = ProductoSucursal::find($id);
       if($ingreso->idcosto == $datos['idcosto'] ){
         if(isset($datos['recargo']) || isset($datos['fijorecargo'])){
           if($datos['idcosto'] == "FIJO"){
             $recargo = json_encode($datos['fijorecargo']);
             $ingreso->recargo = $recargo;
             $ingreso->save();
           }else{
             $recargo = json_encode($datos['recargo']);
             $ingreso->recargo = $recargo;
             $ingreso->save();
           }
         }
       }else{
         if(isset($datos['recargo']) || isset($datos['fijorecargo'])){
         if($datos['idcosto'] == "FIJO"){
           $recargo = json_encode($datos['fijorecargo']);
           $ingreso->recargo = $recargo;
           $ingreso->save();
         }else{
           $recargo = json_encode($datos['recargo']);
           $ingreso->recargo = $recargo;
           $ingreso->save();
         }
        }
       }
       unset($datos['recargo']);
       $ingreso->save();
       $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',$datos['idsucursal'])->first();
       $stock->cantidad  = $stock->cantidad - $ingreso->cantidad;
       $stock->save();
       $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',$datos['idsucursal'])->first();
       $stock->cantidad = $stock->cantidad + $datos['cantidad'];
       $stock->save();
       $ingreso->fill($datos);
       $ingreso->save();
       Session::flash('mensaje',$mensaje);
       return redirect()->to('sucursal/mercaderia');
    }


    public function deletedeposito($id){
      $ingreso = ProductoSucursal::find($id);
      $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',Auth::user()->id)->first();
      $movidos = ProductoSucursal::where('idorigensucursal',Auth::user()->id)->where('idproducto',$ingreso->idproducto)->where('tipo','movidos')->get();
      $imeislist =  preg_split('/\r\n/',$ingreso->imeis);
      foreach ($imeislist as $i => $imei) {
        if($imei == "")
        unset($imeislist[$i]);
      }
      $imeis = [];
      foreach ($movidos as $i => $im) {
        $m = preg_split('/\r\n/',$im->imeis);
        foreach ($m as $k) { //
          if($k != "" && (array_search($k,$imeis) == false))
          $imeis[] = $k;
        }
      }
      $resto = 0;
      foreach ($imeis as $i) {
        if(array_search($i,$imeislist)){
          $resto++;
        }
      }
      $aux = $stock->cantidad - $ingreso->cantidad + $resto;
      if($aux <= 0){
        $stock->cantidad = 0;
      }else{
        $stock->cantidad  = $stock->cantidad - $ingreso->cantidad;
      }
      $stock->save();
      $ingreso->delete();
      return redirect()->to('deposito/mercaderia');
    }

    public function deletesucursal($id){
      $ingreso = ProductoSucursal::find($id);
      $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',Auth::user()->id)->first();
      $stock->cantidad  = $stock->cantidad - $ingreso->cantidad;
      $stock->save();
      $ingreso->delete();
      return redirect()->to('sucursal/mercaderia');
    }

    public function deletemoverborrar($id){
      $ingreso = ProductoSucursal::find($id);
      if($ingreso == null){
          return redirect()->to('deposito/movermercaderia');
      }
      $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',Auth::user()->id)->first();
      $stock->cantidad  = $stock->cantidad + $ingreso->cantidad;
      $stock->save();
      $stock2 = Stock::where('idsucursal',$ingreso->idsucursal)->where('idproducto',$ingreso->idproducto)->first();
      $stock2->cantidad = $stock2->cantidad - $ingreso->cantidad;
      $stock2->save();
      $ingreso->delete();
      return redirect()->to('deposito/movermercaderia');
    }

    public function deletemoverbosucursal($id){
      $ingreso = ProductoSucursal::find($id);
      if($ingreso == null){
          return redirect()->to('sucursal/movermercaderia');
      }
      $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',Auth::user()->id)->first();
      $stock->cantidad  = $stock->cantidad + $ingreso->cantidad;
      $stock->save();
      $stock2 = Stock::where('idsucursal',$ingreso->idsucursal)->where('idproducto',$ingreso->idproducto)->first();
      $stock2->cantidad = $stock2->cantidad - $ingreso->cantidad;
      $stock2->save();
      $ingreso->delete();
      return redirect()->to('sucursal/movermercaderia');
    }

    public function panel(){
      return view('admin.ingresomercaderia.panel');
    }

    public function index(){
      $ingresos = ProductoSucursal::all();
      foreach ($ingresos as $i) {
        $producto = Producto::find($i->idproducto);
        $marca = Marca::find($producto->idmarca);
        $categoria= Categoria::find($marca->idcategoria);
        $i['producto']= $producto->modelo;
        $i['marca'] = $marca->nombre;
        $i['categoria'] = $categoria->nombre;
      }
      return view('admin.ingresomercaderia.index',compact('ingresos'));
    }

    public function indexmover(){
      $ingresos = [];
      return view('admin.ingresomercaderia.indexmover',compact('ingresos'));
    }

    public function indexmovercreate(){
      $categorias = Categoria::all();
      $destinos = User::where('rol','deposito')->orWhere('rol','sucursal')->get();
      $monedas  = Moneda::all();
      return view('admin.ingresomercaderia.createmover',compact('categorias','destinos','monedas'));
    }

    public function indexcreate(){
      $categorias = Categoria::all();
      $importadores = Importador::all();
      $destinos = User::where('rol','deposito')->orWhere('rol','sucursal')->get();
      $monedas  = Moneda::all();
      return view('admin.ingresomercaderia.create',compact('categorias','destinos','monedas','importadores'));
    }

    public function create(){
      $datos = request()->all();
      //dd($datos);
      $mensaje = 'En ingreso fue ralizado con exito';
      $producto = Producto::find($datos['idproducto']);
      if($producto == null){
        $mensaje = 'El producto que intentas cargar no existe';
      }
      $datos['idsucursal'] = $datos['idsucursal'];
      if($datos['cantidad']<1){
        $mensaje = 'Debe ingresar al menos una unidad';
        Session::flash('mensaje',$mensaje);
        return redirect()->to('sucursal/mercaderia');
      }
      if($datos['precio'] < 0.00){
        $mensaje = 'El precio del producto debe ser mayo a 0';
        Session::flash('mensaje',$mensaje);
        return redirect()->to('sucursal/mercaderia');
      }
      if($datos['idcosto'] == "VARIABLE"){
        $datos['recargo'] = json_encode($datos['recargo']);
      }else{
        $datos['recargo'] = json_encode($datos['fijorecargo']);
      }
      ProductoSucursal::create($datos);
      $existe = Stock::where('idproducto',$datos['idproducto'])->where('idsucursal',$datos['idsucursal'])->get();
      if(count($existe)> 0){
        $stock = Stock::where('idproducto',$datos['idproducto'])->where('idsucursal',$datos['idsucursal'])->first();
        $datos['cantidad'] += $stock->cantidad;
        $stock->fill(array('cantidad'=>$datos['cantidad'], 'precio'=>$datos['precio']));;
        $stock->save();
      }else{
        $stock = Stock::create($datos);
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('mercaderia/ingreso');
    }

    public function deleteajax($id,$borrar){
      $producto = ProductoSucursal::find($id);
      if($producto != null){
        $variable = json_decode($producto->recargo,true);
        foreach ($variable as $k => $i) {
          if($k == $borrar){
            unset($variable[$k]);
          }
        }
        $variable = json_encode($variable);
        $producto->recargo = $variable;
        $producto->save();
      }
      return array('resp'=>'true');
    }

    public function delete($id){
      $ingreso = ProductoSucursal::find($id);
      $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',$ingreso->idsucursal)->first();
      $stock->cantidad  = $stock->cantidad - $ingreso->cantidad;
      $stock->save();
      $ingreso->delete();
      return redirect()->to('mercaderia/ingreso');
    }

    public function edit($id){
      $productosucursal = ProductoSucursal::find($id);
      if($productosucursal == null){
        Session::flash('mensaje','El ingreso no existe');
      }
      $producto = Producto::find($productosucursal->idproducto);
      $productosucursal['producto'] = $producto->modelo;
      $marca = Marca::find($producto->idmarca);
      $productosucursal['idmarca'] = $marca->id;
      $productosucursal['marca'] = $marca->nombre;
      $categoria = Categoria::find($marca->idcategoria);
      $productosucursal['idcategoria'] = $categoria->id;
      $productosucursal['categoria'] = $categoria->nombre;
      $categorias = Categoria::all();
      $destinos = User::where('rol','deposito')->orWhere('rol','sucursal')->get();
      $monedas  = Moneda::all();
      $categorias = Categoria::all();
      $importadores = Importador::all();
      return view('admin.ingresomercaderia.create',compact('importadores','productosucursal','categorias','destinos','monedas'));
    }

    public function save(Request $request,$id){
      $this->validate($request,[
         '_token'=>'required',
         'idcategoria'=>'required',
         'idmarca'=>'required',
         'cantidad'=>'required',
         'idmoneda'=>'required',
         'precio'=>'required',
         'idsucursal'=>'required'
       ]);
       $datos = request()->all();
       $mensaje = 'El ingreso de mercaderia fue exitoso';
       if($datos['idproducto']==""){
         Session::flash('mensaje','No seleciono producto a actualizar');
         return redirect()->to('mercaderia/ingreso');
       }
       $ingreso = ProductoSucursal::find($id);
       if($ingreso->idcosto == $datos['idcosto'] ){
         if(isset($datos['recargo']) || isset($datos['fijorecargo'])){
           if($datos['idcosto'] == "FIJO"){
             $recargo = json_encode($datos['fijorecargo']);
             $ingreso->recargo = $recargo;
             $ingreso->save();
           }else{
             $recargo = json_encode($datos['recargo']);
             $ingreso->recargo = $recargo;
             $ingreso->save();
           }
         }
       }else{
         if(isset($datos['recargo']) || isset($datos['fijorecargo'])){
         if($datos['idcosto'] == "FIJO"){
           $recargo = json_encode($datos['fijorecargo']);
           $ingreso->recargo = $recargo;
           $ingreso->save();
         }else{
           $recargo = json_encode($datos['recargo']);
           $ingreso->recargo = $recargo;
           $ingreso->save();
         }
        }
       }
       unset($datos['recargo']);
       $ingreso->save();
       $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',$datos['idsucursal'])->first();
       $stock->cantidad  = $stock->cantidad - $ingreso->cantidad;
       $stock->save();
       $stock = Stock::where('idproducto',$ingreso->idproducto)->where('idsucursal',$datos['idsucursal'])->first();
       $stock->cantidad = $stock->cantidad + $datos['cantidad'];
       $stock->save();
       $ingreso->fill($datos);
       $ingreso->save();
       Session::flash('mensaje',$mensaje);
       return redirect()->to('mercaderia/ingreso');
    }

    public function depositomovercrear(){
      $datos = request()->all();
      $imeis = preg_split('/\r\n/',$datos['imeis']);
      $html = '';
      foreach ($imeis as $i => $imei) {
        if($imei == ""){
          unset($imeis[$i]);
        }
      }

      $productosucursal = ProductoSucursal::where('idsucursal',Auth::user()->id)->
      where('idproducto',$datos['idproducto'])->get();
      $listimei = [];
      foreach ($productosucursal as $p) {
        $listi = preg_split('/\r\n/',$p->imeis);
        foreach ($listi as $i => $imei) {
          if($imei !== "")
            $listimei[] = $imei;
        }
      }
      $html = "";
        foreach ($imeis as $i) {
        if(array_search($i,$listimei)!== false ){
          continue;
        }else{
          $html .= 'EL IMEI: '.strval($i).' NO EXISTE EN ESTE LOCAL <br>';
        }
      }


      if($html != ""){
        Session::flash('mensaje',$html);
        return redirect()->to('deposito/movermercaderia');
      }

      $mensaje = 'El ingreso de mercaderia fue exitoso';
      $stocksucursal = Stock::where('idsucursal',Auth::user()->id)->where('idproducto',$datos['idproducto'])->first();
      if($datos['cantidad'] > $stocksucursal->cantidad){
        Session::flash('mensaje','No alcanzan las unidades existentes para el movimiento');
        return redirect()->to('sucursal/movermercaderia');
      }
      if($datos['cantidad'] < 1){
        Session::flash('mensaje','No se pueden mover menos de una unidad del producto');
        return redirect()->to('sucursal/movermercaderia');
      }


      $producto = ProductoSucursal::where('idproducto',$datos['idproducto'])->get();
      ProductoSucursal::create(array(
        'idproducto' =>$datos['idproducto'],
        'cantidad'=>$datos['cantidad'],
        'idmoneda'=>0,
        'precio'=>0,
        'idorigensucursal'=>Auth::user()->id,
        'idsucursal'=>$datos['idsucursaldestino'],
        'tipo'=>'movidos',
        'imeis'=>$datos['imeis']
      ));
      $stocksucursal->cantidad = $stocksucursal->cantidad - $datos['cantidad'];
      $stocksucursal->save();
      $stock = Stock::where('idsucursal',$datos['idsucursaldestino'])->where('idproducto',$datos['idproducto'])->get();
      if(count($stock) > 0){
        $stock = Stock::where('idsucursal',$datos['idsucursaldestino'])->where('idproducto',$datos['idproducto'])->first();
        $stock->cantidad = $stock->cantidad + $datos['cantidad'];
        $stock->save();
      }else{
        $pstock = Stock::where('idproducto',$datos['idproducto'])->where('idsucursal',Auth::user()->id)->first();

        $stock = Stock::create(array(
          'idproducto'=>$datos['idproducto'],
          'cantidad'=>$datos['cantidad'],
          'idmoneda'=>$pstock->idmoneda,
          'precio'=>$pstock->precio,
          'idsucursal'=>$datos['idsucursaldestino']
        ));
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('deposito/movermercaderia');
    }


    public function sucursalmovercrear(){
      $datos = request()->all();
      $imeis = preg_split('/\r\n/',$datos['imeis']);
      $html = '';
      foreach ($imeis as $i => $imei) {
        if($imei == ""){
          unset($imeis[$i]);
        }
      }

      $productosucursal = ProductoSucursal::where('idsucursal',Auth::user()->id)->
      where('idproducto',$datos['idproducto'])->get();
      $listimei = [];
      foreach ($productosucursal as $p) {
        $listi = preg_split('/\r\n/',$p->imeis);
        foreach ($listi as $i => $imei) {
          if($imei !== "")
            $listimei[] = $imei;
        }
      }
      $html = "";
      foreach ($imeis as $i) {
        if(array_search($i,$listimei)!== false ){
          continue;
        }else{
          $html .= 'EL IMEI: '.strval($i).' NO EXISTE EN ESTE LOCAL <br>';
        }
      }
      if($html != ""){
        Session::flash('mensaje',$html);
        return redirect()->to('sucursal/movermercaderia');
      }

      if(count($imeis)!= $datos['cantidad']){
        Session::flash('mensaje','No coincide la cantidad ingresada con la cantidad de imeis ingresados');
        return redirect()->to('sucursal/movermercaderia');
      }

      $mensaje = 'El ingreso de mercaderia fue exitoso';
      $stocksucursal = Stock::where('idsucursal',Auth::user()->id)->where('idproducto',$datos['idproducto'])->first();
      if($datos['cantidad'] > $stocksucursal->cantidad){
        Session::flash('mensaje','No alcanzan las unidades existentes para el movimiento');
        return redirect()->to('sucursal/movermercaderia');
      }
      if($datos['cantidad'] < 1){
        Session::flash('mensaje','No se pueden mover menos de una unidad del producto');
        return redirect()->to('sucursal/movermercaderia');
      }
      ProductoSucursal::create(array(
        'idproducto' =>$datos['idproducto'],
        'cantidad'=>$datos['cantidad'],
        'idmoneda'=>0,
        'precio'=>0,
        'idorigensucursal'=>Auth::user()->id,
        'idsucursal'=>$datos['idsucursaldestino'],
        'tipo'=>'movidos',
        'imeis'=>$datos['imeis']
      ));
      $stocksucursal->cantidad = $stocksucursal->cantidad - $datos['cantidad'];
      $stocksucursal->save();
      $stock = Stock::where('idsucursal',$datos['idsucursaldestino'])->where('idproducto',$datos['idproducto'])->get();
      if(count($stock) > 0){
        $stock = Stock::where('idsucursal',$datos['idsucursaldestino'])->where('idproducto',$datos['idproducto'])->first();
        $stock->cantidad = $stock->cantidad + $datos['cantidad'];
        $stock->save();
      }else{
        $pstock = Stock::where('idproducto',$datos['idproducto'])->where('idsucursal',Auth::user()->id)->first();

        $stock = Stock::create(array(
          'idproducto'=>$datos['idproducto'],
          'cantidad'=>$datos['cantidad'],
          'idmoneda'=>$pstock->idmoneda,
          'precio'=>$pstock->precio,
          'idsucursal'=>$datos['idsucursaldestino']
        ));
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('sucursal/movermercaderia');
    }

    public function movercreate(){
      $datos = request()->all();
      $mensaje = 'El ingreso de mercaderia fue exitoso';
      $stocksucursal = Stock::where('idsucursal',$datos['idsucursaldestino'])->where('idproducto',$datos['idproducto'])->first();
      if($datos['cantidad'] > $stocksucursal->cantidad){
        Session::flash('mensaje','No alcanzan las unidades existentes para el movimiento');
        return redirect()->to('mercaderia/mover');
      }
      if($datos['cantidad'] < 1){
        Session::flash('mensaje','No se pueden mover menos de una unidad del producto');
        return redirect()->to('mercaderia/mover');
      }
      ProductoSucursal::create(array(
        'idproducto' =>$datos['idproducto'],
        'cantidad'=>$datos['cantidad'],
        'idmoneda'=>0,
        'precio'=>0,
        'idorigensucursal'=>$datos['idsucursal'],
        'idsucursal'=>$datos['idsucursaldestino'],
        'tipo'=>'movidos'
      ));
      $stocksucursal->cantidad = $stocksucursal->cantidad - $datos['cantidad'];
      $stocksucursal->save();
      $stock = Stock::where('idsucursal',$datos['idsucursaldestino'])->where('idproducto',$datos['idproducto'])->get();
      if(count($stock) > 0){
        $stock = Stock::where('idsucursal',$datos['idsucursaldestino'])->where('idproducto',$datos['idproducto'])->first();
        $stock->cantidad = $stock->cantidad + $datos['cantidad'];
        $stock->save();
      }else{
        $stock = Stock::create(array(
          'idproducto'=>$datos['idproducto'],
          'cantidad'=>$datos['cantidad'],
          'idmoneda'=>$datos['idmoneda'],
          'precio'=>$datos['precio'],
          'idsucursal'=>$datos['idsucursaldestino']
        ));
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('mercaderia/mover');
    }

    public function depositomover(){
      $productos = Stock::where('idsucursal',Auth::user()->id)->get();
      foreach ($productos as $p) {
        $pro = Producto::find($p->idproducto);
        $p['producto'] = $pro->modelo;
      }
      $destinos = User::where('id','<>',Auth::user()->id)->where('id','<>',1)->get();
      $monedas  = Moneda::all();
      return view('deposito.movermercaderia.create',compact('productos','destinos','monedas'));
    }

    public function sucursalmover(){
      $productos = Stock::where('idsucursal',Auth::user()->id)->get();
      foreach ($productos as $p) {
        $pro = Producto::find($p->idproducto);
        $p['producto'] = $pro->modelo;
      }
      $destinos = User::where('id','<>',Auth::user()->id)->where('id','<>',1)->get();
      $monedas  = Moneda::all();
      return view('sucursal.movermercaderia.create',compact('productos','destinos','monedas'));
    }

    public function indexdepositomover(){
      $ingresos = ProductoSucursal::where('idorigensucursal',Auth::user()->id)->where('tipo','<>','ingreso')->get();
      foreach ($ingresos as $i) {
        $producto = Producto::find($i->idproducto);
        $marca = Marca::find($producto->idmarca);
        $categoria= Categoria::find($marca->idcategoria);
        $i['producto']= $producto->modelo;
        $i['marca'] = $marca->nombre;
        $i['categoria'] = $categoria->nombre;
      }
      return view('deposito.movermercaderia.index',compact('ingresos'));
    }

    public function indexsucursalmover(){
      $ingresos = ProductoSucursal::where('idorigensucursal',Auth::user()->id)->where('tipo','<>','ingreso')->get();
      foreach ($ingresos as $i) {
        $producto = Producto::find($i->idproducto);
        $marca = Marca::find($producto->idmarca);
        $categoria= Categoria::find($marca->idcategoria);
        $i['producto']= $producto->modelo;
        $i['marca'] = $marca->nombre;
        $i['categoria'] = $categoria->nombre;
      }
      return view('sucursal.movermercaderia.index',compact('ingresos'));
    }

    public function getmarca($id){
      $marcas = Marca::where('idcategoria',$id)->get();
      return $marcas;
    }

    public function getproducto($id){
      $productos = Producto::where('idmarca',$id)->get();
      return $productos;
    }

    public function productossucursales($id){
      $sucursal = Stock::where('idsucursal',$id)->get();
      foreach ($sucursal as $s) {
        $producto = Producto::find($s->idproducto);
        $s['producto'] = $producto->modelo;
      }
      return $sucursal;
    }

    public function getcantidad($sucursal,$id){
      $sucursal = ProductoSucursal::where('idsucursal',$sucursal)->where('idproducto',$id)->get();
      return $sucursal;
    }
}
