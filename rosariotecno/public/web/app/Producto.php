<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = [
      'idmarca',
      'modelo',
      'descripcion',
      'imagen',
      'stockminimo',
      'garantia',
      'transporte'
    ];
}
