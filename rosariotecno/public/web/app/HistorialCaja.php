<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialCaja extends Model
{
    protected $fillable = [
      'articulos',
      'sucursal',
      'total'
    ];
}
