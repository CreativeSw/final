<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $fillable = [
      'sucursal',
      'cliente',
      'formadepagos',
      'total',
      'tipo',
      'estado'
    ];
}
