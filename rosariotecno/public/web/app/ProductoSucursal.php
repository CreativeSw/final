<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoSucursal extends Model
{

    protected $fillable = [
      'idproducto',
      'idcategoria',
      'idimportador',
      'recargo',
      'cantidad',
      'imeis',
      'idmoneda',
      'precio',
      'idsucursal',
      'idorigensucursal',
      'tipo',
      'ingreso'
    ];
}
