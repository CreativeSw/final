<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{

    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idmarca')->unsigned();
            $table->foreign('idmarca')->references('id')->on('marcas');
            $table->string('modelo');
            $table->string('descripcion',700);
            $table->string('imagen');
            $table->string('imagenweb');
            $table->integer('stockminimo');
            $table->integer('garantia');
            $table->float('precioweb');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('productos');
    }
}
