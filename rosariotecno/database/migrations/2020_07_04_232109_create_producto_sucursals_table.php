<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoSucursalsTable extends Migration
{

    public function up()
    {
        Schema::create('producto_sucursals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idproducto');
            $table->integer('idcategoria');
            $table->integer('idsucursal');
            $table->integer('idimportador');
          //  $table->enum('idcosto',['FIJO','VARIABLE']);
            $table->string('recargo',2000);
            $table->integer('cantidad');
            $table->string('imeis',2000);
            $table->integer('idmoneda');
            $table->float('precio');
            $table->enum('ingreso',['nuevo','usado'])->default('nuevo');
            $table->integer('idorigensucursal');
            $table->enum('tipo',array('ingreso','movidos'))->defult('ingreso');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('producto_sucursals');
    }
}
