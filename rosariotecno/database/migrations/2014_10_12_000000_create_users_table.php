<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('dni');
            $table->string('usuario');
            $table->string('password');
            $table->string('email');
            $table->enum('rol',['admin','sucursal','deposito','cliente']);
            $table->enum('estado',['activo','desactivo'])->defualt('activo');
            $table->string('telefono');
            $table->string('direccion');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('users');
    }
}
