<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticuloVentasTable extends Migration
{

    public function up()
    {
        Schema::create('articulo_ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('articulo');
            $table->string('producto');
            $table->integer('cantidad');
            $table->string('imei',1000);
            $table->integer('venta');
            $table->float('precio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articulo_ventas');
    }
}
