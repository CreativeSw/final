<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportadorsTable extends Migration
{

    public function up()
    {
        Schema::create('importadors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razonsocial');
            $table->string('responsable');
            $table->string('direccion');
            $table->string('email');
            $table->string('telefono');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('importadors');
    }
}
