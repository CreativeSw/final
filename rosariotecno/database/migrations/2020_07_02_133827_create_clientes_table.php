<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{

    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('email');
            $table->string('direccion');
            $table->string('telefono');
            $table->enum('tipo',['empleado','clienteminorista','clientemayorista'])->default('clienteminorista');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('clientes');
    }
}
