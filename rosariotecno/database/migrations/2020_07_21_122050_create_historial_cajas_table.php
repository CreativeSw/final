<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialCajasTable extends Migration
{
    public function up()
    {
        Schema::create('historial_cajas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('articulos',2000);
          
            $table->string('sucursal');
            $table->float('total');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('historial_cajas');
    }
}
