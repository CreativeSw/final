<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonedasTable extends Migration
{

    public function up()
    {
        Schema::create('monedas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->float('cotizacion');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('monedas');
    }
}
