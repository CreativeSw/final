<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodigoDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codigo_descuentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->enum('estado',['activo','inactivo']);
            $table->integer('usos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('codigo_descuentos');
    }
}
