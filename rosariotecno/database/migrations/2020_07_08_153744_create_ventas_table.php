<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{

    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sucursal');
            $table->integer('cliente');
            $table->string('formadepagos');
            $table->float('total');
            $table->float('entrega');
            $table->enum('venta',['minorista','mayorista'])->default('minorista');
            $table->enum('tipo',['venta','presupuesto'])->default('venta');
            $table->enum('estado',['cerrada','abierta'])->default('abierta');
            $table->enum('pago',['parcial','completo'])->default('completo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ventas');
    }
}
