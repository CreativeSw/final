<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGastosTable extends Migration
{

    public function up()
    {
        Schema::create('gastos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concepto');
            $table->float('costo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('gastos');
    }
}
