<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormadePagosTable extends Migration
{

    public function up()
    {
        Schema::create('formade_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('formade_pagos');
    }
}
