<?php

use Illuminate\Database\Seeder;
use App\User;
class DatabaseSeeder extends Seeder
{

    public function run()
    {
      User::create(array(
        'nombre'=>'Rodri',
        'usuario'=>'admin',
        'email'=>'admin@admin.com',
        'password'=>bcrypt('admin'),
        'rol'=>'admin'
      ));
        // $this->call(UsersTableSeeder::class);
    }
}
