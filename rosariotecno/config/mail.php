<?php

return [
    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host' => env('MAIL_HOST', 'smtp.googlemail.com'),
    'port' => env('MAIL_PORT', 465),
    'from' => ['address' => null, 'name' => null],
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    'username' => env('testaplicacionessmtp@gmail.com'),
    'password' => env('testaplicacionessmtp1'),
    'sendmail' => '/usr/sbin/sendmail -bs',
];
