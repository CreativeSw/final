@extends('layouts.deposito')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h5>INGRESO MERCADERIA</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-shopping-cart"></i>
          </div>
          <a href="{{url('deposito/mercaderia')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background: #f7d50b !important;">
          <div class="inner">
            <h5>MOVER MERCADERIA</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-shopping-cart"></i>
          </div>
          <a href="{{url('deposito/movermercaderia')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background:#1f8af7 !important;">
          <div class="inner">
            <h5>STOCK</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-truck"></i>
          </div>
          <a href="{{url('deposito/stock')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background:#1f8af7 !important;">
          <div class="inner">
            <h5>STOCK GRAL</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-truck"></i>
          </div>
          <a href="{{url('deposito/stockgral')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-warning" style="background: #c0c1c0 !important;">
          <div class="inner">
            <h5>CONFIGURACION</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-cogs"></i>
          </div>
          <a href="{{url('deposito/configuracion')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-danger">
          <div class="inner">
            <h5>SALIR</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-window-close"></i>
          </div>
          <a href="{{url('salir')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
