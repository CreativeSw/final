@extends('layouts.deposito')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>PRODUCTO</th>
            <th>IMEIS</th>
            <th>CANTIDAD</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($stocks as $s)
          <tr>
          <td>{{$s->producto}}</td>
          <td>@foreach($s->imeis as $i){{$i}}<br>@endforeach</td>
          <td>
            <span style="font-weight: bold;">Stock Actual: </span> {{$s->cantidad}}<br>
            <span style="font-weight: bold;">Stock Minimo: </span> {{$s->stockminimo}}
          </td>
          <td style="border: none;padding: 0;">
            @if($s->cantidad > $s->stockminimo)
            <div style="text-align: center;background: green;color: white;vertical-align: middle;font-weight: bold;padding: 30px;">
            Estable
          </div>
            @else
            <div style="text-align: center;background: red;color: white;vertical-align: middle;font-weight: bold;padding: 30px;">
            Faltantes
            </div>
            @endif
          </td>
          </tr>
          @endforeach

        </tbody>
        <tfoot>
            <tr>
                <th>PRODUCTO</th>
                <th>DESCRIPCION</th>
                <th>ACCIONES</th>
            </tr>
        </tfoot>
    </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
