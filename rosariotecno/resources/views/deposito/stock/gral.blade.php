@extends('layouts.deposito')
@section('content')
<br clear=all> 
<div class="card">
<div class="card-body">
<table id="example" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>SUCURSAL</th>
<th>PRODUCTO</th>
<th>IMEIS</th>
<th>STOCK</th>
<th>ESTADO</th>
</th>
</thead>
<tbody>
@foreach($stocks as $s)
<tr>
<td>{{$s->sucursal->nombre}}</td>
<td>{{$s->producto}}</td>
<td>@foreach(explode(',',$s->imeis) as $i) {{$i}}<br> @endforeach</td>
<td>
<span style="font-weight: bold;">Stock Actual: </span> {{$s->cantidad}}<br>
<span style="font-weight: bold;">Stock Minimo: </span> {{$s->stockminimo}}
</td>
<td>
@if($s->cantidad > $s->stockminimo)
<div style="text-align: center;background: green;color: white;vertical-align: middle;font-weight: bold;padding: 30px;">
Estable
</div>
@else
<div style="text-align: center;background: red;color: white;vertical-align: middle;font-weight: bold;padding: 30px;">
Faltantes
</div>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
