@extends('layouts.deposito')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Ingreso de Mercaderia</h3>
      </div>
      <form  method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
          <div class="form-group">
              <label for="exampleInputEmail1">CATEGORIA</label>
              <select class="form-control categoria" name="idcategoria" id="categoria" data-nombre="CATEGORIA"
              @if(!isset($productosucursal))
              required
              @endif >
                <option value="">SELECIONE UNA</option>
                @foreach($categorias as $c)
                <option value="{{$c->id}}"
                  @if(isset($productosucursal))
                    @if($productosucursal->idcategoria == $c->id)
                      selected="selected"
                    @endif
                  @endif
                  >{{$c->nombre}}</option>
                @endforeach
              </select>
          </div>
        </div>
          <div class="col-md-6">
          <div class="form-group">
              <label for="exampleInputEmail1">MARCA</label>
              <select class="form-control marca" name="idmarca" id="marca" data-nombre="MARCA"
              @if(!isset($productosucursal))
              required
              disabled
              @endif
              >
              @if(isset($productosucursal))
                <option value="">SELECIONE UNA</option>
                <option value="{{$productosucursal->idmarca}}" selected="selected">{{$productosucursal->marca}}</option>
              @endif
            </select>
          </div>
        </div>
          </div>
          <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">PRODUCTO</label>
              <select class="form-control producto" name="idproducto" id="producto" data-nombre="PRODUCTO"
              @if(!isset($productosucursal))
              required
              disabled
              @endif
               >
               @if(isset($productosucursal))
               <option value="">SELECIONE UNA</option>
               <option value="{{$productosucursal->idproducto}}" selected="selected">{{$productosucursal->producto}}</option>
               @endif
             </select>
            </div>
          </div>
          <div class="col-md-6">
            <label for="exampleInputEmail1">IMPORTADOR</label>
            <select class="form-control importador" name="idimportador" data-nombre="IMPORTADOR"
            @if(!isset($productosucursal))
             required
             @endif >
              <option value="">SELECIONE UNA</option>
              @foreach($importadores as $c)
              <option value="{{$c->id}}"
                @if(isset($productosucursal))
                  @if($productosucursal->idimportador == $c->id)
                  selected="selected"
                  @endif
                @endif
                >{{$c->razonsocial}}</option>
              @endforeach
            </select>
          </div>
          </div>
          <div class="row">


          </div>
          <br clear=all>

          <br clear=all>
          <div class="row">
            <div class="col-md-6">
              <label >Cantidad</label>
              <input type="number" class="form-control" name=cantidad placeholder="CANTIDAD"
              @if(isset($productosucursal))
              value="{{$productosucursal->cantidad}}"
              @else
              required
              @endif id="cantidad">
            </div>
            <div class="col-md-6">
              <label>IMEIS</label>
              <textarea class="form-control" name="imeis" data-nombre="IMEIS" id="imeis"
              @if(isset($productosucursal))
              value="{{$productosucursal->imeis}}"
              @else
              required
              @endif

              >@if(isset($productosucursal)){{$productosucursal->imeis}}@endif</textarea>
            </div>
          </div>
          <br clear=all>
            <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">MONEDA</label>
                <select class="form-control" name="idmoneda" data-nombre="MONEDA"
                @if(!isset($productosucursal))
                 required
                @endif
                 >
                  <option value="">SELECIONE UNA</option>
                  @foreach($monedas as $c)
                  <option value="{{$c->id}}"
                    @if(isset($productosucursal))
                      @if($productosucursal->idmoneda == $c->id)
                      selected="selected"
                      @endif
                    @endif
                    >{{$c->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">PRECIO DE COMPRA</label>
                <input type="text" class="form-control" name=precio placeholder="Precio"
                @if(isset($productosucursal))
                value="{{$productosucursal->precio}}"
                @else
                required
                @endif
                >
              </div>
            </div>
          </div>

          <div class="col-md-4" style="float:right;">

        <input type="submit" class="btn btn-primary" id="enviar" style="background:#ff5700;border:none;float:right;"
        value=
        @if(isset($productosucursal))
        ACTUALIZAR
        @else
        REGISTRAR
        @endif>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script>

$(document).ready(function() {
    $('.categoria').select2();
    $('.marca').select2()
    $('.producto').select2()
    $('.importador').select2()
});




$('#idcosto').change(function(e){
  e.preventDefault()
  if($(this).val()== "VARIABLE"){
    $('#variable').css('display','block');
    $('#fijo').css('display','none');
  }else{
    $('#fijo').css('display','block');
    $('#variable').css('display','none');
    $('#listvariable').empty()
  }
})

$('#agregarcosto').click(function(e){
  e.preventDefault()
  html = '<tr>'
  html += '<td>'
  html += $('#concepto').val()
  html += '</td>'
  html += '<td>'
  html += $('#recargo').val()

  html += '<input type=hidden name=recargo['+$('#concepto').val()+'] value="'+$('#recargo').val()+'">'
  html += '</td>'
  html += '<td style="width:10%;">'
  html += '<a href="#" id="borrargasto" ><i class="fas fa-trash"></i></a>'
  html+= '</td>'
  html += '</tr>'
  $('#recargo').val("");
  $('#concepto').val("");

  $('#listvariable').append(html)

  $('#borrargasto').click(function(er){
    er.preventDefault()
    $(this).parent().parent().remove()
  })
})

$('.borrargastoajax').click(function(e){
  e.preventDefault()
  borrar = $(this).parent().parent()
  $.get('/mercaderia/borrar/ajax/'+$(this).data('id')+'/'+$(this).data('borrar'), function(re){
    borrar.remove()
  })
})


$('#categoria').change(function(e){
  e.preventDefault()
  id = $(this).val()
  url = 'categoria/'+id
  $.get(url,function(res){
    html=''
    $('#marca').removeAttr('disabled');
    $('#marca').empty()
    $('#producto').empty()
    html +='<option value="">SELECIONE UNA</option>'
    for(i=0;i<res.length;i++){
      html+='<option value='+res[i].id+'  >'+res[i].nombre+'</option>'
    }
    $('#marca').append(html)
    $('#marca').change(function(event){
      event.preventDefault()
      id = $(this).val()
      url = 'producto/'+id
      $.get(url,function(respuesta){
        html2 = ''
        $('#producto').removeAttr('disabled')
        $('#producto').empty()
        html2 +='<option value="">SELECIONE UNA</option>'
        for(i=0;i<respuesta.length;i++){
          html2+='<option value='+respuesta[i].id+'  >'+respuesta[i].modelo+'</option>'
        }
        $('#producto').append(html2)
      })
    })
  })

})
cantidad = 0;
$('#cantidad').change(function(r){
  r.preventDefault();
  cantidad = parseInt($(this).val())
})


$( "#enviar" ).click(function( event ) {
  event.preventDefault();
  err = 0
  if($("form")[0].checkValidity()){
      imeis = $("form textarea").val().split("\n");
      for(i=0;i< imeis.length;i++){
        if(imeis[i]== ""){
          imeis.splice(i,imeis.length);
        }
      }
      html = 'IMEIS INGRESADOS:'+imeis.length +'<br> CANTIDAD DEFINIDA:'+cantidad
      if(cantidad != imeis.length){
        Swal.fire({
          title:'ERROR ',
          showCancelButton: false,
          showConfirmButton: false,
          html:html
        })
        err = 1
      }
      if(err == 0){
        $( "form" ).first().submit();
      }
  }else{
    html ='<p style="text-align: left;margin-left: 20%;">'
    $("form input").each(function(){
      inputform = $(this)[0]
      if(!inputform.checkValidity()){
        html += 'Complete el campo <span style="font-weight: bold;text-transform: uppercase;color:red;text-align: left;">'
        +$(this).attr('placeholder')+
        '</span><br>'
      }
    });
    $("form select").each(function(){
      selectform = $(this)[0]
      if(!selectform.checkValidity()){
        html += 'Complete el campo <span style="font-weight: bold;text-transform: uppercase;color:red;text-align: left;">'
        +$(this).data('nombre')+
        '</span><br>'
      }
    })
    $("form textarea").each(function(){
      html += 'Complete el campo <span style="font-weight: bold;text-transform: uppercase;color:red;text-align: left;">'
      +$(this).data('nombre')+
      '</span><br>'
    })
    html += '</p>'
    Swal.fire({
      title:'CAMPOS FALTANTES',
      showCancelButton: false,
      showConfirmButton: false,
      html:html
    })
  }
});

$('#imeis').change(function(e){
  e.preventDefault()
  imeistmp = $(this).val().split("\n");
  for(i=0;i< imeistmp.length;i++){
    if(imeistmp[i]== ""){
      imeistmp.splice(i,imeistmp.length);
    }
  }
  for(var i = imeistmp.length -1; i >=0; i--){
    if(imeistmp.indexOf(imeistmp[i]) !== i)
    imeistmp.splice(i,imeistmp.length);
  }
  $('#imeis').val()
  html = ''
  for(i=0;i<imeistmp.length;i++){
    html += imeistmp[i] +'\n'
  }
  $('#imeis').val(html)
})


</script>
@endsection
