@extends('layouts.web')
@section('content')
<div class="breadcrumb-option">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumb__links">
          <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a>
          <a href="{{url('/',[strtolower($producto->categoria)])}}">{{$producto->categoria}} </a>
          <span>{{$producto->modelo}}</span>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="product-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="product__details__pic">
                    <div class="product__details__pic__left product__thumb nice-scroll">
                        @foreach(explode(',',$producto->imagen) as $k => $img)
                        @if($img != "")
                        <a class="pt" href="#{{$img}}">
                            <img src="{{url('uploads',[$img])}}" alt="">
                        </a>
                        @endif
                        @endforeach
                        <a class="pt active" href="#fontal">
                            <img src="{{url('uploads',[$producto->imagenweb])}}" alt="">
                        </a>
                    </div>
                    <div class="product__details__slider__content">
                        <div class="product__details__pic__slider owl-carousel">
                            <img data-hash="fontal" class="product__big__img" src="{{url('uploads',[$producto->imagenweb])}}" alt="">
                            @foreach(explode(',',$producto->imagen) as $k => $img)
                            @if($img != "")
                            <img data-hash="{{$img}}" class="product__big__img" src="{{url('uploads',[$img])}}" alt="">
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="product__details__text">
                    <h3>{{$producto->modelo}}</h3>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="product__details__price">{{$producto->precioweb}}</div>
                    <p></p>
                    <div class="product__details__button">
                        <div class="quantity">
                            <span>Cantidad:</span>
                            <div class="pro-qty">
                                <input type="text" value="1" id="cantidad" >
                            </div>
                        </div>
                        <a href="#" id="agregarcarrito" data-id="{{$producto->id}}" class="cart-btn"><span class="icon_bag_alt"></span> Agregar</a>
                        <ul>
                            <li><a href="{{url('favoritos/agregar',[$producto->id])}}"><span class="icon_heart_alt"></span></a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="product__details__tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link " data-toggle="tab" href="#tabs-2" role="tab">Especificaciones</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-panel" id="tabs-2" role="tabpanel">
                            <h6>Especificaciones</h6>
                            <p>{{$producto->descripcion}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="related__title">
                    <h5>PRODUCTOS RELACIONADOS</h5>
                </div>
            </div>
            @foreach($productorelacionados as $p)
            <div class="col-lg-3 col-md-4 col-sm-6">
              <div class="product__item">
                <div class="product__item__pic set-bg" data-setbg="{{url('uploads',[$p->imagenweb])}}">
                    <ul class="product__hover">
                        <li><a href="#" class="image-popup"><span class="arrow_expand"></span></a></li>
                        <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                        <li><a href="#"><span class="icon_bag_alt"></span></a></li>
                    </ul>
                </div>
              </div>
              <div class="product__item__text">
                  <h6><a href="#">{{$p->modelo}}</a></h6>
                  <div class="rating">
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                  </div>
                  <div class="product__price">{{$p->precio}}</div>
              </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<script>
  $('#agregarcarrito').click(function(e){
    e.preventDefault()
    cantidad = $('#cantidad').val()
    $.get('/carrito/agregar/'+$(this).data('id')+'/'+cantidad,function(res){
      location.reload(true);
    })
  })
</script>
@endsection
