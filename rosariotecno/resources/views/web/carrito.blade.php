@extends('layouts.web')
@section('content')
<div class="breadcrumb-option">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumb__links">
          <a href="{{url('/')}}"><i class="fa fa-home"></i>Home</a>
          <span>Carrito</span>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="shop-cart spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="shop__cart__table">
          <table>
            <thead>
              <tr>
                <th>Producto</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Total</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            @foreach(Session::get('carrito') as $key => $f)
            <tr>
                <td class="cart__product__item">
                    <img src="{{url('uploads',[$f->imagenweb])}}" alt="" style="width:20%;">
                    <div class="cart__product__item__title">
                        <h6>{{$f->modelo}}</h6>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </td>
              <td class="cart__price">$ {{$f->precio}}</td>
              <td class="cart__quantity">
                  <div class="pro-qty">
                      <input type="text" value="{{$f->cantidad}}" id="cantidad">
                  </div>
              </td>
              <td class="cart__total">$ {{$f->precio*$f->cantidad}}</td>
              <td class="cart__close">
                <a href="{{url('favoritos/borrar',[$key])}}" >
                <span class="icon_close"></span>
              </a>
              </td>
                </tr>
            @endforeach
            </tbody>
          </table>

          <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="cart__btn">
                      <a href="{{url('/')}}">Volver a la Tienda</a>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-lg-6">
                  <div class="discount__content">
                      <h6>Codigo de Descuento</h6>
                      <form action="{{url('carrito/descuento')}}" method="post">
                          <input type="text" placeholder="Ingrese Codigo" name="descuento">
                          <button type="submit" class="site-btn" style="background:#ef7a0e;">Aplicar</button>
                      </form>
                  </div>
              </div>
              <div class="col-lg-4 offset-lg-2">
                  <div class="cart__total__procced">
                      <h6>Total</h6>
                      <ul>
                          <li>Subtotal <span>$ {{Session::get('total')}}</span></li>
                          <li>Total <span>$ {{Session::get('total')}}</span></li>
                      </ul>
                      <a href="{{url('carrito/finalizar')}}" class="primary-btn" style="background:#ef7a0e;">Finalizar</a>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
