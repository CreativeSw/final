@extends('layouts.web')
@section('content')
<div class="breadcrumb-option">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumb__links">
            <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a>
            <span>{{$categoria}}</span>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="shop spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="shop__sidebar">
                        <div class="sidebar__categories">
                            <div class="section-title">
                                <h4>MARCAS</h4>
                            </div>
                            <div class="categories__accordion">
                                <div class="accordion" id="accordionExample">
                                    @foreach($filtros['marca'] as $m)
                                    <div class="card">
                                      <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#{{$m->id}}">{{$m->nombre}}</a>
                                      </div>
                                      <div id="{{$m->id}}" class="collapse show" data-parent="#accordionExample">
                                          <div class="card-body">
                                              <ul>

                                              </ul>
                                          </div>
                                      </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="sidebar__filter">
                            <div class="section-title">
                                <h4>ORDENAR POR PRECIO</h4>
                            </div>
                            <div class="filter-range-wrap">
                                <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                data-min="{{$min}}" data-max="{{$max+5}}"></div>
                                <div class="range-slider">
                                    <div class="price-input">
                                        <p>Price:</p>
                                        <input type="text" id="minamount" value="{{$min}}">
                                        <input type="text" id="maxamount" value="{{$max}}">
                                    </div>
                                </div>
                            </div>
                            <a href="{{url(strtolower($categoria),[$min,$max])}}">FILTROS</a>
                        </div>
                        <div class="sidebar__sizes">
                            <div class="section-title">
                                <h4>MARCA</h4>
                            </div>
                            <div class="size__list">
                              @foreach($filtros['marca'] as $m)
                              <label from="{{$m->nombre}}">
                              {{$m->nombre}}
                              <input type="checkbox" id="xxs">
                              <span class="checkmark"></span>
                            </label>
                              @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="row">
                      @foreach($productos as $p)
                      <div class="col-lg-4 col-md-6">
                      <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="{{url('uploads',[$p->imagenweb])}}">
                            <ul class="product__hover">
                                <li><a href="" class="image-popup"><span class="arrow_expand"></span></a></li>
                                <li><a href="{{url('favoritos/agregar',[$p->id])}}"><span class="icon_heart_alt"></span></a></li>
                                <li><a href="{{url('carrito/agregar',[$p->id])}}"><span class="icon_bag_alt"></span></a></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="{{url('producto',[$p->id])}}">{{$p->modelo}}</a></h6>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="product__price">$ {{$p->precioweb}}</div>
                        </div>
                      </div>
                      </div>
                      @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
