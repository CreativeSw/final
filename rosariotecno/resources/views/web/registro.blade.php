@extends('layouts.web')
@section('content')
<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="{{url('/')}}"><i class="fa fa-home"></i>Home</a>
                    <span>Registo</span>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="contact spad">
<div class="container">
<div class="row" style="background:#e8e8e8">
<div class="col-md-8" style="padding: 31px;">
<div class="contact__content">
<div class="contact__form">
<h5 style="color: orange;font-weight: bold;font-size: 24px;">Registo</h5>
<form action="{{url('registrar')}}" method="POST">
{{ csrf_field() }}
<div class="row">
<div class="col-md-6">
<label>Nombre</label>
<input type="text" placeholder="Nombre" name="nombre" required>
</div>
<div class="col-md-6">
<label>DNI</label>
<input type="text" placeholder="DNI" name="dni" requiredSAA>
</div>
</div>
<div class="row">
<div class="col-md-6">
<label>Email</label>
<input type="text" placeholder="Email" name="email" required>
</div>
<div class="col-md-6">
<label>Clave</label>
<input type="password" placeholder="Clave" name="password" required>
</div>
</div>
<div class="row">
<div class="col-md-6">
<label>Telefono</label>
<input type="text" placeholder="Telefono" name="telefono" required>
</div>
<div class="col-md-6">
<label>Direccion</label>
<input type="text" placeholder="Direccion" name="direccion" required>
</div>
</div>
<button type="submit" class="site-btn" style="background:#ef7a0e;float:right;">REGISTRAR</button>
</form>
</div>
</div>
</div>
<div class="col-md-4" style="margin-right:-3px;">
<img src="{{url('web/img/registro.png')}}" style="width:100%;">
</div>
</div>
</div>
</section>
@endsection
