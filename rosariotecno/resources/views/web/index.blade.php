@extends('layouts.web')
@section('content')
<section class="categories">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 p-0">
                <div class="categories__item categories__large__item set-bg"
                data-setbg="{{url('web/img/categories/category-1.jpg')}}">
                <div class="categories__text">
                    <h1 >SMART<br>PHONE</h1>
                    <a href="{{url('smartphone')}}">VER MAS</a>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 p-0">
                    <div class="categories__item set-bg"
                    data-setbg="{{url('web/img/categories/category-2.jpg')}}">
                        <div class="categories__text">
                            <h2 style="color: white;font-weight: bold;">SMART<br>WATCH</h2>
                            <br clear=all>
                            <a href="{{url('smartwatch')}}">VER MAS</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 p-0">
                    <div class="categories__item set-bg"
                    data-setbg="{{url('web/img/categories/category-3.jpg')}}">
                        <div class="categories__text">
                            <h3 style="color: white;font-weight: bold;">NOTEBOOKS</h3>
                            <a href="{{url('notebooks')}}">VER MAS</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-6 col-sm-6 p-0">
                    <div class="categories__item set-bg"
                    data-setbg="{{url('web/img/categories/category-5.jpg')}}">
                        <div class="categories__text">
                            <h3 style="color: white;font-weight: bold;margin-top:-60px;">ACCESORIOS</h3>
                            <a href="{{url('accesorios')}}">VER MAS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- Categories Section End -->

<!-- Product Section Begin -->
<section class="product spad">
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="section-title">
                <h4>Productos Mas Vendidos</h4>
            </div>
        </div>

    </div>
</div>
</section>
<section class="banner set-bg" data-setbg="{{url('web/img/banner/banner-1.jpg')}}">
<div class="container">
  <div class="row">
  <div class="col-xl-7 col-lg-8 m-auto">
    <div class="banner__slider owl-carousel">
      @foreach($sliders as $s)
      <div class="banner__item">
        <div class="banner__text">
          <span style="color:black;">{{$s->categoria}}</span>
          <h1>{{$s->texto}}</h1>
          <a href="{{url($s->categoria)}}">Ver Mas</a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
  </div>
</div>
</section>

<section class="trend spad">
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="trend__content">
                <div class="section-title">
                    <h4>SmartWatch</h4>
                </div>
                @foreach($datos['smartwatch'] as $p)
                <div class="trend__item">
                  <div class="trend__item__pic" style="width:30%;">
                      <img src="{{url('uploads',[$p->imagenweb])}}" alt="{{$p->modelo}}">
                  </div>
                  <div class="trend__item__text">
                      <h6>{{$p->modelo}}</h6>
                      <div class="rating">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                      </div>
                      <div class="product__price">$ {{$p->precioweb}}</div>
                  </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="trend__content">
                <div class="section-title">
                    <h4>SmartPhone</h4>
                </div>
                @foreach($datos['smartphone'] as $p)
                <div class="trend__item">
                  <div class="trend__item__pic" style="width:30%;">
                      <img src="{{url('uploads',[$p->imagenweb])}}" alt="{{$p->modelo}}">
                  </div>
                  <div class="trend__item__text">
                      <h6>{{$p->modelo}}</h6>
                      <div class="rating">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                      </div>
                      <div class="product__price">$ {{$p->precioweb}}</div>
                  </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="trend__content">
                <div class="section-title">
                    <h4>Accesorios</h4>
                </div>
                @foreach($datos['accesorios'] as $p)
                <div class="trend__item">
                  <div class="trend__item__pic" style="width:30%;">
                      <img src="{{url('uploads',[$p->imagenweb])}}" alt="{{$p->modelo}}">
                  </div>
                  <div class="trend__item__text">
                      <h6>{{$p->modelo}}</h6>
                      <div class="rating">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                      </div>
                      <div class="product__price">$ {{$p->precioweb}}</div>
                  </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</section>
<br clear=all>
@if($oferta !== null)
<section class="discount">
<div class="container">
    <div class="row">
      <div class="col-lg-4 p-0" >
          <div class="discount__pic" style="width:100%;height: 390px;">
              <img src="{{url('uploads',[$oferta->imagen])}}" alt="Descuento">
          </div>
      </div>
      <div class="col-lg-8 p-0">
          <div class="discount__text">
              <div class="discount__text__title">
                  <span>DESCUENTO </span>
                  <h2>{{$oferta->producto}}</h2>
                  <h5><span>Descuento</span> {{$oferta->descuento}}%</h5>
              </div>
              <div class="discount__countdown" id="ofertasfin" data-id="{{$oferta->vencimiento}}"></div>
          </div>
      </div>
    </div>
</div>
</section>
@endif

<section class="services spad">
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="services__item">
                <i class="fa fa-car"></i>
                <h6>ENVIOS</h6>
                <p>Envios a todo argentina</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="services__item">
                <i class="fa fa-money"></i>
                <h6>METODOS DE PAGOS</h6>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="services__item">
                <i class="fa fa-support"></i>
                <h6>COMPRAS 24/7</h6>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="services__item">
                <i class="fa fa-headphones"></i>
                <h6>COMPRAS SEGURAS</h6>
                <p>100% Seguras</p>
            </div>
        </div>
    </div>
</div>
</section>

<script>
$('#favorito').click(function(e){
  console.log('sssss')
})

@if($oferta != null)
  finalDate = $("#ofertasfin").data('id');
  $('#ofertasfin').countdown(finalDate, function(event) {
    $('#ofertasfin').html(event.strftime("<div class='countdown__item'><span>%D</span> <p>Dias</p></div>" + "<div class='countdown__item'><span>%H</span> <p>Horas</p> </div>" + "<div class='countdown__item'><span>%M</span> <p>Min</p> </div>" + "<div class='countdown__item'><span>%S</span> <p>Sec</p> </div>"));
  });
@endif
</script>
@endsection
