@extends('layouts.web')
@section('content')
<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="{{url('/')}}"><i class="fa fa-home"></i>Home</a>
                    <span>Contacto</span>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="contact spad">
      <div class="container">
          <div class="row">
              <div class="col-lg-6 col-md-6">
                  <div class="contact__content">
                      <div class="contact__address">
                          <h5>Informacion de Contacto</h5>
                          <ul>
                              <li>
                                  <h6><i class="fa fa-map-marker"></i> Direccion</h6>
                                  <p>Tarragona 309, S2000 Rosario, Santa Fe</p>
                              </li>
                              <li>
                                  <h6><i class="fa fa-phone"></i> Telefono</h6>
                                  <p><span>0341 500-2219</span></p>
                              </li>
                              <li>
                                  <h6><i class="fa fa-headphones"></i> Soporte</h6>
                                  <p>info@rosariotecno.com.ar</p>
                              </li>
                          </ul>
                      </div>
                      <div class="contact__form">
                          <h5>Mensaje</h5>
                          <form action="{{url('contacto')}}" method="POST">
                            {{ csrf_field() }}
                              <input type="text" placeholder="Nombre" name="nombre" required>
                              <input type="text" placeholder="Email" name="email" required>
                              <textarea placeholder="Ingrese Mensaje" name="mensaje" required></textarea>
                              <button type="submit" class="site-btn" style="background:#ef7a0e;">ENVIAR</button>
                          </form>
                      </div>
                  </div>
              </div>
              <div class="col-lg-6 col-md-6">
                  <div class="contact__map">
                      <iframe
                      src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13394.852570711857!2d-60.7343433!3d-32.9321764!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcc40c917ba6eee34!2sRosario%20Tecno!5e0!3m2!1ses-419!2sar!4v1603677668917!5m2!1ses-419!2sar"
                      height="780" style="border:0" allowfullscreen="">

                  </iframe>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection
