@extends('layouts.web')
@section('content')
<div class="breadcrumb-option">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumb__links">
          <a href="{{url('/')}}"><i class="fa fa-home"></i>Home</a>
          <span>Favoritos</span>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="shop-cart spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="shop__cart__table">
          <table>
            <thead>
              <tr>
                <th style="width:20%;">Foto</th>
                <th>Producto</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
            @foreach(Session::get('favoritos') as $key => $f)
            <tr>
              <td class="cart__product__item">
                <img src="{{url('uploads',[$f->imagenweb])}}" alt="{{$f->modelo}}" style="width:20%;">
              </td>
              <td>
                <div class="cart__product__item__title">
                  <h6>{{$f->modelo}}</h6>
                  <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                  </div>
                </div>
              </td>
              <td class="cart__close">
                <a href="{{url('favoritos/borrar',[$key])}}" >
                <span class="icon_close"></span>
              </a>
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
