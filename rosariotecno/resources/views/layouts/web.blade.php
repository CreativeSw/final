<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ashion Template">
    <meta name="keywords" content="Ashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RosarioTecno</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{url('web/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('web/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('web/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('web/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('web/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('web/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('web/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('web/css/style.css')}}" type="text/css">
    <script src="{{url('web/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('web/js/bootstrap.min.js')}}"></script>
    <script src="{{url('web/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{url('web/js/mixitup.min.js')}}"></script>
    <script src="{{url('web/js/jquery-ui.min.js')}}"></script>
    <script src="{{url('web/js/jquery.countdown.min.js')}}"></script>
</head>

<body>
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper" style="background:#0B0B0B;">
        <div class="offcanvas__close" style="background: red;border: none;color: white;font-weight: bold;">+</div>
        <ul class="offcanvas__widget">
            <li style="color:white;"><span class="icon_search search-switch"></span></li>
            <li ><a href="#" style="color:white;"><span class="icon_heart_alt"></span>
                <div class="tip" style="background:red">2</div>
            </a></li>
            <li><a href="#" style="color:white;"><span class="icon_bag_alt"></span>
                <div class="tip" style="background:red">2</div>
            </a></li>
        </ul>
        <div class="offcanvas__logo">
            <a href="{{url('/')}}"><img src="{{url('web/img/logo.png')}}" style="width:100%;"></a>
        </div>
        <div id="mobile-menu-wrap"></div>
        <div class="offcanvas__auth">

          @if(Auth::user()== null)
            <a href="{{url('loginweb')}}" style="color: #ffbf00;">Login</a>
            <a href="{{url('registrar')}}" style="color: #ffbf00;">Register</a>
          @else
            <a href="{{url('perfil')}}" style="color: #ffbf00;">{{Auth::user()->nombre}}</a>
            <a href="{{url('salir')}}" style="color: red;text-align:left;font-weight:bold;">SALIR</a>
          @endif
        </div>
    </div>
<header class="header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-3 col-lg-2">
        <div class="header__logo">
          <a href="{{url('/')}}"><img src="{{url('web/img/logo.png')}}" alt="" style="width:90%;"></a>
        </div>
      </div>
      <div class="col-xl-6 col-lg-7">
        <nav class="header__menu">
        <ul>
          <li @if(strpos(url()->current(),'notebooks')==false
              && (strpos(url()->current(),'smartphone')==false)
              && (strpos(url()->current(),'smartwatch')==false)
              && (strpos(url()->current(),'notebooks')==false)
              && (strpos(url()->current(),'accessorios')==false)
              && (strpos(url()->current(),'contacto')==false)
            )
            class="active"
            @endif


          ><a href="{{url('/')}}" style="color:#ffbf00;">Home</a></li>
          <li
          @if(strpos(url()->current(),'smartphone')!=false)
          class="active"
          @endif
          ><a href="{{url('smartphone')}}" style="color:#ffbf00;">SmartPhone</a></li>
          <li
          @if(strpos(url()->current(),'smartwatch')!=false)
          class="active"
          @endif
          ><a href="{{url('smartwatch')}}" style="color:#ffbf00;">SmartWatch</a></li>
          <li @if(strpos(url()->current(),'notebooks')!=false)
          class="active"
          @endif
          ><a href="{{url('notebooks')}}" style="color:#ffbf00;">Notebooks</a></li>
          <li @if(strpos(url()->current(),'accessorios')!=false)
          class="active"
          @endif
          ><a href="{{url('accessorios')}}" style="color:#ffbf00;">Accesorios</a></li>
          <li @if(strpos(url()->current(),'contacto')!=false)
          class="active"
          @endif
          ><a href="{{url('contacto')}}" style="color:#ffbf00;">Contacto</a></li>
        </ul>
        </nav>
      </div>
      <div class="col-lg-3">
        <div class="header__right">
        <div class="header__right__auth">
          @if(Auth::user()== null)
          <a href="{{url('loginweb')}}">Login</a>
          <a href="{{url('registrar')}}">Register</a>
          @else
          <a href="{{url('perfil')}}" style="color: #ffbf00;"><i class="fa fa-user-circle" aria-hidden="true"></i>{{Auth::user()->nombre}}</a>
          <a href="{{url('salir')}}" style="color:red;font-weight: bold;text-transform: uppercase;">SALIR</a>
          @endif

        </div>
        <ul class="header__right__widget">
        <li><span class="icon_search search-switch" style="color:white;"></span></li>
        <li><a href="{{url('favoritos')}}" style="color:white;"><span class="icon_heart_alt"></span>
        @if(count(Session::get('favoritos')) > 0)
        <div class="tip">{{count(Session::get('favoritos'))}}</div>
        @endif
        </a></li>
        <li><a href="{{url('carrito')}}" style="color:white;"><span class="icon_bag_alt"></span>
        @if(count(Session::get('carrito')) > 0)
        <div class="tip" >{{count(Session::get('carrito'))}}</div>
        @endif
        </a></li>
        </ul>
        </div>
      </div>
    </div>
    <div class="canvas__open" style="border: 1px solid #f7efef;">
      <i class="fa fa-bars" style="color: #ffbf00;"></i>
    </div>
  </div>
</header>
@yield('content')
<footer class="footer" style="background:#0B0B0B;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-7">
                <div class="footer__about">
                    <div class="footer__logo">
                        <a href="{{url('/')}}"><img src="{{url('web/img/logo.png')}}" alt="" style="width:90%;"></a>
                    </div>
                    <p>Somos una empresa dedicada a la venta de articulos de tecnologia.</p>
                    <div class="footer__payment">
                        <a href="/"><img src="{{url('web/img/payment/payment-1.png')}}" alt=""></a>
                        <a href="/"><img src="{{url('web/img/payment/payment-2.png')}}" alt=""></a>
                        <a href="/"><img src="{{url('web/img/payment/payment-3.png')}}" alt=""></a>
                        <a href="/"><img src="{{url('web/img/payment/payment-4.png')}}" alt=""></a>
                        <a href="/"><img src="{{url('web/img/payment/payment-5.png')}}" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-7"></div>
            <div class="col-lg-4 col-md-8 col-sm-8">
                <div class="footer__newslatter" >
                    <h6 style="color:white;">NEWSLETTER</h6>
                    <form action="{{url('newsletter')}}" method="post">
                      {{ csrf_field() }}
                        <input type="text" name="email" placeholder="Email" required>
                        <button type="submit" class="site-btn" style="background: #ef7a0e;" id="newsletter">Subscribe</button>
                    </form>
                    <div class="footer__social">
                        <a href="https://www.facebook.com/Rosariotecno1"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.instagram.com/rosario.tecno/"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch">+</div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>

<script type="text/javascript">


(function () {var options = {

whatsapp: "5493415002219", // WhatsApp number

position: "right", // Position may be 'right' or 'left'.

image: "", //Image to display. Leave blank to display whatsapp defualt icon

text:"Obtener Botón",

link_to:"https://whatschat.co"};

var proto = document.location.protocol, host = "https://whatschat.co", url = host;

var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/whatsapp/init4.js';

s.onload = function () { getbutton(host, proto, options); };

var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);

})();</script>
<!-- Js Plugins -->


<script src="{{url('web/js/owl.carousel.min.js')}}"></script>
<script src="{{url('web/js/jquery.slicknav.js')}}"></script>
<script src="{{url('web/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{url('web/js/main.js')}}"></script>
</body>
</html>
