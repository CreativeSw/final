<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>RosarioTecno</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{url('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{url('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{url('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('plugins/jqvmap/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{url('dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{url('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <link rel="stylesheet" href="{{url('plugins/daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{url('plugins/summernote/summernote-bs4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.css"/>
  <script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
  <script src="{{url('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" >
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background:black;">
    <div class="sidebar">
      <div class="col-md-12" style="background: #ff6c21;border: none;padding: 6px;">
        <div class="image" style="text-align: center;">
          <img src="{{url('img/logo2.png')}}" alt="User Image" style="width:100%;">
        </div>
      </div>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{url('sucursal')}}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>PPAL</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('sucursal/preciosupdate')}}" class="nav-link">
              <i class="nav-icon fab fa-buffer"></i>
              <p>ACTUALIZAR PRECIOS</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('sucursal/clientes')}}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>CLIENTES</p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{url('sucursal/monedas')}}" class="nav-link">
              <i class="nav-icon fas fa-money-bill-wave"></i>
              <p>MONEDAS</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-dolly"></i>
              <p>Ingreso de Mercaderia<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('sucursal/mercaderia')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Listado</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('sucursal/mercaderia/nuevo')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nuevo</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-shipping-fast"></i>
              <p>Mover Mercaderia<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('sucursal/movermercaderia')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Listado</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('sucursal/movermercaderia/nuevo')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nuevo</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-shopping-basket"></i>
              <p>Venta<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('sucursal/ventas')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Listado</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('sucursal/ventas/nueva')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nueva</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-shopping-basket"></i>
              <i class="fas fa-shopping-basket"></i>
              <p>VENTAS MAYORISTA<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('sucursal/ventasmayorista')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Listado</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('sucursal/ventasmayorista/nueva')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nueva</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('sucursal/ventas/cerrarcaja')}}" class="nav-link">
              <i class="nav-icon fa fa-university"></i>
              <p>CERRAR CAJA</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('sucursal/stock')}}" class="nav-link">
              <i class="nav-icon fa fa-truck"></i>
              <p>Stock</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('sucursal/stockgral')}}" class="nav-link">
              <i class="nav-icon fa fa-truck"></i>
              <p>Stock GRAL</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('sucursal/configuracion')}}" class="nav-link">
              <i class="nav-icon fa fa-cogs"></i>
              <p>Configuración</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('salir')}}" class="nav-link">
              <i class="nav-icon fa fa-window-close"></i>
              <p>Salir</p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <div class="content-wrapper">
    <section class="content">
      <div class="container-fluid">
    @yield('content')
  </div>
</section>
  </div>
  <footer class="main-footer">
    <strong style="float:right;">Power By <a href="">CreativeSoftware</a>.</strong>
    <br clear=all>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{url('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{url('plugins/sparklines/sparkline.js')}}"></script>
<script src="{{url('plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{url('plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<script src="{{url('plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{url('plugins/moment/moment.min.js')}}"></script>
<script src="{{url('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{url('plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{url('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{url('dist/js/adminlte.js')}}"></script>
<script src="{{url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<script src="{{url('dist/js/pages/dashboard.js')}}"></script>
<script src="{{url('dist/js/demo.js')}}"></script>
</body>
</html>
