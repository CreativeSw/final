<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
    html,body{
    height:297mm;
    width:300mm;
    }
    </style>
  </head>
  <body>
    <div style="width:100%;height: 120px;">
      <div style="width: 71%;padding: 20px;float: left;text-transform: capitalize;border: 1px solid;margin-left: 33px;">
        <span style="font-weight: bold;font-size: 26px;">{{strtoupper(Auth::user()->rol)}}</span><br>
        <span style="font-weight: bold;">Responsable: </span>{{Auth::user()->nombre}}<br>
        <span style="font-weight: bold;">Direccion: </span>{{Auth::user()->direccion}}<br>
        <span style="font-weight: bold;">Telefono: </span>{{Auth::user()->telefono}}
      </div>
      <div style="background: black;width: 210px;padding: 10px;text-align: center;float: left;">
        <img src="{{ public_path("/img/logo2.png") }}" height="110px" style="width:100%;" >
      </div>
    </div>
    <br clear=all>
    <div style="width: 95%;">

    <h3 style="text-transform: uppercase;margin-left: 35px;">Listado De Productos</h3>
    <ul style="width: 100%;list-style-type: none;text-align: center;display: flex;background:yelow;">
      <li style="width: 100%;border: 1px solid;">SUCURSAL</li>
      <li style="width: 100%;border: 1px solid;">PRODUCTO</li>
      <li style="width: 100%;border: 1px solid;">IMEIS</li>
      <li style="width: 100%;border: 1px solid;">STOCK</li>

      <li style="width: 100%;border: 1px solid;">ESTADO</li>
    </ul>
    @foreach($stocks as $s)
    <ul style="width: 100%;list-style-type: none;text-align: center;display: flex;">
      <li style="width: 100%;border: 1px solid;">{{$s->sucursal->nombre}}</li>
      <li style="width: 100%;border: 1px solid;">{{$s->producto}}</li>
      <li style="width: 100%;border: 1px solid;">
      @foreach(explode(',',$s->imeis) as $i) {{$i}}<br> @endforeach
      </li>
      <li style="width: 100%;border: 1px solid;">
        <span style="font-weight: bold;">Stock Actual: </span> {{$s->cantidad}}<br>
        <span style="font-weight: bold;">Stock Minimo: </span> {{$s->stockminimo}}
      </li>
      <li style="width: 100%;border: 1px solid;">
        @if($s->cantidad > $s->stockminimo)
          <div style="text-align: center;background: green;color: white;vertical-align: middle;font-weight: bold;padding: 30px;">
            Estable
          </div>
        @else
          <div style="text-align: center;background: red;color: white;vertical-align: middle;font-weight: bold;padding: 30px;">
            Faltantes
          </div>
        @endif
      </li>
    </ul>
    @endforeach
  </div>
  </body>
</html>
