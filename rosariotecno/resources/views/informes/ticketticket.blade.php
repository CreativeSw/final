<html style="width: 52mm;">
	<head></head>
	<style type="text/css">
		.pcoustome{
			font-size: 20px;
			font-weight: bold;
			text-align: center;
			margin-bottom: 20px;
		}
		img{
			width: 90%;
		}
	</style>
	<body>
	<center>
		<img src="{{url('img/logo2.png')}}"  />
    <p style="font-size:15px;">ROSARIO TECNO</p>
    <span style="margin-left: 30px;font-size:15px;">{{Auth::user()->direccion}}</span><br>
    <span style="margin-left: 30px;font-size:15px;">{{Auth::user()->telefono}}</span><br>
    <span style="margin-left: 30px;font-size:15px;">SANTA FE - ARGENTINA</span><br>
		<?php
		$a= shell_exec('date "+%T" ');
		print($a);
		?>
	</center>
	<div id="contenido">
    <p class="pcoustome">TIKET NO VALIDO COMO FACTURA</p>
    <table style="text-transform: uppercase;font-size: 13px;text-align: center">
    <tr>
      <td style="font-weight: bold;">Des</td>
      <td style="font-weight: bold;">Cant</td>
    </tr>
    @foreach($venta->articulos as $articulo)
    <tr>
      <td style="font-size: 16px;">{{$articulo->producto}}</td>
      <td style="font-size: 16px;">{{$articulo->cantidad}}</td>
    </tr>
    @endforeach
  </table>
  <p>TOTAL $:{{round($venta->total,2)}}</p>
  </div>

	<p class="pcoustome">GRACIAS POR SU COMPRA</p>
	<p style="color:white;display: none">___________________</p>
	</body>
	<script>
		if (window.print) {
			window.print();
			window.onafterprint = function(){
  				window.location.href = "{{url('/sucursal/ventas')}}";
  			}
		}
	</script>
</html>
