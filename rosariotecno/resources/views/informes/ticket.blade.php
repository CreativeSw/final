<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>

      html,body{
      height:297mm;
      width:300mm;
    }

    </style>
  </head>
  <body>
    <div style="width:100%;height: 120px;border: 1px solid;margin-left: 33px;">
      <br clear=all>
      <div style="width: 25%;padding: 20px;float: left;text-transform: capitalize;margin-left: 5px;">
        <span style="font-weight: bold;font-size: 12px;border:solid 2px;padding:10px;">COMPROBANTE DE GARANTIA</span><br>
        <br><br>
      </div>
      <div style="width: 30%;padding: 20px;float: left;text-transform: capitalize;">
        <span style="font-weight: bold;font-size: 25px;">Datos Comprobante</span><br>
        <br>
        <span style="font-weight: bold;font-size: 18px;">Número de Comprobante: </span>{{$venta->id}}<br>
      </div>
      <div style="width: 30%;padding: 29px;float: left;text-transform: capitalize;">
        <span style="font-weight: bold;font-size: 18px;">Fecha: </span>{{date('d/m/Y')}}<br>
      </div>
    </div>
    <br clear=all>
    <br clear=all>
    <br clear=all>
    <div style="width: 95%;margin-bottom:700px;">
    <h3 style="text-transform: uppercase;margin-left: 30px;font-size: 20px">ROSARIO TECNO</h3>
    <br clear=all>
    <span style="margin-left: 30px;font-size:18px;">{{Auth::user()->direccion}}</span><br>
    <span style="margin-left: 30px;font-size:18px;">{{Auth::user()->telefono}}</span><br>
    <span style="margin-left: 30px;font-size:18px;">SANTA FE - ARGENTINA</span><br>
    <br clear=all>
    <br clear=all>
    <h3 style="margin-left: 35px;">Datos del Cliente:</h3>
    <span style="font-weight: bold;margin-left: 33px;font-size:18px;text-align:right!important;">Código: </span> <span style="font-size:18px;">{{$venta->cliente->id}}</span><br>
    <span style="font-weight: bold;margin-left: 33px;font-size:18px;text-align:right!important;">Señor/a: </span><span style="font-size:18px;">{{$venta->cliente->nombre}}</span><br>
    <span style="font-weight: bold;margin-left: 33px;font-size:18px;text-align:right!important;">Dirección: </span><span style="font-size:18px;">{{$venta->cliente->direccion}}</span><br>
    <span style="font-weight: bold;margin-left: 33px;font-size:18px;text-align:right!important;">Tel: </span><span style="font-size:18px;">{{$venta->cliente->telefono}}</span><br>
    <br clear=all>
    <br clear=all><br clear=all>
    <br clear=all>
    <ul style="width: 100%;list-style-type: none;text-align: left;display: flex;border-top:5px solid;border-bottom:5px solid;padding:5px;">
      <li style="width: 90%;">ARTICULO</li>
      <li style="width: 100%;">DESCRIPCION</li>
      <li style="width: 10%;">CAN</li>
    </ul>
    @foreach($venta->articulos as $articulo)
    <ul style="width: 100%;list-style-type: none;text-align: left;display: flex;background:yelow;">
      <li style="width: 90%;margin-left:none;">{{$articulo->producto}}</li>
      <li style="width: 100%;">{{$articulo->imei}}</li>
      <li style="width: 10%;">{{$articulo->cantidad}}</li>
    </ul>
    @endforeach
  </div>
  <div style="width: 95%;margin-left: 35px;">
  @if($venta->pago=="parcial")
  <p>
    <span style="text-transform: uppercase;font-weight: bold;">Usted abono:</span> ${{$venta->entrega}}<br>
    <span style="text-transform: uppercase;font-weight: bold;">Adeuda:</span> ${{$venta->total - $venta->entrega}}
  </p>
  @endif
  @foreach($formas as $v)
  @if($v->nombre == "EFECTIVO")
  <p>
    <span style="text-transform: uppercase;font-weight: bold;">METODO:</span> {{$v->nombre}}<br>
    <span style="text-transform: uppercase;font-weight: bold;">MONTO:</span> ${{$v->monto}}
  </p>
  @endif
  @endforeach
</div>
  <br clear=all>
  <br clear=all>
  <div style="width: 300mm;margin-left: 35px;">
    <div style="width:60%;float:left;border: 4px solid;height: 125px;">
      <h4 style="float:left;margin-left: 35px;">FIRMA : </h4>
    </div>
    <div style="width:400px;float:left;border-right: 4px solid;border-top:4px solid; border-bottom:4px solid;height: 125px;">
      <h4 style="float:left;margin-left: 15px;">Total : ${{round($venta->total,2)}}</h4><br clear=all>

      @if(count($formas) == 1)

        @if($formas[0]->nombre == "EFECTIVO")
        <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$formas[0]->nombre}}</h4>
        @endif
        @if($formas[0]->nombre == "ahora_3")
        <h4 style="margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$formas[0]->nombre}}</h4>
        <h4 style="margin-left: 14px;margin-top: 0px;">3 CUOTAS ${{round($venta->total/3,2)}}</h4>
        @endif
        @if($formas[0]->nombre == "ahora_6")
        <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$formas[0]->nombre}}</h4>
        <h4 style="float:left;margin-left: 14px;margin-top: 0px;">6 CUOTAS ${{round($venta->total/6,2)}}</h4>
        @endif
        @if($formas[0]->nombre == "ahora_12")
        <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$formas[0]->nombre}}</h4>
        <h4 style="float:left;margin-left: 14px;margin-top: 0px;">12 CUOTAS ${{round($venta->total/12,2)}}</h4>
        @endif
        @if($formas[0]->nombre == "ahora_18")
        <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : $formas[0]->nombre}}</h4>
        <h4 style="float:left;margin-left: 14px;margin-top: 0px;">18 CUOTAS ${{round($venta->total/18,2)}}</h4>
        @endif
      @else
        @foreach($formas as $v)
          @if($v->nombre == "ahora_3")
          <h4 style="margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$v->nombre}}</h4>
          <h4 style="margin-left: 14px;margin-top: 0px;">3 CUOTAS ${{round($venta->total/3,2)}}</h4>
          @endif
          @if($v->nombre == "ahora_6")
          <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$v->nombre}}</h4>
          <h4 style="float:left;margin-left: 14px;margin-top: 0px;">6 CUOTAS ${{round($venta->total/6,2)}}</h4>
          @endif
          @if($v->nombre == "ahora_12")
          <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$v->nombre}}</h4>
          <h4 style="float:left;margin-left: 14px;margin-top: 0px;">12 CUOTAS ${{round($venta->total/12,2)}}</h4>
          @endif
          @if($v->nombre == "ahora_18")
          <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$v->nombre}}</h4>
          <h4 style="float:left;margin-left: 14px;margin-top: 0px;">18 CUOTAS ${{round($venta->total/18,2)}}</h4>
          @endif
          @if($v->nombre == "TRANSFERENCIA")
          <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$v->nombre}}</h4>
          @endif
          @if($v->nombre == "TARJETA DE DEBITO")
          <h4 style="float:left;margin-left: 15px;margin-top: 0px;">Forme de Pago : {{$v->nombre}}</h4>

          @endif
        @endforeach
      @endif
      <br clear=all>
    </div>
  </div>
  <br clear=all>
  </body>
</html>
