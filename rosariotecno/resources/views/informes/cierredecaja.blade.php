<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
    html,body{
    height:297mm;
    width:300mm;
    }
    </style>
  </head>
  <body>
    <div style="width:100%;height: 120px;">
      <div style="width: 67%;padding: 20px;float: left;text-transform: capitalize;border: 1px solid;margin-left: 33px;">
        <span style="font-weight: bold;font-size:20px;text-align:center !important;">CIERRE DE CAJA</span><br>
        <span style="font-weight: bold;">{{strtoupper(Auth::user()->rol)}}</span><br>
        <span style="font-weight: bold;">Fecha: </span>{{date('d/m/Y')}}<br>
        <span style="font-weight: bold;">Responsable: </span>{{Auth::user()->nombre}}<br>
        <span style="font-weight: bold;">Direccion: </span>{{Auth::user()->direccion}}<br>
        <span style="font-weight: bold;">Telefono: </span>{{Auth::user()->telefono}}<br>
      </div>
      <div style="background: black;width: 150px;padding: 12px;text-align: center;float: left;">
        <img src="{{ public_path("/img/logo2.png") }}" height="138px" style="width:100%;" >
      </div>
    </div>
    <br clear=all>
    <br clear=all>
    <div style="width: 95%;">
      <h3 style="text-transform: uppercase;margin-left: 35px;">Listado De Productos</h3>
      <ul style="width: 100%;list-style-type: none;text-align: center;display: flex;background:yelow;">
        <li style="width: 14%;border: 1px solid;">FECHA</li>
        <li style="width: 79%;border: 1px solid;">ARTICULO</li>
        <li style="width: 9%;border: 1px solid;">PRECIO</li>
      </ul>
      @foreach($ventas as $v)
      <ul style="width: 100%;list-style-type: none;display: flex;">
        <li style="width: 14%;border: 1px solid;text-align:center;">{{$v->created_at->format('d/m/Y')}}</li>
        <li style="width: 79%;border: 1px solid;">
          @foreach($v->articulos as $art)
            <span style="font-weight:bold;margin-left:10px;">Cantidad:</span>{{$art->cantidad}}<br>
            <span style="font-weight:bold;margin-left:10px;">Articulo:</span>{{$art->producto}}<br>
          @endforeach
        </li>
        <li style="width: 9%;border: 1px solid;text-align:center;">{{$v->total}}</li>
      </ul>
      @endforeach
    </div>
  <br clear=all>
  <div style="width: 95%;">
    <div style="margin-left: 35px;width: 100%;list-style-type: none;text-align: center;display: flex;background:yelow;">
    <h4 style="float:left;">TOTAL : ${{$ventas[0]->totalcaja}}</h4>
  </div>
  </div>
  </body>
</html>
