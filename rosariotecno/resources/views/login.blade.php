<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>RosarioTecno</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{url('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="{{url('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page" style="background:#ff5700de;">
@if(Session::get('mensaje'))
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="login-box" >
  <div class="card"  style="border-radius: 23px;">
    <div class="card-body login-card-body" style="background:black !important;border-radius: 23px;">
      <div class="login-logo">
        <img src="{{url('img/logo2.png')}}" style="width:100%;">
      </div>
      <form  method="post">
        {{ csrf_field() }}
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" name="email">
          <div class="input-group-append" style="background: #ffb76a;">
            <div class="input-group-text" style="color:black;">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="password">
          <div class="input-group-append" style="background: #ffb76a;">
            <div class="input-group-text" style="color:black;">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">INGRESAR</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{url('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('dist/js/adminlte.min.js')}}"></script>
</body>
</html>
