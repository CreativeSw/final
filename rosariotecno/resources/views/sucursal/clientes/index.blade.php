@extends('layouts.sucursal')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="content-header">
  <div class="container-fluid">
    <div class="col-2" style="float: right;">
      <a href="{{url('sucursal/clientes/nuevo')}}" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">NUEVO</a>
    </div>
    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
              <th>NOMBRE Y APELLIDO</th>
              <th>CONTACTO</th>
              <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($clientes as $c)
           <tr>
               <td>{{$c->nombre}}</td>
               <td><span>Telefono: </span>{{$c->telefono}}<br>
                   <span>Email: </span> {{$c->email}}
               </td>
               <td>
                 <a href="{{url('sucursal/clientes/editar',[$c->id])}}"><i class="fa fa-pen" aria-hidden="true"></i></a>

               </td>
           </tr>
           @endforeach

        </tbody>
        <tfoot>
            <tr>
              <th>NOMBRE Y APELLIDO</th>
              <th>CONTACTO</th>
              <th>ACCIONES</th>
            </tr>
        </tfoot>
    </table>
        </div>
      </div>
    </div>
  </div>
  <script>
  $(document).ready(function() {
      $('#example').DataTable({
        "bInfo" : false
      });
  } );
  </script>
@endsection
