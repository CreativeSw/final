@extends('layouts.sucursal')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Venta</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Cliente</label>
                <select class="form-control cliente" name="cliente"
                @if(!isset($venta)) required @endif >
                  <option value="">SELECIONE UNA</option>
                  @foreach($clientes as $c)
                  <option value="{{$c->id}}"
                    @if(isset($venta))
                      @if($venta->cliente == $c->id)
                        selected="selected"
                      @endif
                    @endif
                    >{{$c->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >Moneda</label>
                <select class="form-control monedas" name="moneda"
                @if(!isset($venta)) required @endif  id="formadepago">
                  <option value="">SELECIONE UNA</option>
                  @foreach($monedas as $c)
                  <option value="{{$c->id}}"
                    @if(isset($venta))
                      @if($venta->id == $c->id)
                        selected="selected"
                      @endif
                    @endif
                    >{{$c->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <input type="text" class="form-control" placeholder="IMEI" id="imei" >
            </div>
          </div>
          <br clear=all>
          <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
              <td>Producto</td>
              <td>IMEI</td>
              <td>Cantidad</td>
              <td>Precio</td>
              <td>SubTotal</td>
              <td>Accion</td>
              </tr>
            </thead>
            <tbody id="subproducto">
              @if(isset($venta))
              @foreach($venta->articulos as $art)
              <tr>
              <td>{{$art->producto}}</td>
              <td>
                @foreach(explode(',',$art->imei) as $imei)
                {{$imei}}<br>
                @endforeach
              </td>
              <td>{{$art->cantidad}}</td>
              <td>{{$art->precio/$art->cantidad}}</td>
              <td>{{$art->precio}}</td>
              <td>
                <a href="#" id="borrarajax" data-id="{{$art->id}}" data-precio="{{$art->precio}}" ><i class="fas fa-trash"></i></a>
              </td>
              </tr>
              @endforeach
              @endif
            </tbody>
            </table>
            <div class="row">
              <label style="float: right;font-weight: bold;font-size: 24px;margin-right: 0px;margin-left: 22px;text-transform: uppercase;">Entrega</label>
              <input type="number" step=0.001 name="entrega"
              @if(isset($venta))
              value="{{$venta->entrega}}"
              @else
              value="0"
              @endif
              >
            </div>
              <div class="row">
                <div class="col-md-8" >
                  <label style="float: right;font-weight: bold;font-size: 26px;margin-right: 0px;margin-left: 30px;">TOTAL: $</label>
                </div>
                <div class="col-md-4">
                  <input type="text" name="total" class="form-control" placeholder="Total" id="total"
                  default=0 >
                </div>
              </div>
          </div>
          <br clear=all>

          <div class="col-md-2" style="float:right;">
            <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;width:100%;">CONFIRMAR</button>
          </div>
          <div class="col-md-2" style="float:right;">
            <a href="{{url('sucursal/ventas')}}" class="btn btn-primary" style="background:#ff5700;border:none;float:right;width:100%;">ATRAS</a>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
<script>
$( document ).ready(function() {
  total = 0;
  @if(isset($venta))
  total = {{$venta->total}}
  @endif
  $('#total').val(total)
  $('.cliente').select2();
  $('.monedas').select2();
});

$('#imei').bind("keypress", function (e) {
  if (e.keyCode == 13) {
    return false;
  }
});

$('#imei').change(function(w){
  w.preventDefault()

  var imei = $('#imei').val()

  if(imei != "" ){
    $.get('/sucursal/ventas/producto/'+imei,function(re){
      console.log(re,'aca');
      if(re.success == false){
        Swal.fire(
          'Error',
          re.producto,
          'success'
        )
        $('#imei').val('')
      }else{
        /*

        */
        html = ''
        html += '<tr>'
        html += '<td>'
        html += re.producto.modelo
        html += '<input type="hidden" name="producto[]" value="'+re.producto.id+'">'
        html += '</td>'
        html += '<td>'
        html += re.producto.imei
        html+='<input type="hidden" name="imei[]" value="'+re.producto.imei+'" >'
        html+='</td>'
        html += '<td style="width:10%;">'
        html += re.producto.cantidad
        html += '</td>'
        html += '<td style="width:10%;">'
        html += re.producto.precio
        html += '<input type="hidden" name="precio[]" value="'+re.producto.precio+'">'
        html += '</td>'
        html += '<td style="width:10%;">'
        html +=  parseFloat(re.producto.precio)
        html += '<input type="hidden" name="cantidad[]" value="'+re.producto.cantidad+'">'
        html += '</td>'
        html += '<td style="width:10%;">'
        html += '<a href="#" class="borrarvista" onclick="borrarproducto('+ parseFloat(re.producto.precio)+')"  ><i class="fas fa-trash"></i></a>'
        html+= '</td>'
        html += '</tr>'

        total = total + parseFloat(re.producto.precio)
        $('#total').val(total)
        $('#imei').val('')
        $('#addproducto').prop('disabled','disabled')
        $('#subproducto').append(html)
        $('.borrarvista').click(function(re){
          re.preventDefault()
          $(this).parent().parent().remove()
        })
        console.log(re.producto)
      }
    })
  }
})



$('#borrarajax').click(function(re){
  re.preventDefault()
  borrar = $(this).parent().parent()
  precio = parseFloat($(this).data('precio'))
  total = total - precio
  $('#total').val(total)
  $(this).parent().parent().remove()
  $.get('borrar/'+$(this).data('id'),function(res){
    console.log(borrar)
  })
})

$('#idproducto').change(function(event){
  event.preventDefault()
  $('#cantidad').val(' ')
  $('#cantidad').attr('disabled','disabled')
  $('#addproducto').attr('disabled','disabled')
  $.get('producto/'+$('#formadepago').val()+'/'+$(this).val(),function(res){
      if('mensaje' in res){
        Swal.fire(
          'Existen en otra sucursal',
          res.mensaje,
          'success'
        )
      }else{
        $('#cantidad').removeAttr('disabled')
        $('#addproducto').removeAttr('disabled')
        $('#imei').removeAttr('disabled')
        $('#cantidad').val(res.cantidad)
        $('#cantidad').attr('min',1)
        $('#cantidad').attr('max',res.cantidad)
        $('#cantidad').data('precio',res.precio)
        $('#precio').attr('min',0);
        $('#precio').val(res.precio)
        $('#precio').removeAttr('disabled')
      }
    })
  })

  $('#precio').change(function(e){
    e.preventDefault()
    $('#cantidad').data('precio',$(this).val())
  })


  $('#addproducto').click(function(event){
    event.preventDefault()
    listimei = []
    listimei = $('#imei').val().split(',')
    if($('#cantidad').val() != listimei.length){
      Swal.fire(
        'Error',
        'No ingreso, la cantidad de imei correspondiente<br> a la cantidad de unidades vendidas',
        'success'
      )
    }else{
      html = ''
      html += '<tr>'
      html += '<td>'
      html += $( "#idproducto option:selected" ).text()
      html += '<input type="hidden" name="producto[]" value="'+$( "#idproducto option:selected" ).val()+'">'
      html += '</td>'
      html += '<td>'
      for(i=0;i<listimei.length;i++){
          html += listimei[i]
          html+='<br>'
      }
      html+='<input type="hidden" name="imei[]" value="'+$('#imei').val()+'" >'
      html+='</td>'
      html += '<td style="width:10%;">'
      html += $('#cantidad').val()
      html += '</td>'
      html += '<td style="width:10%;">'
      html += $('#cantidad').data('precio')
      html += '<input type="hidden" name="precio[]" value="'+$('#cantidad').data('precio')+'">'
      html += '</td>'
      html += '<td style="width:10%;">'
      html += parseFloat($('#cantidad').val())* parseFloat($('#cantidad').data('precio'))
      html += '<input type="hidden" name="cantidad[]" value="'+$('#cantidad').val()+'">'
      html += '</td>'
      html += '<td style="width:10%;">'
      html += '<a href="#" class="borrarvista" onclick="borrarproducto('+parseFloat($('#cantidad').val())* parseFloat($('#cantidad').data('precio'))+')"  ><i class="fas fa-trash"></i></a>'
      html+= '</td>'
      html += '</tr>'
      total = total + parseFloat($('#cantidad').val())*parseFloat($('#cantidad').data('precio'))
      $('#total').val(total)
      $('#idproducto').val("")
      $('#cantidad').val(' ')
      $('#imei').val(' ')
      $('#imei').prop('disabled','disabled')
      $('#cantidad').prop('disabled','disabled')
      $('#addproducto').prop('disabled','disabled')
      $('#subproducto').append(html)
      $('.borrarvista').click(function(re){
        re.preventDefault()
        $(this).parent().parent().remove()
      })
    }
  })
  function borrarproducto(subtotal){
    total = total - subtotal;
    if( total <= 0){
      total = 0
    }
    $('#total').val(total)
  }

</script>
@endsection
