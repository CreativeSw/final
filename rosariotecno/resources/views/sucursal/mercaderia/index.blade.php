@extends('layouts.sucursal')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="content-header">
  <div class="container-fluid">
    <div class="col-2" style="float: right;">
      <a href="{{url('sucursal/mercaderia/nuevo')}}" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">NUEVO</a>
    </div>
    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>FECHA</th>
            <th>DESCRIPCION</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($ingresos as $i)
          <tr>

          <td>{{$i->created_at->format('d/m/y')}}</td>
          <td><span style="font-weight: bold;">Modelo: </span>{{$i->producto}}<br>
              <span style="font-weight: bold;">Marca: </span>{{$i->marca}}<br>
              <span style="font-weight: bold;">Categoria: </span>{{$i->categoria}}<br>
              <span style="font-weight: bold;">Cantidad: </span>{{$i->cantidad}}
          </td>
          <td>

            <a href="{{url('sucursal/mercaderia/editar',[$i->id])}}"><i class="fa fa-pen" aria-hidden="true"></i></a>
            <a href="" data-id="{{$i->id}}" class="borraringreso"><i class="fas fa-trash"></i></a>

          </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>FECHA</th>
                <th>DESCRIPCION</th>
                <th>ACCIONES</th>
            </tr>
        </tfoot>
    </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({
      "ordering": false
    });
} );

$('.borraringreso').click(function(event){
  event.preventDefault();
  id = $(this).data('id');
  borrar = $(this).parent().parent()
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })
  swalWithBootstrapButtons.fire({
    title: 'Borrar',
    text: "Esta seguro que desea eliminar este Ingreso",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      url = 'mercaderia/borrar/'+id
      $.get(url, function(res){
        swalWithBootstrapButtons.fire(
          'Borrado!',
          res.success,
          'success'
        )
        borrar.remove()
      })

    } else if ( result.dismiss === Swal.DismissReason.cancel) {
      swalWithBootstrapButtons.fire(
        'Cancelado',
        'Has cancelado la eliminación de la categoria',
        'error'
      )
    }
  })
})

</script>
@endsection
