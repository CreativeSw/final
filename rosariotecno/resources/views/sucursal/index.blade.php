@extends('layouts.sucursal')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background: #ffab00 !important;">
          <div class="inner">
            <h5>CLIENTES</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-users"></i>
          </div>
          <a href="{{url('sucursal/clientes')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h5>MONEDAS</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-money-bill-wave"></i>
          </div>
          <a href="{{url('sucursal/monedas')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background: #3becd0 !important;">
          <div class="inner">
            <h5>VENTAS</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-shopping-basket"></i>
          </div>
          <a href="{{url('sucursal/ventas')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background: #b43bec !important;">
          <div class="inner">
            <h5>INGRESO MERCADERIA</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-dolly"></i>
          </div>
          <a href="{{url('sucursal/mercaderia')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background: #f7d50b !important;">
          <div class="inner">
            <h5>MOVER MERCADERIA</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-shipping-fast"></i>
          </div>
          <a href="{{url('sucursal/movermercaderia')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background:#1f8af7 !important;">
          <div class="inner">
            <h5>STOCK</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-truck"></i>
          </div>
          <a href="{{url('sucursal/stock')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background:#1f8af7 !important;">
          <div class="inner">
            <h5>STOCK GRAL</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-truck"></i>
          </div>
          <a href="{{url('sucursal/stockgral')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background:#1f8af7 !important;">
          <div class="inner">
            <h5>STOCK GENERAL</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-truck"></i>
          </div>
          <a href="{{url('sucursal/stockral')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background:#f71fac !important;">
          <div class="inner">
            <h5>GARANTIA</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-clipboard-list"></i>
          </div>
          <a href="{{url('sucursal/garantia')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-warning" style="background: #c0c1c0 !important;">
          <div class="inner">
            <h5>CONFIGURACION</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-cogs"></i>
          </div>
          <a href="{{url('sucursal/configuracion')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-danger">
          <div class="inner">
            <h5>SALIR</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-window-close"></i>
          </div>
          <a href="{{url('salir')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
