@extends('layouts.sucursal')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="col-2" style="float: right;">
      <a href="{{url('sucursal/cerrarcaja')}}" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">CERRAR</a>
    </div>

    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>FECHA</th>
                <th>MONTO</th>
                <th>ACCIONES</th>
              </tr>
            </thead>
            <tbody>
            @foreach($cajas as $v)
            <tr>
              <td style="width:10%;">{{$v->created_at->format('d/m/Y')}}<br>
              </td>
              <td style="width:10%;">${{$v->total}}</td>
              <td style="width:10%;">
                  <a href="{{url('sucursal/ventas/cerrarcaja/ver',[$v->id])}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
              </td>
            </tr>
            @endforeach
            </tbody>

          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({
       responsive: true
    });
} );

$('#cierrecaja').click(function(e){
  e.preventDefault()
  console.log('clikkk')
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })
  swalWithBootstrapButtons.fire({
    title: 'Cierre de caja',
    text: "Esta seguro que desea cerrar la CAJA",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      url = 'ventas/cerrarcaja'
      $.get(url, function(res){
        swalWithBootstrapButtons.fire(
          'cerrado!',
          'Se descargo con exito, el informe',
          'success'
        )
      })
    } else if ( result.dismiss === Swal.DismissReason.cancel) {
      swalWithBootstrapButtons.fire(
        'Cancelado',
        'Has cancelado el fin de caja',
        'error'
      )
    }
  })
})


$('.borrarventas').click(function(event){
  event.preventDefault();
  id = $(this).data('id');
  borrar = $(this).parent().parent()
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })
  swalWithBootstrapButtons.fire({
    title: 'Borrar',
    text: "Esta seguro que desea eliminar este Ingreso",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      url = 'ventas/borrar/'+id
      $.get(url, function(res){
        swalWithBootstrapButtons.fire(
          'Borrado!',
          res.success,
          'success'
        )
        borrar.remove()
      })

    } else if ( result.dismiss === Swal.DismissReason.cancel) {
      swalWithBootstrapButtons.fire(
        'Cancelado',
        'Has cancelado la eliminación de la categoria',
        'error'
      )
    }
  })
})
</script>
@endsection
