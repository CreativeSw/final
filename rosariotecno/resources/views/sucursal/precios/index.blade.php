@extends('layouts.sucursal')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif

<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Producto</h3>
      </div>
      <form method="post" enctype="multipart/form-data">



              {{ csrf_field() }}
              <div class="card-body">


                <div class="col-md-10">
                  <p>Seleccione archivo para actualizar los precios</p>
                  <input type="file" name="archivo" required>
                </div>


              <div class="col-md-2" style="float: right;">
              <input type="submit" class="btn btn-primary btn-block" style="background:#ff5700;border:none;" value="ACTUALIZAR">
            </div>
            </form>
          </div>
    </div>
        </div>
      </div>
@endsection
