@extends('layouts.sucursal')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="content-header">
  <div class="container-fluid">
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr>
              <th>NOMBRE</th>
              <th>COTIZACION</th>
              <th>ACCIONES</th>
            </tr>
          </thead>
          <tbody>
          @foreach($monedas as $m)
            <tr>
              <td>{{$m->nombre}}</td>
              <td>{{$m->cotizacion}}</td>
              <td>
                <a href="{{url('contabilidad/monedas/editar',[$m->id])}}"><i class="fa fa-pen" aria-hidden="true"></i></a>

              </td>
            <tr>
          @endforeach
          </tbody>
          <tfoot>
          <tr>
          <th>NOMBRE</th>
          <th>COTIZACION</th>
          <th>ACCIONES</th>
          </tr>
          </tfoot>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );

</script>
@endsection
