@extends('layouts.sucursal')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">MONEDA</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label >NOMBRE</label>
                <input type="text" class="form-control" placeholder="NOMBRE" name=nombre
                @if(isset($moneda))
                value="{{$moneda->nombre}}"
                @else
                required
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">COTIZACION</label>
                <input type="number" class="form-control" placeholder="CATIZACION" name="cotizacion"
                @if(isset($moneda))
                value="{{$moneda->cotizacion}}"
                @else
                required
                @endif
                step="0.01"
                >
              </div>
            </div>
          </div>
          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
            @if(isset($moneda))
            ACTUALIZAR
            @else
            CREAR
            @endif
          </button>
        </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
