@extends('layouts.sucursal')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Mover Mercaderia</h3>
      </div>
      <form  method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <label for="exampleInputEmail1">DESTINO</label>
                  <select class="form-control destino" name="idsucursaldestino"
                  @if(isset($producto))
                   disabled
                   @else
                   required
                   @endif >
                    <option value="">SELECIONE UNA</option>
                    @foreach($destinos as $c)
                    <option value="{{$c->id}}"
                      @if(isset($producto))
                        @if($producto->idsucursal == $c->id)
                        selected="selected"
                        @endif
                      @endif
                      >{{$c->nombre}}</option>
                    @endforeach
                  </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label for="exampleInputEmail1">Producto</label>
                <select class="form-control producto" name="idproducto" id="idproducto"
                @if(isset($producto))
                disabled
                @else
                required
                @endif
                >
                 <option value="">SELECIONE UNA</option>
                 @foreach($productos as $p)
                 <option value="{{$p->idproducto}}" data-cantidad="{{$p->cantidad}}" data-idmoneda="{{$p->idmoneda}}"
                   data-precio="{{$p->precio}}"
                   @if(isset($producto))
                     @if($producto->idproducto == $p->idproducto)
                     selected="selected"
                     @endif

                   @endif
                   >{{$p->producto}}</option>
                 @endforeach
               </select>
              </div>

            </div>
            <div class="col-md-4 ">
              <div class="form-group">
                <label >Cantidad</label>
                <input type="number" class="form-control" name=cantidad placeholder="CANTIDAD" id="cantidad"
                @if(isset($producto))
                value="{{$producto->cantidad}}"
                readonly
                @else
                required
                disabled
                @endif >
              </div>
            </div>
            <div class="col-md-12">
              <label>IMEIS</label>
              <textarea class="form-control" name="imeis" @if(isset($producto)) readonly @endif>@if(isset($producto)){{$producto->imeis}}@endif</textarea>
            </div>
          </div>
          <br clear=all>
          <div class="col-md-4" style="float:right;">
          @if(isset($producto))
            <a href="{{url('sucursal/movermercaderia')}}" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">ATRAS</a>
          @else
            <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">REGISTRAR</button>
          @endif
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    
    $('.destino').select2()
    $('.producto').select2()
});

$('#idproducto').change(function(e){
  e.preventDefault()
  $('#cantidad').removeAttr('disabled');
  $('#cantidad').val($(this).find(':selected').data('cantidad'))
  $('<input>').attr({
    type: 'hidden',
    name: 'idmoneda',
    value:$(this).find(':selected').data('idmoneda')
  }).appendTo('form');

  $('<input>').attr({
    type: 'hidden',
    name: 'precio',
    value:$(this).find(':selected').data('precio')
  }).appendTo('form');
})



</script>
@endsection
