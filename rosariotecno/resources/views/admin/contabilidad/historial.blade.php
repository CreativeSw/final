@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="col-2" style="float: right;">
      <a href="#" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">NUEVO</a>
    </div>
    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
              <th>FECHA</th>
              <th>MONTO</th>
              <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
            <tr>
                <td>Herrod Chandler</td>
                <td>San Francisco</td>
                <td></td>
            </tr>
            <tr>
                <td>Rhona Davidson</td>
                <td>Tokyo</td>
                <td></td>
            </tr>
            <tr>
                <td>Colleen Hurst</td>
                <td>San Francisco</td>
                <td></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
              <th>FECHA</th>
              <th>MONTO</th>
              <th>ACCIONES</th>
            </tr>
        </tfoot>
    </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
