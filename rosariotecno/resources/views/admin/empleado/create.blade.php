@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Empleado</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>NOMBRE</label>
                <input type="text" class="form-control" placeholder="NOMBRE" name="nombre"
                @if(isset($cliente))
                value="{{$cliente->nombre}}"
                @else
                required
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>EMAIL</label>
                <input type="email" class="form-control" name="email" placeholder="EMAIL"
                @if(isset($cliente))
                value="{{$cliente->email}}"
                @else
                required
                @endif
                >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label >DIRECCION</label>
                <input type="text" class="form-control" placeholder="DIRECCION"
                name=direccion
                @if(isset($cliente))
                value="{{$cliente->direccion}}"
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >TELEFONO</label>
                <input type="text" class="form-control"  placeholder="TELEFONO"
                name="telefono"
                @if(isset($cliente))
                value="{{$cliente->telefono}}"
                @endif
                >
              </div>
            </div>
          </div>
          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
          @if(isset($cliente))
          ACTUALIZAR
          @else
          CREAR
          @endif
        </button>
        </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
