@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Venta</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Cliente</label>
                <select class="form-control" name="cliente" id="cliente"
                @if(!isset($venta)) required @endif >
                  <option value="">SELECIONE UNA</option>
                  @foreach($clientes as $c)
                  <option value="{{$c->id}}"
                    @if(isset($venta))
                      @if($venta->cliente == $c->id)
                        selected="selected"
                      @endif
                    @endif
                    >{{$c->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>IMEI</label>
                <input type="text" class="form-control" placeholder="IMEI" name="imei" id="imei" required>
              </div>
            </div>
          </div>
          <div class="col-md-2" style="float:right;">
            <button type="submit" id="enviar" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">CONFIRMAR</button>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>

<script>
$('#enviar').click(function(e){
  e.preventDefault()

  $.get('/sucursal/garantia/'+$('#imei').val()+'/'+$('#cliente').val(),function(res){
    console.log(res)
    Swal.fire(
  'Garantia',
  res,
  'success'
)
  })
})
</script>
@endsection
