@extends('layouts.admin')
@section('content')
<br clear=all>
<br clear=all>
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      @foreach($depositos as $d)
      <div class="col-lg-4 col-6">
      <div class="small-box bg-success"   @if(isset($d->estado))
        style="background: #FF6C21 !important;"
        @endif>
        <div class="inner">
          <h3>Deposito</h3>
          <br clear=all>
          <span style="font-weight: bold;">Dirección: </span>{{$d->direccion}}<br>
          <span style="font-weight: bold;">Telefono: </span>{{$d->telefono}}<br>
        </div>
        <div class="icon">
          <i class="fas fa-warehouse"></i>
        </div>
        <a href="{{url('stock',[$d->id])}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
      </div>
      </div>
      @endforeach
      @foreach($sucursales as $d)

      <div class="col-lg-4 col-6">
      <div class="small-box bg-success"
      @if(isset($d->estado))
      style="background: #FF6C21 !important;"
      @endif
      >
        <div class="inner">
          <h3>Sucursal</h3>
          <br clear=all>
          <span style="font-weight: bold;">Dirección: </span>{{$d->direccion}}<br>
          <span style="font-weight: bold;">Telefono: </span>{{$d->telefono}}<br>
        </div>
        <div class="icon">
          <i class="fas fa-store-alt"></i>
        </div>
        <a href="{{url('stock',[$d->id])}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
      </div>
      </div>
      @endforeach


    </div>
  </div>
</div>
@endsection
