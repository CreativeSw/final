@extends('layouts.admin')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="content-header">
  <div class="container-fluid">
      <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>MARCA</th>
            <th>MODELO</th>
            <th>CANTIDAD VENDIDA</th>

          </tr>
        </thead>
        <tbody>
          @foreach($cantidadcategoria as $k =>$c)

           <tr>
             <td>{{$marca->nombre}}</td>
             <td>{{$k}}</td>
             <td>{{$c}}</td>

           </tr>
           @endforeach

        </tbody>
        <tfoot>
            <tr>
              <th>MARCA</th>
              <th>MODELO</th>
              <th>CANTIDAD VENDIDA</th>
            </tr>
        </tfoot>
    </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );

</script>
@endsection
