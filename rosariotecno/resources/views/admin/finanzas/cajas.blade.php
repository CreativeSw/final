@extends('layouts.admin')
@section('content')
<br clear=all>
<br clear=all>
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      @foreach($sucursales as $d)
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>Sucursal</h3>
            <br clear=all>
            <span style="font-weight: bold;">Dirección: </span>{{$d->direccion}}<br>
            <span style="font-weight: bold;">Telefono: </span>{{$d->telefono}}<br>
          </div>
          <div class="icon">
            <i class="fas fa-store-alt"></i>
          </div>
          <a href="{{url('finanzas/cajas',[$d->id])}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
@endsection
