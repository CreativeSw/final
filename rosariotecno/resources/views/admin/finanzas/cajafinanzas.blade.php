@extends('layouts.admin')
@section('content')
<br clear=all>
<div class="col-md-12" style="background: orange;
width: 50%;
float: right;
text-align: right;
padding: 20px;
border-radius: 10px;">
<h4 style="font-weight: bold;text-transform: full-width;">Total en Caja: </h4><span style="color: #fbfbfb;

font-weight: bold;

font-size: 34px;">${{$totalencaja}}</span>
</div>
<br clear=all>
<div class="col-md-12">

  <form method="post">
    {{ csrf_field() }}
    <div class="card-body">
      <div class="row" style="background: gray;
padding: 20px;
border-radius: 20px;">
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">FECHA INICIO</label>
            <input type="date" class="form-control" placeholder="FECHA INICIO" name="fechainicio" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label >FECHA FIN</label>
            <input type="date" class="form-control" placeholder="FECHA FIN" name="fechafin" required>
          </div>
        </div>
        <div class="col-md-12" style="float:right;">
        <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
          VER
        </button>
      </div>
      </div>
</div>
</form>
</div>
<br clear=all>
<div class="row">
  <div class="col-md-4">
    <canvas id="formadepagos"></canvas>
  </div>
  <div class="col-md-4">
    <canvas id="categoria"></canvas>
    <br clear=all>
    <div style="display: grid;">
    @foreach($todaslascategorias as $tc)
    <a href="{{url('informes/categoria',[$id,$tc->id])}}" style="width: 70%;background: #c8d5c8;
padding: 5px;
color: black;
text-align: left;
border-radius: 15px; margin-bottom: 5px;">{{$tc->nombre}}</a>
    @endforeach
  </div>
</div>
  <div class="col-md-4">
    <canvas id="ventasgastos"></canvas>
    <br clear=all>
      <div style="display: grid;">
        <a href="{{url('admin/gastos')}}" style="width: 70%;background: #c8d5c8;
    padding: 5px;
    color: black;
    text-align: left;
    border-radius: 15px; margin-bottom: 5px;">GASTOS</a>

      <a href="{{url('admin/ventas')}}" style="width: 70%;background: #c8d5c8;
  padding: 5px;
  color: black;
  text-align: left;
  border-radius: 15px; margin-bottom: 5px;">VENTAS</a>

      </div>
  </div>
  <br clear=all>
  <br clear=all>
  <div class="col-md-12">
    <br clear=all>
    <canvas id="speedChart" width="600" height="400"></canvas>
  </div>
  <br clear=all>
  <br clear=all>
  <div class="col-md-12">
    <br clear=all>
    <canvas id="speedgancias" width="600" height="400"></canvas>
  </div>
</div>
<script>
new Chart(document.getElementById("formadepagos"), {
    type: 'pie',
    data: {
      labels: [@foreach($forma as $f)"{{$f}}",@endforeach ],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        data:[@foreach($cantidadforma as $c){{$c}},@endforeach]
      }]
    },
    options: {
      title: {
        display: true,
        text: 'Formas de Pago'
      }
    }
});


new Chart(document.getElementById("categoria"), {
    type: 'pie',
    data: {
      labels: [@foreach($categoria as $f)"{{$f}}",@endforeach ],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        data:[@foreach($cantidadcategoria as $c){{$c}},@endforeach]
      }]
    },
    options: {
      title: {
        display: true,
        text: 'Categoria más Vendidas'
      }
    }
});
new Chart(document.getElementById("ventasgastos"), {
    type: 'pie',
    data: {
      labels: ['Gastos','Ventas'],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#3e95cd", "#8e5ea2"],
        data:[{{$gastotorta}}, {{$ventastorta}}]
      }]
    },
    options: {
      title: {
        display: true,
        text: 'Categoria más Vendidas'
      }
    }
});


var speedCanvas = document.getElementById("speedChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 14;

var speedData = {
  labels:

[0,@foreach($todaslasventas as $y=>$f)"{{$y}}",@endforeach ]
  ,
  datasets: [{
    label: "Ventas",
    data: [0,@foreach($todaslasventas as $y=>$f)"{{$f}}",@endforeach ],
    lineTension: 0,
    fill: false,
    borderColor: 'orange',
    backgroundColor: 'transparent',
    borderDash: [5, 5],
    pointBorderColor: 'orange',
    pointBackgroundColor: 'rgba(255,150,0,0.5)',
    pointRadius: 5,
    pointHoverRadius: 10,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    pointStyle: 'rectRounded'
  }]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 80,
      fontColor: 'black'
    }
  }
};

var lineChart = new Chart(speedCanvas, {
  type: 'line',
  data: speedData,
  options: chartOptions
});



//speedgancias
var speedC = document.getElementById("speedgancias");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 14;

var speedD = {
  labels:

[0,@foreach($todaslasventas as $y=>$f)"{{$y}}",@endforeach ]
  ,
  datasets: [{
    label: "Ganacias",
    data: [0,@foreach($todaslasganancias as $y=>$f)"{{$f}}",@endforeach ],
    lineTension: 0,
    fill: false,
    borderColor: 'orange',
    backgroundColor: 'transparent',
    borderDash: [5, 5],
    pointBorderColor: 'orange',
    pointBackgroundColor: 'rgba(255,150,0,0.5)',
    pointRadius: 5,
    pointHoverRadius: 10,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    pointStyle: 'rectRounded'
  }]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 80,
      fontColor: 'black'
    }
  }
};

var lineChart = new Chart(speedC, {
  type: 'line',
  data: speedD,
  options: chartOptions
});


</script>

@endsection
