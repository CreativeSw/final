@extends('layouts.admin')
@section('content')

<br clear=all>
<div class="row">
  <div class="col-md-8">
    <br clear=all>
    <canvas id="speedChart" width="600" height="400"></canvas>
  </div>
  <div class="col-md-4">
    <canvas id="categoria"></canvas>
    <br clear=all>
    <div style="display: grid;">
    @foreach($todaslascategorias as $tc)
    @if(isset($sucursal))
    <a href="{{url('informes/marcas',[$id,$tc->id])}}" style="width: 70%;background: #c8d5c8;
padding: 5px;
color: black;
text-align: left;
border-radius: 15px; margin-bottom: 5px;">{{$tc->nombre}}</a>
    @else
    <a href="{{url('informes/marcas',$tc->id)}}" style="width: 70%;background: #c8d5c8;
padding: 5px;
color: black;
text-align: left;
border-radius: 15px; margin-bottom: 5px;">{{$tc->nombre}}</a>
    @endif
    @endforeach

  </div>
</div>
  <br clear=all>
  <br clear=all>
</div>
<script>


new Chart(document.getElementById("categoria"), {
    type: 'pie',
    data: {
      labels: [@foreach($categoria as $f)"{{$f}}",@endforeach ],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        data:[@foreach($cantidadcategoria as $c){{$c}},@endforeach]
      }]
    },
    options: {
      title: {
        display: true,
        text: 'Categoria más Vendidas'
      }
    }
});



var speedCanvas = document.getElementById("speedChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 14;

var speedData = {
  labels:

[0,@foreach($todaslasventas as $y=>$f)"{{$y}}",@endforeach ]
  ,
  datasets: [{
    label: "Ventas",
    data: [0,@foreach($todaslasventas as $y=>$f)"{{$f}}",@endforeach ],
    lineTension: 0,
    fill: false,
    borderColor: 'orange',
    backgroundColor: 'transparent',
    borderDash: [5, 5],
    pointBorderColor: 'orange',
    pointBackgroundColor: 'rgba(255,150,0,0.5)',
    pointRadius: 5,
    pointHoverRadius: 10,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    pointStyle: 'rectRounded'
  }]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 80,
      fontColor: 'black'
    }
  }
};

var lineChart = new Chart(speedCanvas, {
  type: 'line',
  data: speedData,
  options: chartOptions
});



</script>

@endsection
