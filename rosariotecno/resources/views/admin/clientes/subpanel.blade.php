@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>CLIENTES</h3>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-users"></i>
          </div>
          <a href="{{url('clientes')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>USUARIOS</h3>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-users"></i>
          </div>
          <a href="{{url('usuarios')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>EMPLEADOS</h3>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-users"></i>
          </div>
          <a href="{{url('empleados')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
