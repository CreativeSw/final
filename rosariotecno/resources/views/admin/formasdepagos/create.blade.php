@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Forma de Pago</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label >Forma de Pago</label>
                <input type="text" class="form-control" placeholder="FORMA DE PAGO" name="nombre"
                @if(isset($forma))
                  value="{{$forma->nombre}}"
                @else
                  required
                @endif
                >
              </div>
            </div>

          </div>


          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
            @if(isset($forma))
            ACTUALIZAR
            @else
            CREAR
            @endif
          </button>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
