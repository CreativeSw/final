@extends('layouts.admin')
@section('content')
<br clear=all>
<br clear=all>
<br clear=all>
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-2 col-6"></div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>Forma de Pago</h3>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-credit-card"></i>
          </div>
          <a href="{{url('formadepagos')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>Monedas</h3>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-money-bill-wave"></i>
          </div>
          <a href="{{url('contabilidad/monedas')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
