@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">USUARIO</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label >NOMBRE</label>
                <input type="text" class="form-control" name="nombre" placeholder="NOMBRE"
                @if(isset($usuario))
                value="{{$usuario->nombre}}"
                @else
                required
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >TIPO</label>
                <select class="form-control" name="rol"
                  @if(!isset($usuario))
                  required
                  @endif
                  >
                  <option value="" >SELECIONE </option>
                  <option value="deposito"
                  @if(isset($usuario))
                    @if($usuario->rol == "deposito")
                      selected
                    @endif
                  @endif
                  >DEPOSITO</option>
                  <option value="sucursal"
                  @if(isset($usuario))
                    @if($usuario->rol == "sucursal")
                      selected
                    @endif
                  @endif

                  >SUCURSAL</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label >DIRECCION</label>
                <input type="text" class="form-control" name="direccion" placeholder="DIRECCION"
                @if(isset($usuario))
                value="{{$usuario->direccion}}"
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >TELEFONO</label>
                <input type="text" class="form-control" name="telefono" placeholder="TELEFONO"
                @if(isset($usuario))
                value="{{$usuario->telefono}}"
                @endif
                >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label >EMAIL</label>
                <input type="email" class="form-control" name=email placeholder="EMAIL"
                @if(isset($usuario))
                value="{{$usuario->email}}"
                @else
                required
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >CLAVE</label>
                <input type="password" class="form-control" name="password" placeholder="CLAVE"
                @if(!isset($usuario))
                required
                @endif
                >
              </div>
            </div>
          </div>
          <div class="col-md-4" style="float:right;">
          @if(isset($usuario))
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">ACTUALIZAR</button>
          @else
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">CREAR</button>
          @endif
        </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
