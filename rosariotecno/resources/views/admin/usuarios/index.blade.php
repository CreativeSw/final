@extends('layouts.admin')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="content-header">
  <div class="container-fluid">
    <div class="col-2" style="float: right;">
      <a href="{{url('usuarios/nuevo')}}" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">NUEVO</a>
    </div>
    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
              <th>NOMBRE Y APELLIDO</th>
              <th>CONTACTO</th>
              <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($usuarios as $u)
          <tr>
            <td>{{$u->nombre}}</td>
            <td>
              <span style="font-weight: bold;">Telefono: </span>{{$u->telefono}}<br>
              <span style="font-weight: bold;">Direccion: </span>{{$u->direccion}}<br>
              <span style="font-weight: bold;">Rol: </span>{{$u->rol}}<br>
            </td>
            <td>
              <a href="{{url('usuarios/editar',[$u->id])}}"><i class="fa fa-pen" aria-hidden="true"></i></a>
              <a href="" data-id="{{$u->id}}" class="borrarusuario"><i class="fas fa-trash"></i></a>
            </td>
          </tr>
          @endforeach

        </tbody>
        <tfoot>
            <tr>
              <th>NOMBRE Y APELLIDO</th>
              <th>CONTACTO</th>
              <th>ACCIONES</th>
            </tr>
        </tfoot>
    </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );

$('.borrarusuario').click(function(event){
  event.preventDefault();
  id = $(this).data('id');
  borrar = $(this).parent().parent()
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })
  swalWithBootstrapButtons.fire({
    title: 'Borrar',
    text: "Esta seguro que desea eliminar este usuario",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      url = 'usuarios/borrar/'+id
      $.get(url, function(res){
        swalWithBootstrapButtons.fire(
          'Borrado!',
          res.success,
          'success'
        )
        if(res.success == "El usuario fue borrado con exito."){
            borrar.remove()
        }
      })

    } else if ( result.dismiss === Swal.DismissReason.cancel) {
      swalWithBootstrapButtons.fire(
        'Cancelado',
        'Has cancelado la eliminación del usuario',
        'error'
      )
    }
  })
})
</script>
@endsection
