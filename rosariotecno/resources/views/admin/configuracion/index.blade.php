@extends('layouts.admin')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Configuracion</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">

            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">CLAVE</label>
                <input type="password" class="form-control" placeholder="CLAVE" name="password">
              </div>
            </div>

          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">GUARDAR</button>
        </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
