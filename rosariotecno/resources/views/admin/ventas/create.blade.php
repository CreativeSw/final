@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Venta</h3>
      </div>
      <form>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Cliente</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="CLIENTE">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Forma de Pago</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="FORMA DE PAGO">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label for="exampleInputEmail1">Producto</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Producto">
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label for="exampleInputEmail1">Cantidad</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Cantidad">
              </div>
            </div>
            <div class="col-md-1">
              <br clear=all>
              <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">Agregar</button>
            </div>
          </div>
          <div class="col-md-2" style="float:right;">
            <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">PRESUPUESTO</button>
          </div>
          <div class="col-md-2" style="float:right;">
            <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">CONFIRMAR</button>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
