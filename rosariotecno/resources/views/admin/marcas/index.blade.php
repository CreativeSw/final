@extends('layouts.admin')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="content-header">
  <div class="container-fluid">
    <div class="col-2" style="float: right;">
      <a href="{{url('marcas/nuevo')}}" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">NUEVO</a>
    </div>
    <div class="col-2" style="float: right;">
      <a href="#" class="btn btn-primary btn-block"  data-toggle="modal" data-target="#exampleModal" style="background:#ff5700;border:none;">IMPORTADOR</a>
    </div>
    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
              <th>NOMBRE</th>
              <th>CATEGORIA</th>
              <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($marcas as $m)
          <tr>
            <td>{{$m->nombre}}</td>
            <td>{{$m->categoria}}</td>
            <td>
              <a href="{{url('marcas/editar',[$m->id])}}"><i class="fa fa-pen" aria-hidden="true"></i></a>
              <a href="" data-id="{{$m->id}}" class="borrarmarca"><i class="fas fa-trash"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>NOMBRE</th>
                <th>CATEGORIA</th>
                <th>ACCIONES</th>
            </tr>
        </tfoot>
    </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #FF5700;color: white;">
        <h5 class="modal-title" id="exampleModalLabel">Importador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12">
              <p>Seleccione archivo para actualizar las marcas</p>
              <input type="file" name="archivo" required>
            </div>
          </div>
          <br clear=all>
          <input type="submit" class="btn btn-primary btn-block" style="background:#ff5700;border:none;" value="ACTUALIZAR">
        </form>
      </div>

    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );

$('.borrarmarca').click(function(event){
  event.preventDefault();
  id = $(this).data('id');
  borrar = $(this).parent().parent()
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })
  swalWithBootstrapButtons.fire({
    title: 'Borrar',
    text: "Esta seguro que desea eliminar esta marca",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      url = 'marcas/borrar/'+id
      $.get(url, function(res){
        swalWithBootstrapButtons.fire(
          'Borrado!',
          res.success,
          'success'
        )
        borrar.remove()
      })

    } else if ( result.dismiss === Swal.DismissReason.cancel) {
      swalWithBootstrapButtons.fire(
        'Cancelado',
        'Has cancelado la eliminación de la marca',
        'error'
      )
    }
  })
})
</script>
@endsection
