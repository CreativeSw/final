@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Marca</h3>
      </div>
      <form method="post">
          {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" name="nombre" placeholder="NOMBRE"
                @if(isset($marca))
                value="{{$marca->nombre}}"
                @else
                required
                @endif
                 >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >Categoria</label>
                <select class="form-control" name="idcategoria"  required>
                  <option value="">SELECIONE UNA</option>
                  @foreach($categorias as $c)
                  <option value="{{$c->id}}"
                    @if(isset($c['selecionado']))
                    selected="selected"
                    @endif
                    >{{$c->nombre}}</option>
                  @endforeach
                </select>

              </div>
            </div>
          </div>
          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
            @if(isset($marca))
            ACTUALIZAR
            @else
            CREAR
            @endif
          </button>
        </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
