@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Ingreso de Mercaderia</h3>
      </div>
      <form  method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
          <div class="form-group">
              <label for="exampleInputEmail1">CATEGORIA</label>
              <select class="form-control" name="idcategoria" id="categoria"
              @if(!isset($productosucursal))
              required
              @endif >
                <option value="">SELECIONE UNA</option>
                @foreach($categorias as $c)
                <option value="{{$c->id}}"
                  @if(isset($productosucursal))
                    @if($productosucursal->idcategoria == $c->id)
                      selected="selected"
                    @endif
                  @endif
                  >{{$c->nombre}}</option>
                @endforeach
              </select>
          </div>
        </div>
          <div class="col-md-6">
          <div class="form-group">
              <label for="exampleInputEmail1">MARCA</label>
              <select class="form-control" name="idmarca" id="marca"
              @if(!isset($productosucursal))
              required
              disabled
              @endif
              >
              @if(isset($productosucursal))
                <option value="">SELECIONE UNA</option>
                <option value="{{$productosucursal->idmarca}}" selected="selected">{{$productosucursal->marca}}</option>
              @endif
            </select>
          </div>
        </div>
          </div>
          <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">PRODUCTO</label>
              <select class="form-control" name="idproducto" id="producto"
              @if(!isset($productosucursal))
              required
              disabled
              @endif
               >
               @if(isset($productosucursal))
               <option value="">SELECIONE UNA</option>
               <option value="{{$productosucursal->idproducto}}" selected="selected">{{$productosucursal->producto}}</option>
               @endif
             </select>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">DESTINO</label>
                <select class="form-control" name="idsucursal"
                @if(!isset($productosucursal))
                 required
                 @endif >
                  <option value="">SELECIONE UNA</option>
                  @foreach($destinos as $c)
                  <option value="{{$c->id}}"
                    @if(isset($productosucursal))
                      @if($productosucursal->idsucursal == $c->id)
                      selected="selected"
                      @endif
                    @endif
                    >{{$c->nombre}}</option>
                  @endforeach
                </select>
            </div>
          </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="exampleInputEmail1">IMPORTADOR</label>
              <select class="form-control" name="idimportador"
              @if(!isset($productosucursal))
               required
               @endif >
                <option value="">SELECIONE UNA</option>
                @foreach($importadores as $c)
                <option value="{{$c->id}}"
                  @if(isset($productosucursal))
                    @if($productosucursal->idimportador == $c->id)
                    selected="selected"
                    @endif
                  @endif
                  >{{$c->razonsocial}}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-6">
              <label>COSTO TRASPORTE</label>
              <select class="form-control" name="idcosto" id="idcosto"
              @if(!isset($productosucursal))
               required
               @endif >
                <option value="">SELECIONE UNA</option>
                <option value="FIJO"
                @if(isset($productosucursal))
                @if($productosucursal->idcosto == "FIJO")
                selected=selected
                @endif
                @endif
                >FIJO</option>
                <option value="VARIABLE"
                @if(isset($productosucursal))
                @if($productosucursal->idcosto == "VARIABLE")
                selected=selected
                @endif
                @endif
                >VARIABLE</option>
              </select>
            </div>
          </div>
          <br clear=all>
          <div class="row">
            <div class="col-md-12" id="fijo"
            @if(isset($productosucursal))
              @if($productosucursal->idcosto == "FIJO")
                 style="display:block;"
              @else
               style="display:none;"
              @endif
            @else
              style="display:none;"
            @endif >
              <label>PORCENTAJE %</label>

              <input type="number" step="0.001" name="fijorecargo[]"  class="form-control"
              @if(isset($productosucursal))
              @foreach(json_decode($productosucursal->recargo) as $v)
              value = "{{$v}}"
              @endforeach
              @endif
               >
            </div>

            <div class="col-md-12" id="variable"
            @if(isset($productosucursal))
              @if($productosucursal->idcosto == "VARIABLE")
                 style="display:block;"
              @else
                style="display:none;"
              @endif
            @else
              style="display:none;"
            @endif >
              <div class="row">
              <div class="col-md-5">
                <label>CONCEPTO</label>
                <input type="text" name="variableconcepto" class="form-control" id="concepto">
              </div>
              <div class="col-md-5">
                <label>PORCENTAJE %</label>
                <input type="number" step="0.001" name="variablerecargo" class="form-control" id="recargo">
              </div>
              <div class="col-md-2">
                <button  class="btn btn-primary" id="agregarcosto" style="background: #ff5700;border: none;float: right;margin-top: 32px;width: 100%;">AGREGAR</button>
              </div>
              <br clear=all>
              <div class="col-md-12">
                <br clear=all>
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <td>DESCRIPCION</td>
                      <td>%RECARGO</td>
                      <td>ACCION</td>
                    </tr>
                  </thead>
                  <tbody id="listvariable">
                    @if(isset($productosucursal))
                    @foreach(json_decode($productosucursal->recargo) as $descripcion => $recargo )
                    <tr>
                      <td>{{$descripcion}}</td>
                      <td>{{$recargo}}</td>
                      <td style="width:10%;">
                        <a href="#" class="borrargastoajax" data-id="{{$productosucursal->id}}" data-borrar="{{$descripcion}}" ><i class="fas fa-trash"></i></a>
                      </td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
            </div>
          </div>
          <br clear=all>
          <div class="row">
            <div class="col-md-4">
              <label >Cantidad</label>
              <input type="number" class="form-control" name=cantidad placeholder="CANTIDAD"
              @if(isset($productosucursal))
              value="{{$productosucursal->cantidad}}"
              @else
              required
              @endif >
            </div>
            <div class="col-md-4">
              <label>INGRESO</label>
              <select class="form-control" name="ingreso" @if(!isset($productosucursal)) required
              @endif >
                <option value="">SELECIONE UNA</option>

                <option value="nuevo"
                @if(isset($productosucursal))
                  @if($productosucursal->ingreso == "nuevo")
                  selected=selected
                  @endif
                @endif

                >NUEVO</option>
                <option value="usado"
                @if(isset($productosucursal))
                  @if($productosucursal->ingreso == "usado")
                  selected=selected
                  @endif
                @endif

                >USADO</option>
              </select>
            </div>
            <div class="col-md-4">
              <label>IMEIS</label>
              <textarea class="form-control" name="imeis">@if(isset($productosucursal)){{$productosucursal->imeis}}@endif</textarea>
            </div>
          </div>
          <br clear=all>
            <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">MONEDA</label>
                <select class="form-control" name="idmoneda"
                @if(!isset($productosucursal))
                 required
                @endif
                 >
                  <option value="">SELECIONE UNA</option>
                  @foreach($monedas as $c)
                  <option value="{{$c->id}}"
                    @if(isset($productosucursal))
                      @if($productosucursal->idmoneda == $c->id)
                      selected="selected"
                      @endif
                    @endif
                    >{{$c->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">PRECIO</label>
                <input type="text" class="form-control" name=precio placeholder="Precio"
                @if(isset($productosucursal))
                value="{{$productosucursal->precio}}"
                @else
                required
                @endif
                >
              </div>
            </div>
          </div>

          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
          @if(isset($productosucursal))
          ACTUALIZAR
          @else
          REGISTRAR
          @endif
        </button>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script>

$('#idcosto').change(function(e){
  e.preventDefault()
  if($(this).val()== "VARIABLE"){
    $('#variable').css('display','block');
    $('#fijo').css('display','none');
  }else{
    $('#fijo').css('display','block');
    $('#variable').css('display','none');
    $('#listvariable').empty()
  }
})

$('#agregarcosto').click(function(e){
  e.preventDefault()
  html = '<tr>'
  html += '<td>'
  html += $('#concepto').val()
  html += '</td>'
  html += '<td>'
  html += $('#recargo').val()
  html += '<input type=hidden name=recargo['+$('#concepto').val()+'] value="'+$('#recargo').val()+'">'
  html += '</td>'
  html += '<td style="width:10%;">'
  html += '<a href="#" id="borrargasto" ><i class="fas fa-trash"></i></a>'
  html+= '</td>'
  html += '</tr>'
  $('#recargo').empty()
  $('#concepto').empty()

  $('#listvariable').append(html)

  $('#borrargasto').click(function(er){
    er.preventDefault()
    $(this).parent().parent().remove()
  })
})

$('.borrargastoajax').click(function(e){
  e.preventDefault()
  borrar = $(this).parent().parent()
  $.get('/mercaderia/borrar/ajax/'+$(this).data('id')+'/'+$(this).data('borrar'), function(re){
    borrar.remove()
  })
})


$('#categoria').change(function(e){
  e.preventDefault()
  id = $(this).val()
  url = 'categoria/'+id
  $.get(url,function(res){
    html=''
    $('#marca').removeAttr('disabled');
    $('#marca').empty()
    $('#producto').empty()
    html +='<option value="">SELECIONE UNA</option>'
    for(i=0;i<res.length;i++){
      html+='<option value='+res[i].id+'  >'+res[i].nombre+'</option>'
    }
    $('#marca').append(html)
    $('#marca').change(function(event){
      event.preventDefault()
      id = $(this).val()
      url = 'producto/'+id
      $.get(url,function(respuesta){
        console.log(respuesta)
        html2 = ''
        $('#producto').removeAttr('disabled')
        $('#producto').empty()
        html2 +='<option value="">SELECIONE UNA</option>'
        for(i=0;i<respuesta.length;i++){
          html2+='<option value='+respuesta[i].id+'  >'+respuesta[i].modelo+'</option>'
        }
        $('#producto').append(html2)
      })
    })
  })

})

</script>
@endsection
