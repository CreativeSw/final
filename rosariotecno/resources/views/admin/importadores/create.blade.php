@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Importador</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Razón Social</label>
                <input type="text" class="form-control" name="razonsocial" placeholder="RAZON SOCIAL"
                @if(isset($importador))
                value="{{$importador->razonsocial}}"
                @else
                required
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Responsable</label>
                <input type="text" class="form-control" name="responsable" placeholder="RESPONSABLE"
                @if(isset($importador))
                value="{{$importador->responsable}}"
                @endif
                >
              </div>
            </div>
          </div>
          <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <label >Email</label>
                <input type="email" class="form-control" name="email"  placeholder="EMAIL"
                @if(isset($importador))
                value="{{$importador->email}}"
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >Telefono</label>
                <input type="text" class="form-control" name="telefono" placeholder="TELEFONO"
                @if(isset($importador))
                value="{{$importador->telefono}}"
                @endif
                >
              </div>
            </div>
          </div>
          <div class="row">

            <div class="col-md-12">
              <div class="form-group">
                <label >Direccion</label>
                <input type="text" class="form-control" name="direccion" placeholder="DIRECCION"
                @if(isset($importador))
                value="{{$importador->direccion}}"
                @endif
                >
              </div>
            </div>
          </div>
          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
            @if(isset($importador))
            ACTUALIZAR
            @else
            CREAR
            @endif
          </button>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
