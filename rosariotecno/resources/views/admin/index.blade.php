@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3>Flujo de Cajas</h3>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="{{url('finanzas/cajas')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>Estable</h3>
            <p>Stock General</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="{{url('stock')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-warning">
          <div class="inner">
            <h3>{{$clientes}}</h3>
            <p>Clientes</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="{{'clientes'}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-danger">
          <div class="inner">
            <h5>GASTOS</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="{{url('admin/gastos')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h5>MONEDAS</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-money-bill-wave"></i>
          </div>
          <a href="{{url('contabilidad/monedas')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background: #b43bec !important;">
          <div class="inner">
            <h5>INGRESO MERCADERIA</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-dolly"></i>
          </div>
          <a href="{{url('mercaderia/ingreso')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background:#f71fac !important;">
          <div class="inner">
            <h5>GARANTIA</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-clipboard-list"></i>
          </div>
          <a href="{{url('garantia')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-6">
        <div class="small-box bg-warning" style="background: #c0c1c0 !important;">
          <div class="inner">
            <h5>CONFIGURACION</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-cogs"></i>
          </div>
          <a href="{{url('admin/configuracion')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-danger">
          <div class="inner">
            <h5>SALIR</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-window-close"></i>
          </div>
          <a href="{{url('salir')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
