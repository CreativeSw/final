@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Producto</h3>
      </div>
      <form method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label >Marca</label>
                <select name="idmarca" class="form-control" required>
                  <option value="">SELECIONE UNA</option>
                  @foreach($marcas as $i)
                  <option value="{{$i->id}}"
                    @if(isset($producto))
                    @if($producto->idmarca == $i->id)
                    selected="selected"
                    @endif
                    @endif
                    >{{$i->descripcion}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >Modelo</label>
                <input type="text" class="form-control" placeholder="Modelo" name="modelo"
                @if(isset($producto))
                value="{{$producto->modelo}}"
                @else
                required
                @endif
                 >
              </div>
            </div>
          </div>
          <div class="form-group">
              <label>Descripción</label>
              <textarea class="form-control" rows="3" placeholder="Descripción" name="descripcion"
              @if(!isset($producto))
              required
              @endif
              >@if(isset($producto)){{$producto->descripcion}} @endif</textarea>
          </div>
          <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputFile">Foto Frontal</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" name="imagenweb"
                  @if(!isset($producto))
                  required
                  @endif
                  >
                  <label class="custom-file-label" >Selecione Imagen</label>
                </div>
                <div class="input-group-append">
                  <span class="input-group-text">Upload</span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label>Fotos</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="imagen[]" multiple
                    @if(!isset($producto))
                    required
                    @endif
                    >
                    <label class="custom-file-label" >Selecione Imagen</label>
                  </div>
                  <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                  </div>
                </div>
            </div>
          </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label>Stock minimo</label>
                  <input type="number" class="form-control" placeholder="Stock minimo" name="stockminimo"
                  @if(isset($producto))
                  value="{{$producto->stockminimo}}"
                  @else
                  required
                  @endif
                  >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >Garantia</label>
                <select name="grantia" class="form-control" @if(!isset($producto)) required @endif>
                  <option value="">SELECIONE UNA</option>
                  <option value="3"
                  @if(isset($producto))
                    @if($producto->garantia == 0)
                      selected="selected"
                    @endif
                  @endif
                  >Sin Garantia</option>
                  <option value="3"
                  @if(isset($producto))
                    @if($producto->garantia == 6)
                      selected="selected"
                    @endif
                  @endif
                  >3 meses</option>
                  <option value="6"
                  @if(isset($producto))
                    @if($producto->garantia == 6)
                      selected="selected"
                    @endif
                  @endif
                  >6 meses</option>
                  <option value="12"
                  @if(isset($producto))
                    @if($producto->garantia == 12)
                      selected="selected"
                    @endif
                  @endif

                  >1 año</option>
                </select>

              </div>
            </div>
          </div>
          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
          @if(isset($producto))
          ACTUALIZAR
          @else
          CREAR
          @endif
        </button>
        </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
