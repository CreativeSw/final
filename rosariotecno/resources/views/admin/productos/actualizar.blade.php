@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Actualizador de Precios</h3>
      </div>
      <form>
        <div class="card-body">

            <div class="form-group">
              <label for="exampleInputFile">Archivo Excel</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="exampleInputFile">
                  <label class="custom-file-label" for="exampleInputFile">Selecione Archivo</label>
                </div>
                <div class="input-group-append">
                  <span class="input-group-text">Upload</span>
                </div>
              </div>
            </div>

          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">ACTUALIZAR</button>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
