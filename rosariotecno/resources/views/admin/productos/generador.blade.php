  @extends('layouts.admin')
@section('content')
<script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.0/dist/JsBarcode.all.min.js"></script>
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">GENERADOR</h3>
      </div>
      <form method="post" >
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label >MARCA</label>
                <select name="idmarca" class="form-control" required id="marca">
                  <option value="">SELECIONE UNA</option>
                  @foreach($marcas as $i)
                  <option value="{{$i->id}}"
                    @if(isset($producto))
                    @if($producto->idmarca == $i->id)
                    selected="selected"
                    @endif
                    @endif
                    >{{$i->descripcion}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label >PRODUCTO</label>
                <select name="producto" class="form-control" required id="producto" disabled required>
                  <option value="">SELECIONE UNA</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label >CANTIDAD</label>
                <input type="number" name="cantidad" placeholder="Cantidad" required class="form-control" id="cantidad">

              </div>
            </div>
          </div>
          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;" >
          GENERAR CODIGO
        </button>
        </div>
        </div>

      </form>
    </div>
    <br clear=all>
    <div class="col-md-12" id="barra">

    </div>
  </div>
</div>
<script>
$('#marca').change(function(i){
  i.preventDefault();
  if($(this).val() != ""){
    $.get('/productos/codigo/marca/'+$(this).val(),function(res){
      $('#producto').empty()
      $('#producto').prop('disabled',false);
      for(var i=0;i< res.length;i++){
        html ='<option value="'+res[i].id+'">'+res[i].modelo+'</option>'
        $('#producto').append(html)
      }
    })
  }
})

</script>
@endsection
