@extends('layouts.admin')
@section('content')
@if(Session::get('mensaje'))
<br clear=all>
<div class="alert alert-success">
  {{ Session::get('mensaje') }}
</div>
@endif
<div class="content-header">
  <div class="container-fluid">



    <div class="col-2" style="float: right;">
      <a href="{{url('admin/gastos/nuevo')}}" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">NUEVO</a>
    </div>
  
    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
              <th>CONCEPTO</th>
              <th>IMPORTE</th>
              <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($clientes as $c)
           <tr>
               <td>{{$c->concepto}}</td>
               <td>{{$c->costo}}</td>
               <td>
                 <a href="{{url('admin/gastos/editar',[$c->id])}}"><i class="fa fa-pen" aria-hidden="true"></i></a>
                 <a href="" data-id="{{$c->id}}" class="borrarcategoria"><i class="fas fa-trash"></i></a>
               </td>
           </tr>
           @endforeach

        </tbody>
        <tfoot>
            <tr>
              <th>NOMBRE Y APELLIDO</th>
              <th>CONTACTO</th>
              <th>ACCIONES</th>
            </tr>
        </tfoot>
    </table>
        </div>
      </div>
    </div>
  </div>

  <script>
  $(document).ready(function() {
      $('#example').DataTable({
        "bInfo" : false
      });
  } );


  $('.borrarcategoria').click(function(event){
    event.preventDefault();
    id = $(this).data('id');
    borrar = $(this).parent().parent()
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: 'Borrar',
      text: "Esta seguro que desea eliminar este gasto",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        url = '/admin/gastos/borrar/'+id
        $.get(url, function(res){
          swalWithBootstrapButtons.fire(
            'Borrado!',
            res.success,
            'success'
          )
          borrar.remove()
        })

      } else if ( result.dismiss === Swal.DismissReason.cancel) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'Has cancelado la eliminación del cliente',
          'error'
        )
      }
    })
  })
  </script>
@endsection
