@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Gastos</h3>
      </div>
      <form method="post">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>CONCEPTO</label>
                <input type="text" class="form-control" placeholder="CONCEPTO" name="concepto"
                @if(isset($cliente))
                value="{{$cliente->concepto}}"
                @else
                required
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>COSTO</label>
                <input type="number" class="form-control" name="costo" placeholder="COSTO"
                @if(isset($cliente))
                value="{{$cliente->costo}}"
                @else
                required
                @endif
                step=0.001
                >
              </div>
            </div>
          </div>

          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
          @if(isset($cliente))
          ACTUALIZAR
          @else
          CREAR
          @endif
        </button>
        </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
