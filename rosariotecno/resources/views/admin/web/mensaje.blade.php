@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">MENSAJE</h3>
      </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Nombre</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="{{$mensaje->nombre}}" readonly>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="{{$mensaje->email}}" readonly>
              </div>
            </div>
          </div>

            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Mensaje</label>
                <textarea class="form-control"  readonly>{{$mensaje->mensaje}}</textarea>

              </div>
            </div>


          <div class="col-md-2" style="float:right;">
            <a href="{{url('admin/mensajes')}}" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">ATRAS</a>
          </div>
        </div>


    </div>
  </div>
</div>
@endsection
