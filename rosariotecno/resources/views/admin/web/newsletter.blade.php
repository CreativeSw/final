@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">


    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>FECHA</th>
                <th>EMAIL</th>

              </tr>
            </thead>
            <tbody>
            @foreach($newsletters as $v)
            <tr>
              <td style="width:10%;">{{$v->created_at->format('d/m/Y')}}</td>
              <td>
                {{$v->email}}
              </td>

            </tr>
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>FECHA</th>
                <th>EMAIL</th>

            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({
       responsive: true
    });
} );
</script>
@endsection
