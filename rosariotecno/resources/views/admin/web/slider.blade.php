@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">Slider</h3>
      </div>
      <form method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card-body">
          <div class="row">

            <div class="col-md-12">
              <div class="form-group">
                <label >CATEGORIA</label>
                <input type="text" class="form-control" placeholder="Categoria" name="categoria"
                @if(isset($slider))
                value="{{$slider->categoria}}"
                @else
                required
                @endif
                 >
              </div>
            </div>
          </div>
          <div class="col-md-12">
          <div class="form-group">
              <label>Descripción</label>
              <textarea class="form-control" rows="3" placeholder="Texto" name="texto"
              @if(!isset($slider))
              required
              @endif
              >@if(isset($slider)){{$slider->texto}} @endif</textarea>
          </div>
        </div>

            </div>
          </div>
          <div class="col-md-4" style="float:right;">
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
          @if(isset($producto))
          ACTUALIZAR
          @else
          CREAR
          @endif
        </button>
        </div>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection
