@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">OFERTA</h3>
      </div>
        <div class="card-body">
          <form method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Producto</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="{{$descuento->producto}}" name="producto">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">DESCUENTO</label>
                <input type="number" class="form-control"  value="{{$descuento->descuento}}" name="descuento">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">VALIDES EN DIAS</label>
                <input type="number" class="form-control"  value="{{$descuento->valides}}" name="valides">
              </div>
            </div>

          <div class="col-md-6">
            <div class="form-group">
                <label>Fotos</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="imagen"
                    @if(!isset($descuento))
                    required
                    @endif
                    >
                    <label class="custom-file-label" >Selecione Imagen</label>
                  </div>
                  <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                  </div>
                </div>
            </div>
          </div>
        </div>
          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
          @if(isset($producto))
          ACTUALIZAR
          @else
          CREAR
          @endif
        </button>
      </form>
        </div>


    </div>
  </div>
</div>
@endsection
