@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="col-2" style="float: right;">
      <a href="{{url('admin/sliders/nuevo')}}" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">NUEVO</a>
    </div>

    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>FECHA</th>
                <th>TEXTO</th>
                <th>CATEGORIA</th>
                <th>ACCIONES</th>
              </tr>
            </thead>
            <tbody>
            @foreach($sliders as $v)
            <tr>
              <td style="width:10%;">{{$v->created_at->format('d/m/Y')}}</td>
              <td>
                {{$v->texto}}
              </td>
              <td>{{$v->categoria}}</td>
              <td>
                <a href="{{url('/admin/sliders/editar',[$v->id])}}"><i class="fa fa-pen" aria-hidden="true"></i></a>
                <a href="{{url('/admin/sliders/borrar',[$v->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
              </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>FECHA</th>
                <th>MENSAJE</th>
                <th>ACCIONES</th>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({
       responsive: true
    });
} );
</script>
@endsection
