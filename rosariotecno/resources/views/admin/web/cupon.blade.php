@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">

    <div class="card card-primary">
      <div class="card-header" style="background:#ff5700">
        <h3 class="card-title">OFERTA</h3>
      </div>
        <div class="card-body">
          <form method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">CODIGO</label>
                <input type="text" class="form-control" name="codigo"
                @if(isset($cupon))
                value="{{$cupon->codigo}}"
                @else
                required
                @endif
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">CANTIDAD DE USOS</label>
                <input type="number" class="form-control" name="usos"
                @if(isset($cupon))
                  value="{{$cupon->usos}}"
                @else
                  required
                @endif
                >
              </div>
            </div>
          </div>

          <button type="submit" class="btn btn-primary" style="background:#ff5700;border:none;float:right;">
          @if(isset($cupon))
          ACTUALIZAR
          @else
          CREAR
          @endif
        </button>
      </form>
        </div>


    </div>
  </div>
</div>
@endsection
