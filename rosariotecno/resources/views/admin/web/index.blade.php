@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-6">
        <div class="small-box bg-danger">
          <div class="inner">
            <h5>NEWSLETTER</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="{{url('admin/newsletter')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h5>MENSAJES</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-money-bill-wave"></i>
          </div>
          <a href="{{url('admin/mensajes')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background: #b43bec !important;">
          <div class="inner">
            <h5>SLIDER TEXTO</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-dolly"></i>
          </div>
          <a href="{{url('admin/sliders')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background:#f71fac !important;">
          <div class="inner">
            <h5>DESCUENTO</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-clipboard-list"></i>
          </div>
          <a href="{{url('admin/descuentos')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-6">
        <div class="small-box bg-warning" style="background: #c0c1c0 !important;">
          <div class="inner">
            <h5>CUPONES</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fa fa-cogs"></i>
          </div>
          <a href="{{url('admin/cupones')}}" class="small-box-footer">Ver Mas<i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success" style="background: #ffb725 !important;">
          <div class="inner">
            <h5>USUARIOS REGISTRADOS</h5>
            <br clear=all>
            <br clear=all>
          </div>
          <div class="icon">
            <i class="fas fa-user-circle"></i>
          </div>
          <a href="{{url('admin/usuarios')}}" class="small-box-footer">Ver Mas <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
