@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>DATOS</th>

              </tr>
            </thead>
            <tbody>
            @foreach($usuarios as $v)
            <tr>
              <td style="">
                <span style="font-weight: bold;">NOMBRE:</span>{{$v->nombre}}<br>
                <span style="font-weight: bold;">EMAIL:</span>{{$v->email}}<br>
                <span style="font-weight: bold;">DIRECCION:</span>{{$v->direccion}}<br>
                <span style="font-weight: bold;">TELEFONO:</span>{{$v->telefono}}<br>
              </td>

            </tr>
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>DATOS</th>

            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({
       responsive: true
    });
} );
</script>
@endsection
