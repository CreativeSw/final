@extends('layouts.admin')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="col-2" style="float: right;">
      <a href="{{url('admin/cupones/nuevo')}}" class="btn btn-primary btn-block" style="background:#ff5700;border:none;">NUEVO</a>
    </div>

    <br clear=all>
    <br clear=all>
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>FECHA</th>
                <th>MENSAJE</th>
                <th>ACCIONES</th>
              </tr>
            </thead>
            <tbody>
            @foreach($codigos as $v)
            <tr>
              <td style="width:10%;">{{$v}}</td>

            </tr>
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>FECHA</th>
                <th>MENSAJE</th>
                <th>ACCIONES</th>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({
       responsive: true
    });
} );
</script>
@endsection
