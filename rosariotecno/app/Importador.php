<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Importador extends Model
{
    protected $fillable = [
      'razonsocial',
      'responsable',
      'direccion',
      'email',
      'telefono'
    ];
}
