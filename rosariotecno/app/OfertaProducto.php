<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfertaProducto extends Model
{
    //
    protected $fillable = [
      'producto',
      'descuento',
      'imagen',
      'valides'
    ];
}
