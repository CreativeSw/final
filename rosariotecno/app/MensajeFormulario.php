<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MensajeFormulario extends Model
{
    protected $fillable = [
      'nombre',
      'email',
      'mensaje'
    ];
}
