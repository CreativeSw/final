<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextoSlider extends Model
{
    //
    protected $fillable = [
      'texto',
      'categoria'
    ];
}
