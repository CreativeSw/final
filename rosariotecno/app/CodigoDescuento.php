<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodigoDescuento extends Model
{
    //
    protected $fillable = [
      'codigo',
      'estado',
      'usos'
    ];
}
