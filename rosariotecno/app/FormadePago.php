<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormadePago extends Model
{
    protected $fillable = [
      'nombre',
      'recargo',
      'idcategoria',
      'grupoderecargo',
      
    ];
}
