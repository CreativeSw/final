<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticuloVenta extends Model
{
    protected $fillable = [
      'articulo',
      'producto',
      'cantidad',
      'precio',
      'imei',
      'venta'
    ];
}
