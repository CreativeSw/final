<?php





Route::get('login',function(){
  return redirect()->to('/');
});
/*
Route::get('/',function(){
  return redirect()->to('admin');
});
*/
Route::get('/','SiteController@index');
Route::post('newsletter','SiteController@newsletter');
Route::get('loginweb','LoginController@index');
Route::post('loginweb','LoginController@login');
Route::get('contacto','SiteController@contacto');
Route::post('contacto','SiteController@enviarmsj');
Route::get('registrar','SiteController@indexregister');
Route::post('registrar','SiteController@register');
Route::get('smartphone','SiteController@smartphone');
Route::get('smartwatch','SiteController@smartwatch');
Route::get('accessorios','SiteController@accessorios');
Route::get('notebooks','SiteController@notebooks');
Route::get('producto/{id}','SiteController@producto');
Route::get('favoritos','SiteController@favoritos');
Route::get('favoritos/agregar/{id}','SiteController@addfavorito');
Route::get('favoritos/borrar/{id}','SiteController@deletefavorito');

Route::get('carrito','SiteController@carrito');
Route::get('carrito/agregar/{id}','SiteController@addcarrito');
Route::get('carrito/agregar/{id}/{cantidad}','SiteController@addcarritocoutome');
Route::get('carrito/finalizar','SiteController@finishcarrito');
Route::get('perfil','SiteController@perfil');
Route::post('perfil','SiteController@saveperfil');
Route::get('admin','LoginController@index');
Route::post('admin','LoginController@login');

//Route::get('/', 'LoginController@index');
//Route::post('/', 'LoginController@login');

Route::group(['middleware'=>['admin']],function(){
  Route::get('ppal', 'LoginController@ppal');
  Route::get('usuarios','UsuarioController@index');
  Route::get('usuarios/nuevo','UsuarioController@viewcreate');
  Route::post('usuarios/nuevo','UsuarioController@create');
  Route::get('usuarios/editar/{id}','UsuarioController@edit');
  Route::post('usuarios/editar/{id}','UsuarioController@save');
  Route::get('usuarios/borrar/{id}','UsuarioController@delete');
  Route::get('subanel',function(){
    return view('admin.clientes.subpanel');
  });
  Route::get('clientes', 'ClienteController@index');
  Route::post('clientes', 'ClienteController@importar');
  Route::get('clientes/exportar', 'ClienteController@exportar');
  Route::get('clientes/nuevo', 'ClienteController@indexcreate');
  Route::post('clientes/nuevo', 'ClienteController@create');
  Route::get('clientes/editar/{id}','ClienteController@edit');
  Route::post('clientes/editar/{id}','ClienteController@save');
  Route::get('clientes/borrar/{id}','ClienteController@delete');
  Route::get('empleados', 'ClienteController@indexempleado');
  Route::post('empleados', 'ClienteController@importarempleado');
  Route::get('empleados/exportar', 'ClienteController@exportarempleado');
  Route::get('empleados/nuevo', 'ClienteController@indexcreateempleado');
  Route::post('empleados/nuevo', 'ClienteController@createempleado');
  Route::get('empleados/editar/{id}','ClienteController@editempleado');
  Route::post('empleados/editar/{id}','ClienteController@saveempleado');
  Route::get('empleados/borrar/{id}','ClienteController@deleteempleado');
  Route::get('categorias', 'CategoriaController@index');
  Route::post('categorias', 'CategoriaController@importar');
  Route::get('categorias/nuevo', 'CategoriaController@indexcreate');
  Route::post('categorias/nuevo', 'CategoriaController@create');
  Route::get('categorias/editar/{id}','CategoriaController@edit');
  Route::post('categorias/editar/{id}','CategoriaController@save');
  Route::get('categorias/borrar/{id}','CategoriaController@delete');
  Route::get('marcas','MarcaController@index');
  Route::post('marcas','MarcaController@importar');
  Route::get('marcas/nuevo','MarcaController@indexcreate');
  Route::post('marcas/nuevo','MarcaController@create');
  Route::get('marcas/editar/{id}','MarcaController@edit');
  Route::post('marcas/editar/{id}','MarcaController@save');
  Route::get('marcas/borrar/{id}','MarcaController@delete');
  Route::get('importadores', 'ImportadorController@index');
  Route::post('importadores','ImportadorController@importar');
  Route::get('importadores/nuevo','ImportadorController@indexcreate');
  Route::post('importadores/nuevo','ImportadorController@create');
  Route::get('importadores/editar/{id}','ImportadorController@edit');
  Route::post('importadores/editar/{id}','ImportadorController@save');
  Route::get('importadores/borrar/{id}','ImportadorController@delete');
  Route::get('paneldepagos',function(){
    return view('admin.formasdepagos.panel');
  });
  Route::get('contabilidad/monedas','MonedaController@index');
  Route::post('contabilidad/monedas','MonedaController@importar');
  Route::get('contabilidad/monedas/nueva','MonedaController@indexcreate');
  Route::post('contabilidad/monedas/nueva','MonedaController@create');
  Route::get('contabilidad/monedas/editar/{id}','MonedaController@edit');
  Route::post('contabilidad/monedas/editar/{id}','MonedaController@save');
  Route::get('contabilidad/monedas/borrar/{id}','MonedaController@delete');
  Route::get('formadepagos', 'FormadePagoController@index');
  Route::post('formadepagos','FormadePagoController@importador');
  Route::get('formadepagos/nuevo', 'FormadePagoController@indexcreate');
  Route::post('formadepagos/nuevo', 'FormadePagoController@create');
  Route::get('formadepagos/editar/{id}','FormadePagoController@edit');
  Route::post('formadepagos/editar/{id}','FormadePagoController@save');
  Route::get('formadepagos/borrar/{id}','FormadePagoController@delete');
  Route::get('productos','ProductoController@index');
  Route::post('productos','ProductoController@importar');
  Route::get('productos/nuevo','ProductoController@indexcreate');
  Route::post('productos/nuevo','ProductoController@create');
  Route::get('productos/editar/{id}','ProductoController@edit');
  Route::post('productos/editar/{id}','ProductoController@save');
  Route::get('productos/borrar/{id}','ProductoController@delete');

  Route::get('productos/codigo','ProductoController@indexcodigos');
  Route::post('productos/codigo','ProductoController@generarcodigo');
  Route::get('productos/codigo/marca/{id}','ProductoController@getproducto');

  Route::get('preciosupdate','ProductoController@indexprecios');
  Route::post('preciosupdate','ProductoController@updateprecios');

  Route::post('actualizarweb','ProductoController@updatepreciosweb');


  Route::get('mercaderia', 'MercaderiaController@panel');
  Route::get('mercaderia/ingreso','MercaderiaController@index');
  Route::get('mercaderia/ingreso/nuevo', 'MercaderiaController@indexcreate');
  Route::post('mercaderia/ingreso/nuevo', 'MercaderiaController@create');
  Route::get('mercaderia/ingreso/borrar/{id}','MercaderiaController@delete');
  Route::get('mercaderia/ingreso/editar/{id}','MercaderiaController@edit');
  Route::post('mercaderia/ingreso/editar/{id}','MercaderiaController@save');
  Route::get('mercaderia/borrar/ajax/{id}/{borrar}','MercaderiaController@deleteajax');
  Route::get('mercaderia/ingreso/editar/categoria/{id}','MercaderiaController@getmarca');
  Route::get('mercaderia/ingreso/editar/producto/{id}','MercaderiaController@getproducto');
  Route::get('mercaderia/ingreso/categoria/{id}','MercaderiaController@getmarca');
  Route::get('mercaderia/ingreso/producto/{id}','MercaderiaController@getproducto');
  Route::get('stock', 'StockController@indexadmin');
  Route::get('stock/{id}','StockController@stocklocal');

  Route::get('admin/stock/informes/{id}','StockController@informessucursal');
  Route::get('admin/stock/informesdiario/{id}','StockController@informediario');




  Route::get('admin/stock/informegeneral','StockController@informegeneral');
  Route::get('admin/ventas','VentasController@indexadmin');
  Route::get('admin/sucursal/{id}','VentasController@sucursal');
  Route::get('garantia','VentasController@garantiaadmin');
  Route::get('garantia/{imei}/{producto}','VentasController@getgarantia');
  Route::get('admin/contabilidad', 'FinanzasController@index');
  Route::post('admin/contabilidad', 'FinanzasController@index_custome');
  Route::get('informes/categoria/{id}','FinanzasController@indexcategoria');
  Route::get('informes/marcas/{id}','FinanzasController@indexmarcas');

  Route::get('admin/gastos','GastoController@index');
  Route::get('admin/gastos/nuevo','GastoController@viewcreate');
  Route::post('admin/gastos/nuevo','GastoController@create');
  Route::get('admin/gastos/editar/{id}','GastoController@edit');
  Route::post('admin/gastos/editar/{id}','GastoController@save');
  Route::get('admin/gastos/borrar/{id}','GastoController@delete');
  Route::get('finanzas/cajas','FinanzasController@indexcajas');
  Route::get('finanzas/cajas/{id}','FinanzasController@flujocaja');
  Route::post('finanzas/cajas/{id}','FinanzasController@flujocajacoutome');
  Route::get('informes/categoria/{sucursal}/{id}','FinanzasController@categoriasucursal');
  Route::get('informes/marcas/{sucursal}/{id}','FinanzasController@indexmarcassucursal');;


  Route::get('admin/configuracion','UsuarioController@adminconfiguracion');
  Route::post('admin/configuracion','UsuarioController@adminconfiguracionsave');
  Route::get('salir','LoginController@salir');
  Route::get('admin/web','WebController@index');
  Route::get('admin/newsletter','WebController@newsletter');
  Route::get('admin/mensajes','WebController@mensajes');
  Route::get('admin/mensajes/ver/{id}','WebController@vermensaje');
  Route::get('admin/sliders','WebController@indexslider');
  Route::get('admin/sliders/nuevo','WebController@slidernuevo');
  Route::post('admin/sliders/nuevo','WebController@slider');
  Route::get('admin/sliders/editar/{id}','WebController@edit');

  Route::post('admin/sliders/editar/{id}','WebController@save');
  Route::get('admin/sliders/borrar/{id}','WebController@delete');
  Route::get('admin/descuentos','WebController@indexdescuento');
  Route::post('admin/descuentos','WebController@descuento');
  Route::get('admin/cupones','WebController@indexcupones');
  Route::get('admin/cupones/nuevo','WebController@cupon');
  Route::post('admin/cupones/nuevo','WebController@createcupon');

  Route::get('admin/usuarios','WebController@indexusuarios');


});


//inicio sucursal
Route::group(['middleware'=>['sucursal']],function(){
  Route::get('sucursal','LoginController@sucursal');
  Route::get('sucursal/clientes', 'ClienteController@indexsucursal');
  Route::get('sucursal/clientes/nuevo', 'ClienteController@indexcreate');
  Route::post('sucursal/clientes/nuevo', 'ClienteController@create');
  Route::get('sucursal/clientes/editar/{id}','ClienteController@edit');
  Route::post('sucursal/clientes/editar/{id}','ClienteController@save');
  Route::get('sucursal/monedas','MonedaController@index');
  Route::get('sucursal/monedas/editar/{id}','MonedaController@edit');
  Route::post('sucursal/monedas/editar/{id}','MonedaController@save');
  Route::get('sucursal/stock','StockController@sucursal');
  Route::get('sucursal/stock/informes','StockController@informes');

  Route::get('sucursal/stockral','StockController@stockgral');
  Route::get('sucursal/stockgral','StockController@gralsucursal');

  Route::get('sucursal/mercaderia','MercaderiaController@indexsucursal');
  Route::get('sucursal/mercaderia/nuevo','MercaderiaController@ingresosucursal');
  Route::post('sucursal/mercaderia/nuevo','MercaderiaController@createsucursal');
  Route::get('sucursal/mercaderia/borrar/{id}','MercaderiaController@deletesucursal');
  Route::get('sucursal/mercaderia/editar/{id}','MercaderiaController@editsucursal');
  Route::post('sucursal/mercaderia/editar/{id}','MercaderiaController@savesucursal');
  Route::get('sucursal/mercaderia/categoria/{id}','MercaderiaController@getmarca');
  Route::get('sucursal/mercaderia/producto/{id}','MercaderiaController@getproducto');
  Route::get('sucursal/movermercaderia','MercaderiaController@indexsucursalmover');
  Route::get('sucursal/movermercaderia/nuevo','MercaderiaController@sucursalmover');
  Route::post('sucursal/movermercaderia/nuevo','MercaderiaController@sucursalmovercrear');
  Route::get('sucursal/movermercaderia/ver/{id}','MercaderiaController@editsucursalmover');
  Route::get('sucursal/movermercaderia/borrar/{id}','MercaderiaController@deletemoverbosucursal');
  Route::get('sucursal/configuracion','UsuarioController@sucursalconfiguracion');
  Route::post('sucursal/configuracion','UsuarioController@sucursalsaveConfiguracion');
  Route::get('sucursal/ventas','VentasController@indexsucursal');
  Route::get('sucursal/ventas/nueva','VentasController@indexcreatesucursal');
  Route::post('sucursal/ventas/nueva','VentasController@createsucursal');

  Route::get('sucursal/ventas/producto/{imei}','VentasController@getproductoimei');

  Route::get('sucursal/ventas/borrar/{id}','VentasController@deletesucursal');
  Route::get('sucursal/ventas/editar/{id}','VentasController@editarventasucursal');
  Route::post('sucursal/ventas/editar/{id}','VentasController@saveventasucursal');
  Route::get('sucursal/ventas/ver/{id}','VentasController@view');

  Route::get('sucursal/ventas/cerrarcaja','VentasController@cerrarcajasucursal');
  Route::get('sucursal/ventas/cerrarcaja/ver/{id}','VentasController@cerrarcajasucursalver');
  Route::get('sucursal/ventas/editar/borrar/{id}','VentasController@deleteproducto');
  Route::get('sucursal/ventas/producto/{forma}/{id}','VentasController@getproducto');
  Route::get('sucursal/ventas/editar/producto/{forma}/{id}','VentasController@getproducto');

  Route::get('sucursal/cerrarcaja','VentasController@cerrarcaja');

  Route::get('sucursal/ventasmayorista','VentasController@indexsucursalmayorista');
  Route::get('sucursal/ventasmayorista/nueva','VentasController@indexcreatesucursalmayorista');
  Route::post('sucursal/ventasmayorista/nueva','VentasController@createsucursalmayorista');

  Route::get('sucursal/ventasmayorista/editar/{id}','VentasController@editmayorista');
  Route::post('sucursal/ventasmayorista/editar/{id}','VentasController@savemayorista');


  Route::get('sucursal/ventasmayorista/producto/{forma}/{id}','VentasController@getproducto');


//  Route::get('sucursal/ventasmayorista','VentasController@')


  Route::get('sucursal/garantia','VentasController@garantia');
  Route::get('sucursal/garantia/{imei}/{producto}','VentasController@getgarantia');
  Route::get('sucursal/preciosupdate','ProductoController@indexsucursalupdate');
  Route::post('sucursal/preciosupdate','ProductoController@sucursalupdate');
  Route::get('salir','LoginController@salir');
});
//end sucursal
//inicio deposito
Route::group(['middleware' => ['deposito']], function () {
  Route::get('deposito','UsuarioController@usuariodeposito');
  Route::get('deposito/configuracion','UsuarioController@configuracion');
  Route::post('deposito/configuracion','UsuarioController@saveConfiguracion');
  Route::get('deposito/mercaderia','MercaderiaController@indexdeposito');
  Route::get('deposito/mercaderia/nuevo','MercaderiaController@ingresodeposito');
  Route::post('deposito/mercaderia/nuevo','MercaderiaController@createdeposito');
  Route::get('deposito/mercaderia/editar/{id}','MercaderiaController@editdeposito');
  Route::post('deposito/mercaderia/editar/{id}','MercaderiaController@savedeposito');
  Route::get('deposito/mercaderia/borrar/{id}','MercaderiaController@deletedeposito');
  Route::get('deposito/mercaderia/categoria/{id}','MercaderiaController@getmarca');
  Route::get('deposito/mercaderia/producto/{id}','MercaderiaController@getproducto');
  Route::get('deposito/movermercaderia/nuevo','MercaderiaController@depositomover');
  Route::post('deposito/movermercaderia/nuevo','MercaderiaController@depositomovercrear');
  Route::get('deposito/movermercaderia/editar/{id}','MercaderiaController@editdepositomover');
  Route::get('deposito/movermercaderia/borrar/{id}','MercaderiaController@deletemoverborrar');
  Route::get('deposito/movermercaderia','MercaderiaController@indexdepositomover');
  Route::get('deposito/stock','StockController@depostio');
  Route::get('deposito/stockgral','StockController@graldeposito');
  //informegeneral
  Route::get('salir','LoginController@salir');
});
Route::get('salir','LoginController@salir');




/*

*/
