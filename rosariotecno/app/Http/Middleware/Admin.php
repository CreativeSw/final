<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Admin
{

    public function handle($request, Closure $next)
    {
      if(!isset(Auth::user()->rol))
        return redirect()->to('salir');

      if(Auth::user()->rol=="admin"){
        return $next($request);
      }
      return redirect()->to('salir');
    }
}
