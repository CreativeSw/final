<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Importador;
use App\Marca;
use App\Categoria;
use App\Producto;
use App\ProductoSucursal;
use App\FormadePago;
use App\ProductoFormaPago;
use App\Moneda;
use Session;
use Excel;
use Picqer\Barcode\BarcodeGeneratorJPG;
use Picqer\Barcode\BarcodeGeneratorHTML;

class ProductoController extends Controller
{
    public function index(){
      $productos = Producto::all();
      foreach ($productos as $p) {
        $marca = Marca::find($p->idmarca);
        $p['marca'] = $marca->nombre;
        $categoria = Categoria::find($marca->idcategoria);
        $p['categoria'] = $categoria->nombre;
      }
      return view('admin.productos.index',compact('productos'));
    }

    public function indexcodigos(){
      $marcas = Marca::all();
      foreach ($marcas as $m) {
        $cat = Categoria::find($m->idcategoria);
        $m['descripcion'] = strtoupper($m->nombre.'-'.$cat->nombre);
      }
      return view('admin.productos.generador',compact('marcas'));
    }

    public function getproducto($id){
      $productos = Producto::where('idmarca',$id)->get();
      return $productos;
    }

  public function generarcodigo(Request $request){
      $this->validate($request,[
        "idmarca" => "required",
        "producto" => "required",
        "cantidad" => "required"
      ]);
      $datos = request()->all();
      $archivos = [];
      for($i=0;$i< $datos['cantidad'];$i++){
        $generador = new BarcodeGeneratorHTML();
        $producto = Producto::find($datos['producto']);
        $texto = date('dhms');
        $tipo = $generador::TYPE_CODE_39;
        $imagen = $generador->getBarcode($texto, $tipo);
        $archivos[$i] = (object)array(
          'imagen'=> $imagen,
        );
      }
      $pdf = \PDF::loadView('admin.productos.generadortst', compact('archivos'));
      return $pdf->download('codigobarras-'.$producto->modelo.date('d/m/Y').'.pdf');

    }

    public function indexprecios(){
      return view('admin.precios.index');
    }

    public function indexsucursalupdate(){
      return view('sucursal.precios.index');
    }
    public function updateprecios(){
      $datos = request()->all();
      $productos = Excel::load($datos['archivo'])->get();
      foreach ($productos as $c) {
        if(isset($c['marca']) && $c['marca'] != ""){
          $macaexiste = Marca::where('nombre',$c['marca'])->get();
          if(count($macaexiste) > 0){
            $listmarca = [];
            $idmarca = Marca::where('nombre',$c['marca'])->get(['id']);
            foreach ($idmarca as $m) {
              $listmarca[] = $m['id'];
            }
            $pos = 0;
            foreach ($c as $key => $value) {
              if($pos > 4 && $pos <10){
                $formapago = FormadePago::where('nombre',$key)->get();
                if(count($formapago)== 0){
                  Session::flash('mensaje','NO EXISTE ESTA FORMA DE PAGO '.$key.' EN EL SISTEMA');
                  return redirect()->to('preciosupdate');
                }else{
                  $producto = Producto::where('modelo',$c['modelo'])->get();
                  if(count($producto)== 0){
                    Session::flash('mensaje','NO EXISTE EL ARTICULO '.$c['modelo'].' EN EL SISTEMA');
                    return redirect()->to('preciosupdate');
                  }else{

                    $productomarca = Producto::where('modelo',$c['modelo'])->whereIn('idmarca',$listmarca)->get();
                    if(count($productomarca) == 0){
                      Session::flash('mensaje','NO EXISTE EL ARTICULO '.$c['modelo'].' DE LA MARCA '.$c['marca'].'EN EL SISTEMA');
                      return redirect()->to('preciosupdate');
                    }else{
                      $formadepagoproducto = ProductoFormaPago::where('formapago',$formapago[0]->id)->where('producto',$productomarca[0]->id)->get();
                      if(count($formadepagoproducto) == 0){
                        ProductoFormaPago::create(array(
                          'producto'=>$productomarca[0]->id,
                          'formapago'=>$formapago[0]->id,
                          'precio'=>(float) $value

                        ));
                      }else{
                        $formadepagoproducto = ProductoFormaPago::find($formadepagoproducto[0]->id);
                        $formadepagoproducto->fill(array(
                          'precio'=>(float) $value
                        ));
                        $formadepagoproducto->save();
                      }
                    }
                  }
                }
              }
              $pos++;
            }
          }else{
            Session::flash('mensaje','NO EXISTE LA MARCA '.$c['marca'].' EN EL SISTEMA');
            return redirect()->to('preciosupdate');
          }
        }
      }
      Session::flash('mensaje','LOS PRECIOS FUERON ACTUALIZADOS');
      return redirect()->to('preciosupdate');
    }


    public function updatepreciosweb(){
      $datos = request()->all();
      $productos = Excel::load($datos['archivo'])->get();
      foreach ($productos as $p) {
        $existe = Producto::where('modelo',$p['producto'])->first();
        if($existe == null){
          Session::flash('mensaje','LOS PRODUCTOS '.$p['producto'].' NO EXISTE');
          return redirect()->to('actualizarweb');
        }else{
          $existe->precioweb = $p['precio'];
          $existe->save();
        }
      }
      Session::flash('mensaje','LOS PRODUCTOS SE ACTUALIZARON CON EXITO');
      return redirect()->to('actualizarweb');
    }


    public function sucursalupdate(){
      ini_set('max_execution_time', 180);
      $datos = request()->all();

      $productos = Excel::load($datos['archivo'])->get();
      //dd($productos);
      foreach ($productos as $c) {
      //  dd($c);
        if(isset($c['marca']) && $c['marca'] != ""){
          $macaexiste = Marca::where('nombre',$c['marca'])->get();
          if(count($macaexiste) > 0){
            $listmarca = [];
            $idmarca = Marca::where('nombre',$c['marca'])->get(['id']);
            foreach ($idmarca as $m) {
              $listmarca[] = $m['id'];
            }
            $moneda = Moneda::where('nombre',$c['moneda'])->get();
            if(count($moneda) == 0){
              Session::flash('mensaje','NO EXISTE '.$c['moneda'].' COMO MONEDA EN EL SISTEMA');
              return redirect()->to('sucursal/preciosupdate');
            }
              $moneda = Moneda::where('nombre',$c['moneda'])->first();
              $formapago = FormadePago::where('nombre','EFECTIVO')->get();
            if(count($formapago)== 0){
              Session::flash('mensaje','NO EXISTE ESTA FORMA DE PAGO EFECTIVO EN EL SISTEMA');
              return redirect()->to('sucursal/preciosupdate');
            }
            $producto = Producto::where('modelo',$c['modelo'])->get();

            if(count($producto)== 0){
              Session::flash('mensaje','NO EXISTE EL ARTICULO '.$c['modelo'].' EN EL SISTEMA');
              return redirect()->to('sucursal/preciosupdate');
            }
            $productomarca = Producto::where('modelo',$c['modelo'])->whereIn('idmarca',$listmarca)->get();
            if(count($productomarca) == 0){
              Session::flash('mensaje','NO EXISTE EL ARTICULO '.$c['modelo'].' DE LA MARCA '.$c['marca'].'EN EL SISTEMA');
              return redirect()->to('sucursal/preciosupdate');
            }
            $formadepagoproducto = ProductoFormaPago::where('formapago',$formapago[0]->id)->where('producto',$productomarca[0]->id)->get();
            if(count($formadepagoproducto) == 0){
              ProductoFormaPago::create(array(
                'producto'=>$productomarca[0]->id,
                'formapago'=>$formapago[0]->id,
                'precio'=>(float) $c['efectivo'],
                'moneda'=>$moneda->id
              ));
            }else{
              $formadepagoproducto = ProductoFormaPago::find($formadepagoproducto[0]->id);
              $formadepagoproducto->fill(array(
                'precio'=>(float) $c['efectivo'],
                'moneda'=>$moneda->id
              ));
              $formadepagoproducto->save();
              dd($formadepagoproducto,$producto);
            }
          }else{
            Session::flash('mensaje','NO EXISTE LA MARCA '.$c['marca'].' EN EL SISTEMA');
            return redirect()->to('sucursal/preciosupdate');
          }
        }
      }
      Session::flash('mensaje','LOS PRECIOS FUERON ACTUALIZADOS');
      return redirect()->to('sucursal/preciosupdate');
    }

    public function indexcreate(){
      $importadores = Importador::all();
      $marcas = Marca::all();
      foreach ($marcas as $m) {
        $cat = Categoria::find($m->idcategoria);
        $m['descripcion'] = strtoupper($m->nombre.'-'.$cat->nombre);
      }
      return view('admin.productos.create',compact('importadores','marcas'));
    }

    public function create(Request $request){
      $datos = request()->all();

      $mensaje = 'El producto, fue creado con exito';
      $existe = Producto::where('idmarca',$datos['idmarca'])->where('modelo',$datos['modelo'])->count();
      if($existe > 0){
        $mensaje = 'El producto, ya existe en el sistema';
      }else{
        $imagenes = '';
        foreach ($datos['imagen'] as $imagen) {
          $nombre = $this->upload($imagen);
          $imagenes .=$nombre.',';
        }
        $nombre = $this->upload($datos['imagenweb']);
        $datos['imagenweb'] = $nombre;
        $datos['imagen'] = $imagenes;
        Producto::create(array(
          "idmarca" => $datos['idmarca'],
          "modelo" => $datos['modelo'],
          "descripcion" => $datos['descripcion'],
          "stockminimo" => $datos['stockminimo'],
          "imagen" => $datos['imagen'],
          "imagenweb"=>$datos['imagenweb'],
          "garantia"=>$datos['grantia'],
        ));
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('productos');
    }

    public function edit($id){
      $producto = Producto::find($id);
      if($producto == null){
        Session::flash('mensaje','No se encontro el producto');
        return redirect()->to('productos');
      }
      $marca = Marca::find($producto->idmarca);
      $categoria = Categoria::find($marca->idcategoria);
      $producto['marcaproducto'] = strtoupper($marca->nombre.'-'.$categoria->nombre);

      $importadores = Importador::all();
      $marcas = Marca::all();
      foreach ($marcas as $m) {
        $cat = Categoria::find($m->idcategoria);
        $m['descripcion'] = strtoupper($m->nombre.'-'.$cat->nombre);
      }
      return view('admin.productos.create',compact('importadores','marcas','producto'));
    }

    public function save($id,Request $request){
      $datos = request()->all();
      $mensaje = 'El producto, fue actualizado con exito';
      $existe = Producto::where('idmarca',$datos['idmarca'])->where('modelo',$datos['modelo'])->where('id','<>',$id)->count();
      if($existe > 0){
        $mensaje = 'El producto ya existe en nuestra base de datos';
      }else{
        $producto = Producto::find($id);
        if($producto == null){
          $mensaje = 'El producto no existe';
        }else{
        //  dd($datos);
          if(isset($datos['imagen']) ){
            $imagenes = '';
            foreach ($datos['imagen'] as $imagen) {
              //dd($imagen);
              if($imagen == null){
                $datos['imagen'] = $producto->imagen;
                break;
              }else{
                $nombre = $this->upload($imagen);
                $imagenes .=$nombre.',';
              }

            }
            $datos['imagen'] = $imagenes;
          }else{
            $datos['imagen'] = $producto->imagen;
          }
          if(isset($datos['imagenweb'])){
            //dd('acaa');
            $nombre = $this->upload($datos['imagenweb']);
            $datos['imagenweb'] = $nombre;
          }else{
            $datos['imagenweb'] = $producto->imagenweb;
          }
        //  dd('asa');
          $datos['stock'] = $producto->stock;
          $producto->fill(array(
            "idmarca" => $datos['idmarca'],
            "modelo" => $datos['modelo'],
            "descripcion" => $datos['descripcion'],
            "stockminimo" => $datos['stockminimo'],
            "imagen" => $datos['imagen'],
            'imagenweb'=>$datos['imagenweb'],
            'stock' => $datos['stock'],
            'garantia'=>$datos['grantia']
          ));
          $producto->save();
        }
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('productos');
    }

    public function delete($id){
      $mensaje = 'El producto fue eliminado con exito';
      $producto = Producto::find($id);
      if($producto == null){
        $mensaje = 'El producto no puede ser borrado';
      }else{
        unlink('uploads/'.$producto->imagen);
        $producto->delete();
      }
      Session::flash('mensaje',$mensaje);
      return redirect()->to('productos');
    }

    public function importar(){
      $datos = request()->all();
      $mensaje = 'La importacion se realizo con exito';
      $productos = Excel::load($datos['archivo'])->get();
      foreach ($productos as $c) {
        if(isset($c['marca']) && isset($c['modelo']) && isset($c['descripcion'])
        && isset($c['stock']) && isset($c['stockminimo']) && isset($c['transporte'])
        && isset($c['importador']) && isset($c['categoria'])){
          $marcas = Marca::where('nombre',$c['marca'])->get();
          if(count($marcas) == 0){
            $mensaje = 'La marca '.$c['marca'].', no existe';
            Session::flash('mensaje', $mensaje);
            return redirect()->to('productos');
          }
          $categorias = Categoria::where('nombre',$c['categoria'])->get();
          if(count($marcas) == 0){
            $mensaje = 'La categoria '.$c['categoria'].', no existe';
            Session::flash('mensaje', $mensaje);
            return redirect()->to('productos');
          }

          foreach ($marcas as $m) {
            $categorias = Categoria::where('nombre',$c['categoria'])->where('id',$m->idcategoria)->get();
            if(count($categorias)==0){
              $categoria = Categoria::find($m->idcategoria);
              $mensaje = 'La marca '.$c['marca'].', no pertenece a la categoria '.$categoria->nombre.' '.$c['categoria'];
              Session::flash('mensaje', $mensaje);
              return redirect()->to('productos');
            }
          }
          $importadores = Importador::where('nombre',$c['importador'])->get();
          if(count($importadores) == 0){
            $mensaje = 'El importador '.$c['importar'].', no existe';
            Session::flash('mensaje', $mensaje);
            return redirect()->to('productos');
          }

          $existe = Producto::where('modelo',$c['modelo'])->count();
          if($existe > 0){
            $producto = Producto::where('modelo',$c['modelo'])->forst();
            $producto->fill(array(
              "descripcion" => $c['descripcion'],
              "stock" => $c['stock'],
              "stockminimo" => $c['stockminimo'],
              "transporte" => $c['transporte']
            ));
            $producto->save();
          }else{
            $marcas = Marca::where('nombre',$c['marca'])->get();
            foreach ($marcas as $m) {
              $categoria = Categoria::find($marca->idcategoria);
              if($categoria->nombre == $c['categoria']){
                $idmarca = $m->id;
                break;
              }
              // code...
            }

            $importadores = Importador::where('nombre',$c['importador'])->first();
            $producto = Producto::create(array(
              "idmarca"=>$idmarca,
              "modelo" => $c['modelo'],
              "descripcion" => $c['descripcion'],
              "stockminimo" => $c['stockminimo'],
              "transporte" => $c['transporte'],
            ));
          }


        }else{
          dd('no tiene formato');
        }
      }

      Session::flash('mensaje', $mensaje);
      return redirect()->to('productos');
    }

    private function upload($file){
      //dd($file);
      $nombre = str_random(40).'.'.$file->getClientOriginalExtension();
      $destinationPath = 'uploads';
      $file->move('uploads',$nombre);
      return $nombre;
    }

}
