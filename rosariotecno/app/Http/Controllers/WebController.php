<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Newsletter;
use App\MensajeFormulario;
use App\TextoSlider;
use App\OfertaProducto;
use App\CodigoDescuento;
use App\User;
use Session;
class WebController extends Controller
{
    public function index(){

      return view('admin.web.index');
    }

    public function indexusuarios(){
      $usuarios = User::where('rol','cliente')->get();
      return view('admin.web.usuarios',compact('usuarios'));
    }

    public function newsletter(){
      $newsletters = Newsletter::all();
      return view('admin.web.newsletter',compact('newsletters'));
    }

    public function mensajes(){
      $mensajes =MensajeFormulario::all();
      return view('admin.web.indexmensaje',compact('mensajes'));
    }

    public function vermensaje($id){
      $mensaje = MensajeFormulario::find($id);
      return view('admin.web.mensaje',compact('mensaje'));
    }

    public function indexslider(){
      $sliders = TextoSlider::all();
      return view('admin.web.indexsliders',compact('sliders'));
    }


    public function slidernuevo(){
      return view('admin.web.slider');
    }

    public function slider(){
      $datos= request()->all();
      $existe = TextoSlider::where('categoria',$datos['categoria'])->where('texto',$datos['texto'])
      ->count();
      if($existe > 0 ){
        Session::flash('mensaje','Ya fue creado con anterioridad');
      }else{
        Session::flash('mensaje','Fue creado con exito');
        TextoSlider::create($datos);
      }
      return redirect()->to('admin/sliders');
    }

    public function edit($id){
      $slider = TextoSlider::find($id);
      return view('admin.web.slider',compact('slider'));
    }

    public function save($id){
      $slider = TextoSlider::find($id);
      $datos = request()->all();
      $slider->fill($datos);
      $slider->save();
      return redirect()->to('admin/sliders');
    }

    public function delete($id){
      $slider = TextoSlider::find($id);
      $slider->delete();
      return redirect()->to('admin/sliders');
    }

    public function indexdescuento(){
      $descuento = OfertaProducto::first();
      if($descuento == null){
        $descuento = (object)array(
          'producto'=>'',
          'descuento'=>'',
          'imagen'=>'',
          'valides'=>''
        );
      }
      return view('admin.web.descuento',compact('descuento'));
    }

    public function descuento(){
      $existe = OfertaProducto::first();
      $datos = request()->all();
      if($existe != null){
        $existe->delete();
      }
      if(isset($datos['imagen'])){
          $datos['imagen'] = $this->upload($datos['imagen']);
      }else{
        $datos['imagen'] = '';
      }
      OfertaProducto::create($datos);
      return redirect()->to('admin/descuentos');
    }

    public function indexcupones(){
      $codigos = CodigoDescuento::all();
      return view('admin.web.indexcupones',compact('codigos'));
    }

    public function cupon(){
      return view('admin.web.cupon');
    }

    public function createcupon(){
      $datos = request()->all();
      $datos['estado'] = 'activo';
      CodigoDescuento::create();
      return redirect()->to('admin/cupones');
    }

    private function upload($file){

      $nombre = str_random(40).'.'.$file->getClientOriginalExtension();
      $destinationPath = 'uploads';
      $file->move('uploads',$nombre);
      return $nombre;
    }
}
