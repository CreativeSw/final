<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Categoria;
use Session;
use Excel;
class CategoriaController extends Controller
{

    public function index(){
      $categorias = Categoria::all();
      return view('admin.categorias.index',compact('categorias'));
    }

    public function importar(){
      $datos = request()->all();
      $mensaje = 'La importacion se realizo con exito';
      $categorias = Excel::load($datos['archivo'])->get();
      foreach ($categorias as $c) {
        if(isset($c['nombre'])){
          $cate = Categoria::where('nombre',$c['nombre'])->count();
          if($cate == 0){
            Categoria::create(array('nombre'=>$c['nombre']));
          }
        }else{
          dd('no tiene formato');
        }
      }

      Session::flash('mensaje', $mensaje);
      return redirect()->to('categorias');

    }

    public function indexcreate(){
      return view('admin.categorias.create');
    }

    public function create(){

      $mensaje = 'La categoria fue creada con exito';
      $datos = request()->all();
      $existe = Categoria::where('nombre',$datos['nombre'])->count();
      if($existe > 0){
        $mensaje = 'La categoria ya existe';
      }else{
        $categoria = Categoria::create($datos);
      }
      Session::flash('mensaje', $mensaje);
      return redirect()->to('categorias');
    }

    public function edit($id){
      $categoria = Categoria::find($id);
      if($categoria == null){
        Session::flash('mensaje', 'La categoria selecionada no existe');
        return redirect()->to('categorias');
      }
      return view('admin.categorias.create',compact('categoria'));
    }

    public function save($id){
      $datos =  request()->all();
      $categoria = Categoria::find($id);
      if($categoria == null){
        Session::flash('mensaje', 'Esta categoria no es posible actualizar');
        return redirect()->to('categorias');
      }
      $mensaje = 'La categoria fua actualizada correctamente';
      $existe = Categoria::where('id','<>',$id)->where('nombre',$datos['nombre'])->count();
      if($existe > 0 ){
          $mensaje = 'La categoria ya existe';
      }else{
        $categoria->fill($datos);
        $categoria->save();
      }
      Session::flash('mensaje', $mensaje);
      return redirect()->to('categorias');
    }

    public function delete($id){
      $mensaje = 'La Categoria fue borrada con exito';
      $categoria = Categoria::find($id);
      if($categoria == null){
        Session::flash('mensaje', 'Esta categoria no puede ser borrada');
        return redirect()->to('categorias');
      }
      $categoria->delete();
      return array('success'=>$mensaje);
    }


}
