<?php
//01143793400
//19499793 nro reclamo.
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Newsletter;
use App\MensajeFormulario;
use App\User;
use App\Categoria;
use App\Marca;
use App\Producto;
use App\TextoSlider;
use App\OfertaProducto;
use App\ProductoFormaPago;

use Auth;
use Mail;
use Session;
use MercadoPago\SDK;
use MercadoPago;
use DateTime;
use DateInterval;


class SiteController extends Controller
{
    //
    public function index(){
      $datos = [];
      $smartphone = Categoria::where('nombre','SMARTPHONE')->first();
      $filtros = [];
      $listmarca = [];
      if($smartphone == null){
        $marcas = [];
      }else{
        $marcas = Marca::where('idcategoria',$smartphone->id)->get();
      }
      foreach ($marcas as $m) {
        $listmarca[] = $m['id'];
      }
      $filtros['marca'] = $marcas;
      $productos = Producto::whereIn('idmarca',$listmarca)->get();
      $datos['smartphone'] = $productos;
      $smartphone = Categoria::where('nombre','SMARTWATCH')->first();
      $filtros = [];
      $listmarca = [];
      if($smartphone == null){
          $marcas = [];
      }else{
        $marcas = Marca::where('idcategoria',$smartphone->id)->get();
      }
      foreach ($marcas as $m) {
        $listmarca[] = $m['id'];
      }
      $filtros['marca'] = $marcas;
      $productos = Producto::whereIn('idmarca',$listmarca)->get();
      $datos['smartwatch'] = $productos;
      $smartphone = Categoria::where('nombre','ACCESORIOS')->first();
      $filtros = [];
      $listmarca = [];
      if($smartphone == null){
        $marcas = [];
      }else{
        $marcas = Marca::where('idcategoria',$smartphone->id)->get();
      }

      foreach ($marcas as $m) {
        $listmarca[] = $m['id'];
      }
      $filtros['marca'] = $marcas;
      $productos = Producto::whereIn('idmarca',$listmarca)->get();
      $datos['accesorios'] = $productos;
      if(Session::get('favoritos') == null){
        Session::set('favoritos',[]);
      }
      if(Session::get('carrito') == null){
        Session::set('carrito',[]);
      }
      if(Session::get('total') == null){
        Session::set('total',0);
      }
      if(Session::get('descuento') == null){
        Session::set('descuento','no');
      }
      $sliders = TextoSlider::all();
      $oferta = OfertaProducto::first();
      if($oferta != null){
        $vencimiento = new DateTime($oferta->created_at->format('Y-m-d m:i:s'));
        $vencimiento->add(new DateInterval('P'.$oferta->valides.'D'));
        $oferta['vencimiento'] = $vencimiento->format('Y-m-d m:i:s');
      }
      return view('web.index', compact('datos','sliders','oferta'));
    }


    public function favoritos(){
      if(Session::get('favoritos') == null){
        Session::set('favoritos',[]);
      }
      return view('web.favoritos');
    }

    public function perfil(){
      $cuenta = Auth::user();
      if($cuenta == null){
        return redirect()->to('/');
      }
      return view('web.perfil',compact('cuenta'));
    }

    public function saveperfil(Request $request){
      $datos = request()->all();
      $cuenta = User::find(Auth::user()->id);
      if($cuenta == null){
        return redirect()->to('/');
      }else{
      //  dd($datos);
        if($datos['password'] == ''){
          unset($datos['password']);
        }else{
          $datos['password'] = bcrypt($datos['password']);

        }

        $cuenta->fill($datos);
        $cuenta->save();

        return redirect()->to('/');
      }

    }

    public function carrito(){
      if(Session::get('carrito') == null){
        Session::set('carrito',[]);
      }
      return view('web.carrito');
    }

    public function addfavorito($id){
      $producto = Producto::find($id);
      Session::push('favoritos',$producto);
      return redirect()->to('/');
    }

    public function addcarrito($id){
      $producto = Producto::find($id);
      if($producto->precio == null){
        $producto->precio = 0;
      }
      $producto['precio'] = $producto->precio;
      $producto['cantidad'] = 1;
      $total = Session::get('total');
      $total = $total + $producto->precio ;
      Session::set('total',$total);
      Session::push('carrito',$producto);
      return redirect()->to('/');
    }

    public function addcarritocoutome($id,$cantidad){
      $producto = Producto::find($id);
      $subtotal = $producto->precio * $cantidad;
      $total = Session::get('total');
      $total = $total + $subtotal;
      $producto['precio'] = $producto->precio;
      $producto['cantidad'] = $cantidad;
      Session::push('carrito',$producto);
      Session::set('total',$total);
      return redirect()->to('/');
    }

    public function finishcarrito(){

      dd(Auth::user());
      if(Auth::user() == null){
        return redirect()->to('loginweb');
      }
      dd(Auth::user());
    }

    public function deletefavorito($id){
      $favoritos = Session::get('favoritos');
      Session::pull('favoritos');
      unset($favoritos[$id]);
      foreach ($favoritos as $f) {
        Session::push('favoritos',$f);
      }
      return redirect()->to('/');
    }

    public function smartphone(){
      $smartphone = Categoria::where('nombre','SMARTPHONE')->first();
      $min = 0;
      $max = 0;
      $filtros = [];
      $listmarca = [];
      if($smartphone == null){
        $productos = (object)[];
        $categoria = 'SMARTPHONE';
        $filtros['marca'] = [];
      }else{
        $marcas = Marca::where('idcategoria',$smartphone->id)->get();
        foreach ($marcas as $m) { //O(n)
          $listmarca[] = $m['id'];
        }
        $filtros['marca'] = $marcas;
        $productos = Producto::whereIn('idmarca',$listmarca)->get();
        $categoria = 'SMARTPHONE';
      }
      return view('web.categorias',compact('productos','filtros','categoria','min','max'));
    }

    public function smartwatch(){
      $smartphone = Categoria::where('nombre','SMARTWATCH')->first();
      $min = 0;
      $max = 0;
      $filtros = [];
      $listmarca = [];
      if($smartphone == null){
        $productos = (object)[];
        $categoria = 'SMARTWATCH';
        $filtros['marca'] = [];
      }else{
        $marcas = Marca::where('idcategoria',$smartphone->id)->get();
        foreach ($marcas as $m) {
          $listmarca[] = $m['id'];
        }
        $filtros['marca'] = $marcas;
        $productos = Producto::whereIn('idmarca',$listmarca)->get();
        $categoria = 'SMARTWATCH';
      }
      return view('web.categorias',compact('productos','filtros','categoria','min','max'));
    }

    public function accessorios(){
      $smartphone = Categoria::where('nombre','ACCESORIOS')->first();
      $filtros = [];
      $listmarca = [];
      $min = 0;
      $max = 0;
      if($smartphone == null){
        $productos = (object)[];
        $categoria = 'ACCESORIOS';
        $filtros['marca'] = [];
      }else{
        $marcas = Marca::where('idcategoria',$smartphone->id)->get();
        foreach ($marcas as $m) {
          $listmarca[] = $m['id'];
        }
        $filtros['marca'] = $marcas;
        $productos = Producto::whereIn('idmarca',$listmarca)->get();
        $categoria = 'ACCESORIOS';
      }
      return view('web.categorias',compact('productos','filtros','categoria','min','max'));
    }
    public function notebooks(){
      $smartphone = Categoria::where('nombre','NOTEBOOKS')->first();
      $filtros = [];
      $listmarca = [];
      $min = 0;
      $max = 0;
      if($smartphone == null){
        $productos = (object)[];
        $categoria = 'NOTEBOOKS';
        $filtros['marca'] = [];
      }else{
        $marcas = Marca::where('idcategoria',$smartphone->id)->get();
        foreach ($marcas as $m) {
          $listmarca[] = $m['id'];
        }
        $filtros['marca'] = $marcas;
        $productos = Producto::whereIn('idmarca',$listmarca)->get();
        $categoria = 'NOTEBOOKS';
      }
      return view('web.categorias',compact('productos','filtros','categoria','min','max'));
    }

    public function producto($id){
      $producto = Producto::find($id);
      $marca = Marca::find($producto->idmarca);
      $categoria = Categoria::find($marca->idcategoria);
      $producto['categoria'] = $categoria->nombre;
      $productorelacionados = Producto::where('idmarca','<>',$producto->idmarca)->where('idmarca',$producto->idmarca)->get();
      return view('web.producto',compact('producto','productorelacionados'));
      dd($producto);
    }

    public function newsletter(){
      $datos = request()->all();
      $existe = Newsletter::where('email',$datos['email'])->get();
      if(count($existe) == 0){
        Newsletter::create($datos);
      }
      return redirect()->to('/');
    }

    public function indexlogin(){
      return view('web.login');
    }
    public function contacto(){
      //dd(explode('/',url()->current()));
    //  dd(strpos(url()->current(),'putas'));
      return view('web.contacto');
    }

    public function enviarmsj(){
      $datos = request()->all();
      MensajeFormulario::create($datos);
      return redirect()->to('/');
    }

    public function indexregister(){
      return view('web.registro');
    }

    public function register(){
    //  dd(request()->all());
      $datos = request()->all();
      $datos['rol'] = 'cliente';
      $datos['password'] = bcrypt($datos['password']);
    //  dd($datos);
      $existe = User::where('email',$datos['email'])->get();
      if(count($existe)> 0){
        return redirect()->to('/');
      }
      //dd(User::all());
      User::create($datos);
      return redirect()->to('/');
      /*
      $datos['email'] = 'testaplicacionessmtp@gmail.com';
      $to_name = 'DANIEL';
      $to_email = $datos['email'];
      $data = array('name'=>"Sam Jose", "body" => "Test mail");
      Mail::send('mail.activacion', $data, function($message) use ($to_name, $to_email) {$message->to($to_email, $to_name)->subject('Artisans Web Testing Mail');
      $message->from('testaplicacionessmtp@gmail.com','Artisans Web');});
      */
    }
}
