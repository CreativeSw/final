<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Validator;
use App\Cliente;

class LoginController extends Controller
{
    public function index(){
      return view('login');
    }
    public function login(Request $request){

     $this->validate($request,[
        '_token'=>'required',
        'email'=>'required',
        'password'=>'required'
      ]);
      $form = request()->all();
      $credenciales = array(
        'email' => $form['email'],
        'password' =>$form['password']
      );
      //dd(Auth::attempt($credenciales));
      if(Auth::attempt($credenciales) ){
        if(Auth::user()->rol == "admin")
          return redirect()->to('ppal');

        if(Auth::user()->rol == "sucursal")
          return redirect()->to('sucursal');

        if(Auth::user()->rol == "deposito")
          return redirect()->to('deposito');

        if(Auth::user()->rol == "cliente")
          return redirect()->to('/');
          
      }else{
        \Session::flash('mensaje', 'Usuario o Clave Incorrecto');
        return redirect()->to('/');
      }
    }

    public function salir(){
      Auth::logout();
      return redirect()->to('/');
    }

    public function ppal(){

      $clientes = Cliente::where('tipo','cliente')->count();

      return view('admin.index',compact('clientes'));
    }

    public function sucursal(){
      return view('sucursal.index');
    }
}
