<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Gasto;
class GastoController extends Controller
{
    public function index(){
      $clientes = Gasto::all();
      return view('admin.gastos.index', compact('clientes'));
    }

    

    public function viewcreate(){
      return view('admin.gastos.create');
    }

    public function create(){
      $datos = request()->all();
      Gasto::create($datos);
      return redirect()->to('admin/gastos');
    }

    public function edit($id){
      $cliente = Gasto::find($id);
      return view('admin.gastos.create',compact('cliente'));
    }

    public function save($id){
      $datos = request()->all();
      $cliente = Gasto::find($id);
      $cliente->fill($datos);
      $cliente->save();
      return redirect()->to('admin/gastos');
    }

    public function delete($id){
      $cliente = Gasto::find($id);
      $cliente->delete();
      return array('mensaje'=>'ok');
    }
}
