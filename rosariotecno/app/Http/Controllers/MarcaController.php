<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Categoria;
use App\Marca;
use Session;
use Excel;
class MarcaController extends Controller
{

    public function index(){
      $marcas = Marca::all();
      foreach ($marcas as $m) {
        $m['categoria'] = Categoria::find($m->idcategoria)->nombre;
      }
      return view('admin.marcas.index',compact('marcas'));
    }

    public function indexcreate(){
      $categorias = Categoria::all();
      return view('admin.marcas.create',compact('categorias'));
    }

    public function create(){
      $mensaje = 'La Marca, fue creada con exito';
      $datos = request()->all();
      $existe = Marca::where('nombre',$datos['nombre'])->where('idcategoria',$datos['idcategoria'])->count();
      if($existe > 0 ){
        $mensaje = 'La Marca ya existe';
      }else{
        Marca::create($datos);
      }
      Session::flash('mensaje', $mensaje);
      return redirect()->to('marcas');
    }

    public function edit($id){
      $marca = Marca::find($id);
      if($marca == null){
        Session::flash('mensaje', 'La Marca selecionada no existe');
        return redirect()->to('marcas');
      }
      $categorias = Categoria::all();
      foreach ($categorias as $c) {
        if($c->id == $marca->idcategoria){
          $c['selecionado'] = 'si';
        }
      }
      return view('admin.marcas.create',compact('marca','categorias'));
    }

    public function save($id){
      $datos = request()->all();
      $marca = Marca::find($id);
      if($marca == null){
        Session::flash('mensaje', 'La Marca no puede ser actualizada');
        return redirect()->to('marcas');
      }
      $existe = Categoria::find($marca->idcategoria);

      if($existe == null){
        Session::flash('mensaje', 'La Categoria que selecciono no existe');
        return redirect()->to('marcas');
      }

      $existe = Marca::where('nombre',$datos['nombre'])->where('idcategoria',$datos['idcategoria'])->where('id','<>',$id)->count();
      if($existe > 0){
          Session::flash('mensaje', 'La Marca ya existe');
      }else{
        $marca->fill($datos);
        $marca->save();
        Session::flash('mensaje', 'La Marca fue actualizada correctamente');
      }
      return redirect()->to('marcas');
    }

    public function delete($id){
      $marca = Marca::find($id);
      if($marca == null){
        Session::flash('mensaje','La Marca no se puede borrar');
      }
      $marca->delete();
      Session::flash('mensaje','La Marca se borro con exito');
      return redirect()->to('marcas');
    }

    public function importar(){

      $datos = request()->all();
      $mensaje = 'La importacion se realizo con exito';
      $marcas = Excel::load($datos['archivo'])->get();
      foreach ($marcas as $c) {
        if(isset($c['nombre']) && isset($c['categoria'])){
          $cate = Categoria::where('nombre',$c['categoria'])->first();
          if($cate == null){
            $mensaje = 'La categoría '.$c['categoria'].' no existe. Primero cree la categoria';
            break;
          }
          $existe = Marca::where('idcategoria',$cate->id)->where('nombre',$c['nombre'])->count();
          if($existe == 0){
            unset($datos['categoria']);
            Marca::create(array('nombre'=>$c['nombre'],'idcategoria'=>$cate->id));
          }
        }else{
          dd('no tiene formato');
        }
      }
      Session::flash('mensaje', $mensaje);
      return redirect()->to('marcas');
    }

}
