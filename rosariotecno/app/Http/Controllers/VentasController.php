<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cliente;
use App\FormadePago;
use App\Producto;
use App\Stock;
use App\Moneda;
use App\Venta;
use App\User;
use App\ArticuloVenta;
use App\HistorialCaja;
use App\ProductoSucursal;
use App\ProductoFormaPago;
use Auth;
use Session;
use Carbon\Carbon;

class VentasController extends Controller
{
    public function indexsucursal(){
      $ventas = Venta::where('sucursal',Auth::user()->id)->where('venta','minorista')->orderBy('id','desc')->get();
      foreach ($ventas as $v) {
        $v['articulos'] = ArticuloVenta::where('venta',$v->id)->get();
      }
      return view('sucursal.ventas.index',compact('ventas'));
    }

    public function indexsucursalmayorista(){
      $ventas = Venta::where('sucursal',Auth::user()->id)->orderBy('id')->where('venta','mayorista')->get();
    //  dd($ventas);

      foreach ($ventas as $v) {
        $v['articulos'] = ArticuloVenta::where('venta',$v->id)->get();
      }
      return view('sucursal.ventasmayorista.index',compact('ventas'));
    }

    public function indexadmin(){
      $sucursales = User::where('rol','sucursal')->get(['id','direccion','telefono','rol']);
      return view('admin.ventas.panel',compact('sucursales'));
    }

    public function sucursal($id){
      $ventas = Venta::where('sucursal',$id)->orderBy('id','desc')->get();
      foreach ($ventas as $v) {
        $v['articulos'] = ArticuloVenta::where('venta',$v->id)->get();
      }
      return view('admin.ventas.index',compact('ventas'));
    }

    public function garantia(){
      $clientes = Cliente::where('tipo','cliente')->get();
      return view('sucursal.garantia.index',compact('clientes'));
    }

    public function garantiaadmin(){
      $clientes = Cliente::where('tipo','cliente')->get();
      return view('admin.garantia.index',compact('clientes'));
    }

    public function getgarantia($imei,$cliente){
      $datos = request()->all();
      $ventas = Venta::where('cliente',$cliente)->join('articulo_ventas','ventas.id','=','articulo_ventas.venta')
      ->where('imei', 'like', '%' . $imei . '%')
      ->get();
      if(count($ventas) == 0){
        $mensaje = 'El producto no fue comprado por el cliente';
      }else{
        foreach ($ventas as $v) {
          $producto = Producto::find($v->articulo);
          $inicio = Carbon::parse($v->created_at);
          $mensaje = 'El producto '.$producto->modelo.'<br> vence: '.$inicio->addMonths($producto->garantia)->format('d/m/Y');
        }
      }
      return $mensaje;
    }

    public function deletesucursal($id){
      ArticuloVenta::where('venta',$id)->delete();
      $venta = Venta::find($id);
      $venta->delete();
      Session::flash('mensaje','La venta fue borrado con exito');
      return redirect()->to('sucursal/ventas');
    }

    public function deleteproducto($id){
      $art = ArticuloVenta::find($id);
      $art->delete();
      return array('oki'=>'oki');
    }
    public function indexcreatesucursal(){
      $clientes = Cliente::where('tipo','clienteminorista')->get();
      $formasdepago = FormadePago::all();
      $productos = Producto::all();
      return view('sucursal.ventas.create',compact('clientes','productos','formasdepago'));
    }

    public function indexcreatesucursalmayorista(){
      $clientes = Cliente::where('tipo','clientemayorista')->get();
      //$formasdepago = FormadePago::all();
      $monedas = Moneda::all();
      $productos = Producto::all();
      return view('sucursal.ventasmayorista.create',compact('clientes','productos','monedas'));
    }

    public function editarventasucursal($id){
      $clientes = Cliente::all();
      $formasdepago = FormadePago::all();
      $productos = Producto::all();
      $venta = Venta::find($id);
      $venta['articulos'] = ArticuloVenta::where('venta',$id)->get();
      return view('sucursal.ventas.create',compact('venta','clientes','productos','formasdepago'));
    }

    public function view($id){
      $clientes = Cliente::all();
      $formasdepago = FormadePago::all();
      $productos = Producto::all();
      $venta = Venta::find($id);
      $venta['articulos'] = ArticuloVenta::where('venta',$id)->get();
      return view('sucursal.ventas.view',compact('venta','clientes','productos','formasdepago'));
    }

    public function saveventasucursal(Request $request,$id){
      $this->validate($request,[
         '_token'=>'required',
         'formadepagos'=>'required',
         'cliente'=>'required',
         'total'=>'required',
      ]);

      $datos = request()->all();
      $venta = Venta::find($id);


      $venta->fill($datos);
      $venta->save();
      if(isset($datos['producto'])){
        foreach ($datos['producto'] as $i => $p) {
          $producto = Producto::find($p);
          $stock = Stock::where('idproducto',$p)->where('idsucursal',Auth::user()->id)->first();
          $stock->cantidad = $stock->cantidad - $datos['cantidad'][$i];
          $stock->save();
          ArticuloVenta::create(array(
            'articulo'=>$p,
            'producto'=>$producto->modelo,
            'cantidad'=>$datos['cantidad'][$i],
            'precio'=>$datos['precio'][$i],
            'imei'=>trim($datos['imei'][$i]),
            'venta'=>$venta->id
          ));
        }
      }
      return redirect()->to('sucursal/ventas');
    }


    public function getproductoimei($imei){
      //$imei = $imei.'\r\n';
      //dd($imei);
      $psucursal = ProductoSucursal::where('imeis','like','%'.$imei.'%')->first();
      //dd($psucursal);
      $existe = '';
      $vendido = ArticuloVenta::where('imei',$imei)->get();

      if(count($vendido) > 0){
        return array('success'=>false,
                    'producto'=>'El Imei: '.$imei.' fue vendido con enterioridad'
                  );
      }

      $forma = "EFECTIVO";
      $f = FormadePago::where('nombre',"EFECTIVO")->first();
      $forma = $f->id;
      $productos = [];
      if($psucursal == null)
      return array('success'=>false,
                  'producto'=>'El Imei: '.$imei.' no existe'
                );
      $imeilist = '';
      foreach (preg_split('/\n|\r\n?/',$psucursal['imeis']) as $key => $im) {
        if($im == $imei){
          $producto = Producto::find($psucursal->idproducto);
          $precio = ProductoFormaPago::where('producto',$producto->id)->where('formapago',$forma)->first();
          if($precio == null)
          return array('success'=>false,
                      'producto'=>'El Imei: '.$imei.'<br> no tiene precio cargado en el sistema'
                    );
          $moneda = Moneda::find($precio->moneda);
          $preciofinal = $precio->precio*$moneda->cotizacion;
          $productos['precio'] = $preciofinal;
          $productos['cantidad'] = 1;
          $productos['id'] = $psucursal->idproducto;
          $productos['imei'] = $imei;
          $productos['modelo'] = $producto->modelo;
          break;
        }

      }
      if(count($productos) > 0){
        return array('success'=>true,
                    'producto'=>$productos
                  );
      }else{
        return array('success'=>false,
                    'producto'=>'El Imei: '.$imei.'<br> no tiene precio cargado en el sistema'
                  );
      }

    }

    public function getproducto($forma,$id){
      $existen = Stock::where('idproducto',$id)->where('idsucursal',Auth::user()->id)->first();
      $recargo = ProductoSucursal::where('idproducto',$id)->where('idsucursal',Auth::user()->id)->latest('created_at')->first();
    //  dd($recargo);
    //  $recargolist = json_decode($recargo->recargo);
    //  dd($recargo);
    //  $totalrecargo = 0;

      //foreach ($recargolist as $r) {
      //  $totalrecargo += $r;
      //}
      //$totalrecargo = $totalrecargo / $recargo->cantidad;
      //dd($totalrecargo);
      $formasdepago = FormadePago::find($forma);
      $totalrecargo = 1;
      //dd($formasdepago,$existen);
      $existen->precio = $existen->precio +(($existen->precio * $formasdepago->recargo)/100);
      $existen->precio = $existen->precio +(($existen->precio * $totalrecargo)/100);


      if($existen == null){
        $existen = Stock::where('idproducto',$id)->get();
        $mensaje  = '';
        if(count($existen) > 0){
          foreach ($existen as $e) {
            $sucursal = User::find($e->idsucursal);
            $mensaje .= 'Hay existencia en '.$sucursal->nombre.'<br> <span style="font-weight:bold; ">Dirección:</span> '.$sucursal->direccion.'<br>';
          }
        }else{
          $mensaje = 'No hay en stock';
        }
        return array('mensaje'=>$mensaje);
      }
      $moneda = Moneda::find($existen->idmoneda);
      $existen->precio = $existen->precio*$moneda->cotizacion;
      return $existen;
    }

    public function createsucursal(Request $request){
    //  dd(request()->all());
      $this->validate($request,[
         '_token'=>'required',
         'formadepagos'=>'required',
         'cliente'=>'required',
         'total'=>'required',
         'producto'=>'required',
         'cantidad'=>'required',
         'imei'=>'required',
         'precio'=>'required',
      ]);
      $datos = request()->all();
      $datos['pago'] = 'completo';
      if($datos['entrega'] != 0){
        $datos['pago'] = 'parcial';
      }
      $datos['sucursal'] =Auth::user()->id;
      $cliente= Cliente::find($datos['cliente']);
      $datos['venta']='minorista';
      $formasdepagos = [];
      foreach ($datos['formadepagos'] as $key => $value) {
        $formasdepagos[$value] = $datos['formadepagosmonto'][$key];
      }
      $datos['formadepagos'] = json_encode($formasdepagos);
      $venta = Venta::create($datos);
      $formas = [];
      $i = 0;

      foreach ($formasdepagos as $key => $value) {
        $formanombre = FormadePago::find($key);
        $formas[$i] = (object)array(
          'id'=>$key,
          'nombre'=> $formanombre->nombre,
          'monto' => $value,
        );
        $i++;
      }
      foreach ($datos['producto'] as $i => $p) {
        $producto = Producto::find($p);
        $stock = Stock::where('idproducto',$p)->where('idsucursal',Auth::user()->id)->first();


        $stock->cantidad = $stock->cantidad - $datos['cantidad'][$i];
        $stock->save();
        ArticuloVenta::create(array(
          'articulo'=>$p,
          'producto'=>$producto->modelo,
          'cantidad'=>$datos['cantidad'][$i],
          'precio'=>$datos['precio'][$i],
          'imei'=>trim($datos['imei'][$i]),
          'venta'=>$venta->id
        ));
      }
      $venta['cliente'] = $cliente;
      $venta['articulos'] = ArticuloVenta::where('venta',$venta->id)->get();
      if($datos['comprobante'] == 'vanta'){
        $pdf = \PDF::loadView('informes.ticket', compact('venta','formas'));
        return $pdf->download('comprobante-'.$cliente->nombre.date('d/m/Y').'.pdf');
      }else{
        return view('informes.ticketticket', compact('venta','formas'));
      //  $pdf = \PDF::loadView();
      //  return $pdf->download('ticket-'.$cliente->nombre.date('d/m/Y').'.pdf');
      }
    }


    public function createsucursalmayorista(Request $request){

      $this->validate($request,[
         '_token'=>'required',
         'moneda'=>'required',
         'cliente'=>'required',
         'total'=>'required',
         'producto'=>'required',
         'cantidad'=>'required',
         'imei'=>'required',
         'precio'=>'required',
      ]);


      $datos = request()->all();

      $datos['pago'] = 'completo';
      if($datos['entrega'] != 0){
        $datos['pago'] = 'parcial';
      }

      $datos['sucursal'] =Auth::user()->id;
      $cliente= Cliente::find($datos['cliente']);
      $datos['venta'] = 'mayorista';
      $formasdepago = FormadePago::where('nombre','EFECTIVO')->first();
      $datos['forma']= $formasdepago->id;
      $ventacrear = Venta::create($datos);
      $venta = $ventacrear;
      foreach ($datos['producto'] as $i => $p) {
        $producto = Producto::find($p);
        $stock = Stock::where('idproducto',$p)->where('idsucursal',Auth::user()->id)->first();
        $stock->cantidad = $stock->cantidad - $datos['cantidad'][$i];
        $stock->save();
        /*ACA*/
        ArticuloVenta::create(array(
          'articulo'=>$p,
          'producto'=>$producto->modelo,
          'cantidad'=>$datos['cantidad'][$i],
          'precio'=>$datos['precio'][$i],
          'imei'=>trim($datos['imei'][$i]),
          'venta'=>$ventacrear->id
        ));
      }

      $formas = FormadePago::where('nombre','EFECTIVO')->get();
      $venta['cliente'] = $cliente;
      $venta['articulos'] = ArticuloVenta::where('venta',$venta->id)->get();
      $pdf = \PDF::loadView('informes.ticket', compact('venta','formas'));
      return $pdf->download('ticket-'.$cliente->nombre.date('d/m/Y').'.pdf');
    }

    public function editmayorista($id){
      $venta = Venta::find($id);
      $venta['articulos'] = ArticuloVenta::where('venta',$id)->get();
      $clientes = Cliente::where("tipo",'<>' ,"clienteminorista")->get();
      $monedas = Moneda::all();
      return view('sucursal.ventasmayorista.create',compact('venta','clientes','monedas'));
    }



    public function savemayorista(Request $request,$id){

      $this->validate($request,[
         '_token'=>'required',
         'moneda'=>'required',
         'cliente'=>'required',
         'total'=>'required',
      ]);
      $datos = request()->all();
      $venta = Venta::find($id);
      $venta->fill($datos);
      $venta->save();
      if(isset($datos['producto'])){
        foreach ($datos['producto'] as $i => $p) {
          $producto = Producto::find($p);
          $stock = Stock::where('idproducto',$p)->where('idsucursal',Auth::user()->id)->first();
          $stock->cantidad = $stock->cantidad - $datos['cantidad'][$i];
          $stock->save();
          ArticuloVenta::create(array(
            'articulo'=>$p,
            'producto'=>$producto->modelo,
            'cantidad'=>$datos['cantidad'][$i],
            'precio'=>$datos['precio'][$i],
            'imei'=>trim($datos['imei'][$i]),
            'venta'=>$venta->id
          ));
        }
      }
      return redirect()->to('sucursal/ventasmayorista');

    }

    public function cerrarcajasucursalver($id){
      $caja = HistorialCaja::find($id);
      $articulos = $caja->articulos;
      $articulos = explode(',',$articulos);
      foreach ($articulos as $k => $art) {
        if($art == " "){
          unset($articulos[$k]);
        }
      }
      $ventas = array(
        'total'=>0,
        'articulos'=>[],
        'cantidad'=>[],
        'precio'=>[],
        'formadepago'=>'',
        'adelanto'=>0
      );
      foreach ($articulos as $k => $art) {
        $venta = Venta::find($art);
        $formasdepago = FormadePago::find($venta->formadepagos);
        if($formasdepago != null){
          $productos = ArticuloVenta::where('venta',$venta->id)->get();
          foreach ($productos as $p) {
            // code...
            $ventas['articulos'][] = $p->producto;
            $ventas['cantidad'][] = $p->cantidad;
            $ventas['precio'][] = $p->precio;
          }
          $ventas['adelanto'] = $venta->entrega;
          $ventas['total'] = $venta->total;
          $ventas['formadepago'] = $formasdepago->nombre;
        }else{
          continue;
        }

      }
      $caja['ventas'] = $ventas;
      unset($caja['articulos']);
      //dd($caja);

      //dd($caja['ventas']);
      return view('sucursal.cierre.view',compact('caja'));
    }

    public function cerrarcajasucursal(){
      $cajas = HistorialCaja::where('sucursal',Auth::user()->nombre)->get();
      return view('sucursal.cierre.index',compact('cajas'));

      /*
      $total = 0;
      $articulos = ' ';
      //dd('aca');
      $ventas = Venta::where('sucursal',Auth::user()->id)->where('estado','abierta')->get();
      foreach ($ventas as $v) {
        $articulos = $articulos.','.strval($v->id);
        $total += $v->total;
        $v->estado = 'cerrada';
        $v->save();
        $v['articulos'] = ArticuloVenta::where('venta',$v->id)->get();
      }

      if(count($ventas)> 0){
        $ventas[0]['totalcaja'] = $total;
        $sucursal = User::find(Auth::user()->id);
        HistorialCaja::create(array(
          'sucursal'=>$sucursal->nombre,
          'articulos'=>$articulos,
          'total'=>$total
        ));
        dd($aca);
        $pdf = \PDF::loadView('informes.cierredecaja', compact('ventas'));
        return $pdf->download('cierredecaja-'.Auth::user()->nombre.date('d/m/Y').'.pdf');
      }
      //return redirect()->to('sucursal/ventas');
    }*/
  }

  public function cerrarcaja(){
    $total = 0;
    $articulos = ' ';
    //dd('aca');
    $ventas = Venta::where('sucursal',Auth::user()->id)->where('estado','abierta')->get();
    foreach ($ventas as $v) {
      $articulos = $articulos.','.strval($v->id);
      $total += $v->total;
      $v->estado = 'cerrada';
      $v->save();
      $v['articulos'] = ArticuloVenta::where('venta',$v->id)->get();
    }

    if(count($ventas)> 0){
      $ventas[0]['totalcaja'] = $total;
      $sucursal = User::find(Auth::user()->id);
      HistorialCaja::create(array(
        'sucursal'=>$sucursal->nombre,
        'articulos'=>$articulos,
        'total'=>$total
      ));
    //  dd($aca);
      $pdf = \PDF::loadView('informes.cierredecaja', compact('ventas'));
      return $pdf->download('cierredecaja-'.Auth::user()->nombre.date('d/m/Y').'.pdf');
    }
    return redirect()->to('sucursal/ventas');
  }
}
