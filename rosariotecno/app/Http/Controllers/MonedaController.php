<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Moneda;
use Session;
Use Excel;
Use Auth;
use Illuminate\Support\Facades\URL;

class MonedaController extends Controller
{

    public function index(){
      $url = URL::current();
      $monedas = Moneda::all();
      if(Auth::user()->rol=="sucursal"){
        return view('sucursal.monedas.index',compact('monedas'));
      }else{
        return view('admin.monedas.index',compact('monedas'));
      }
    }

    public function indexcreate(){
      return view('admin.monedas.create');
    }

    public function create(){
      $datos = request()->all();
      $url = URL::current();
      $mensaje = 'La moneda fue creada correctamente';
      $existe = Moneda::where('nombre',$datos['nombre'])->count();
      if($existe > 0){
        $mensaje = 'La moneda ya existe en el sistema';
      }else{
        Moneda::create($datos);
      }
      Session::flash('mensaje',$mensaje);
      if(Auth::user()->rol=="sucursal"){
          return redirect()->to('sucursal/monedas');
      }else{
          return redirect()->to('contabilidad/monedas');
      }
    }

    public function edit($id){
      $moneda = Moneda::find($id);
      $url = URL::current();
      if($moneda == null){
        Session::flash('mensaje','La moneda que quieres editar, no existe');
        if(Auth::user()->rol=="sucursal"){
          return redirect()->to('sucursal/monedas');
        }else{
          return redirect()->to('contabilidad/monedas');
        }
      }
      if(strpos($url,'sucursal')!= -1){
        return view('sucursal.monedas.create',compact('moneda'));
      }else{
        return view('admin.monedas.create',compact('moneda'));
      }

    }

    public function save($id){
      $moneda = Moneda::find($id);
      $url = URL::current();
      $datos = request()->all();
      $mensaje = 'La moneda fue actualizada con exito';
      if($moneda == null){
        Session::flash('mensaje','La moneda que quieres editar, no existe');
        if(Auth::user()->rol=="sucursal"){
          return redirect()->to('sucursal/monedas');
        }else{
          return redirect()->to('contabilidad/monedas');
        }
      }
      $existe = Moneda::where('nombre',$datos['nombre'])->where('id','<>',$id)->count();
      if($existe > 0){
        $mensaje = 'La moneda ya existe en el sistema';
      }else{
        $moneda->fill($datos);
        $moneda->save();
      }
      Session::flash('mensaje',$mensaje);
      if(Auth::user()->rol=="sucursal"){
        return redirect()->to('sucursal/monedas');
      }else{
        return redirect()->to('contabilidad/monedas');
      }
    }

    public function delete($id){
      $moneda = Moneda::find($id);
      $url = URL::current();
      $mensaje = 'La moneda fue borrada con exito';
      if($moneda == null){
        $mensaje = 'La moneda, no existe';
      }
      $moneda->delete();
      Session::flash('mensaje',$mensaje);
      if(Auth::user()->rol=="sucursal"){
        return redirect()->to('sucursal/monedas');
      }else{
        return redirect()->to('contabilidad/monedas');
      }
    }

    public function importar(){
      $datos = request()->all();
      $url = URL::current();
      $mensaje = 'La importacion se realizo con exito';
      $monedas = Excel::load($datos['archivo'])->get();
      foreach ($monedas as $i) {
        if(isset($i['nombre']) && isset($i['cotizacion'])){
          $imp = Moneda::where('nombre',$i['nombre'])->first();
          if($imp == null){
            $im = Moneda::create(array(
              'nombre'=>$i['nombre'],
              'cotizacion'=>$i['cotizacion']
            ));
          }else{
            $imp->fill(array(
              'nombre'=>$i['nombre'],
              'cotizacion'=>$i['cotizacion']
            ));
            $imp->save();
          }
        }else{
          dd('no tiene formato');
        }

      }
      Session::flash('mensaje', $mensaje);
      if(Auth::user()->rol=="sucursal"){
        return redirect()->to('sucursal/monedas');
      }else{
        return redirect()->to('contabilidad/monedas');
      }
    }

}
